///@description Select a random item froma  DS list exclusing certain results from the result

///@param {Number} id
///@param {Array} exclude
///@retusn {DSList}

///@author Codecomposer

var list_id = argument[0];
var exclude = argument[1];

// Make a copy of the list
var tmp_list = ds_list_create();
ds_list_copy(tmp_list, list_id);

// get a random result
ds_list_shuffle(tmp_list);
var result = ds_list_find_value(tmp_list, 0);

// Destroy the list
ds_list_destroy(tmp_list);

// If the result was not exclude return it
if( !in_array(result, exclude) ){
	return result;
}

// If the esult was excluded run again
return ds_list_random_exclude(list_id, exclude);