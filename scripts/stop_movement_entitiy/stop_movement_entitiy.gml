///@description stops all acceleration on a movement entity

///@param {Integer} axis - 0 vertical | 1 horizontal

if( argument0 == 0 ){
	vsp[0] = 0;
	vsp[1] = 0;
}

if( argument0 == 1 ){
	hsp[0] = 0;
	hsp[1] = 0;
}