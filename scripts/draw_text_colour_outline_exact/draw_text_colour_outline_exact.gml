///@description Draw text with a fixed outline with exact pixel rendering

///@param {Float} x
///@param {Float }y
///@param {String} text
///@param {Number} thickness
///@param {Colour} outline_color

var x1 = argument[0];
var y1 = argument[1];
var text = argument[2];
var size = 1;
var ol_col = c_black;

var draw_col = draw_get_colour();

if (argument_count >= 4){
	size = argument[3];
}

if (argument_count >= 5){
	ol_col = argument[4];
}

draw_set_color(ol_col);

var x2 = -size;
var y2 = -size;

repeat (1 + (size * 2)){
	repeat (1 + (size * 2)){
		if (x2 == 0) and (y2 == 0){
			x2++;
			continue;
		}
   
		draw_text(x1 + x2, y1 + y2, text);
   
		x2++;
	}
   
	y2++
	x2 = -size;
}
   
draw_set_color(draw_col);

draw_text(x1, y1, text);