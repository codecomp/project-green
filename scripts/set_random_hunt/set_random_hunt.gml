///@description Sets the active hunt to a new random hunt and restarts the timer
///@returns {Boolean}

if(instance_exists(obj_hunt)){
	ds_list_shuffle(obj_hunt.hunts);
	obj_hunt.alarm[0] = obj_hunt.duration;
	return true;
}

return false;