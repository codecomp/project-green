///@description Checks duration entity has beem movign towards a dropable ledge, changes state to move down ledge if duration has reached 35 steps
///@param {enum} state

if( drop_counter >= 35 ){
	drop_counter = 0;
	set_state(argument0);
} else {
	drop_counter++;
}