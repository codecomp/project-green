///@description Adds dialogue to existing dialogue entity or creates a new entity and loads in dialogue
///@param {String} text

if( !instance_exists(obj_dialogue) ){
	instance_create_layer(0, 0, "Instances", obj_dialogue);
}

var message;
message[0] = obj_dialogue.current_portrait;
message[1] = string_wordwrap_width(argument0, obj_dialogue.line_width, "¦", false);

ds_queue_enqueue(obj_dialogue.messages, message);