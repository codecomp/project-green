///@description Check if a needle, or one of an array fo needles is present in the given array

///@param {*} needle
///@param {Array} haystack

///@returns {Boolean}

///@author Codecomposer

var needle = argument0;
var haystack = argument1;

if( is_array(needle) ){
	for(var n=0; n<array_length_1d(needle); n++){
		for(var h=0; h<array_length_1d(haystack); h++){
			if( needle[n] == haystack[h] ){
				return true;
			}
		}
	}
} else {
	for(var h=0; h<array_length_1d(haystack); h++){
		if( needle == haystack[h] ){
			return true;
		}
	}
}

return false;