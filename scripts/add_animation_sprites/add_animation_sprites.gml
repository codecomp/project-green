///@description Setup sprite map for entities
///@param {Enum} state
///@param {Enum} up
///@param {enum} right
///@param {Enum} down
///@param {Enum} left

ds_map_add(sprites, string(argument0) + "-" + string(dir.up),	argument1);
ds_map_add(sprites, string(argument0) + "-" + string(dir.right),argument2);
ds_map_add(sprites, string(argument0) + "-" + string(dir.down),	argument3);
ds_map_add(sprites, string(argument0) + "-" + string(dir.left),	argument4);