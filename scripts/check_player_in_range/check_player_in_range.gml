///@description Check to see if the player is within sight range
///@returns {Boolean}

if( object_exists(obj_player) ){

	if( in_array(available_states, states.path) ){
	
	} else if(point_distance(x, y, obj_player.x, obj_player.y) < sight){
		return true;
	}
	
}

return false;