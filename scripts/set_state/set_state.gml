///@description Updates the current objects state, or optionally the target objects state
///@param {Enum} state
///@param {Object} [object]

if( argument_count > 1 ){
	with(argument[1]){
		state = argument[0];
	}
} else {
	state = argument[0];
}