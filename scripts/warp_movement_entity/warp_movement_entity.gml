///@description Moves an entity directly to a given position. If called without a third argument it will target the calling object
///@param {Number} x
///@param {Number} y
///@param {Object} [object]

if( argument_count > 2 ){
	with(argument[2]){
		x = argument[0];
		y = argument[1];
	}
} else {
	x = argument[0];
	y = argument[1];
}