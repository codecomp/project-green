///@description Move the object in the requested direction untill the object has cleared all the drop tiles
//@param {String} direction

// Calculate the checks so they can be stored in step end
check_top = tile_collide_check(dir.up);
check_right = tile_collide_check(dir.right);
check_bottom = tile_collide_check(dir.down);
check_left = tile_collide_check(dir.left);

// Check all to ease calculations
check_all = tile_collide_check("all");

var drop_speed = 0.7;

switch argument[0]{
	case dir.up:
		if( in_array([tiles.drop_up, tiles.drop_up_right, tiles.drop_up_left, tiles.drop_continuous], check_all) ){
			y -= drop_speed;
			airbourne = true;
		} else {
			set_state(states.idle);
			airbourne = false;
		}
		break;
	case dir.right:
		if( in_array([tiles.drop_right, tiles.drop_up_right, tiles.drop_down_right, tiles.drop_continuous], check_all) ){
			x += drop_speed;
			airbourne = true;
		} else {
			set_state(states.idle);
			airbourne = false;
		}
		break;
	case dir.down:
		if( in_array([tiles.drop_down, tiles.drop_down_right, tiles.drop_down_left, tiles.drop_continuous], check_all) ){
			y += drop_speed;
			airbourne = true;
		} else {
			set_state(states.idle);
			airbourne = false;
		}
		break;
	case dir.left:
		if( in_array([tiles.drop_left, tiles.drop_up_left, tiles.drop_down_left, tiles.drop_continuous], check_all) ){
			x -= drop_speed;
			airbourne = true;
		} else {
			set_state(states.idle);
			airbourne = false;
		}
		break;
}