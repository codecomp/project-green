///@description Swaps the appication pallet surface out

///@param {Id} Sprite

if( !sprite_exists(argument0) ){
	exit;
}

with(obj_application_surface){
	// Get texture from sprite
	palette_tex = sprite_get_texture(argument0, 0);

	// Calculate texture UVs
	uvs = sprite_get_uvs(argument0, 0);
	uvs_x = uvs[0];
	uvs_y = uvs[1];
	uvs_z = 1 / (uvs[2] - uvs[0]);
	uvs_w = 1 / (uvs[3] - uvs[1]);

	// Calculate the texel
	texel_x = texture_get_texel_width(palette_tex);
	texel_y = texture_get_texel_height(palette_tex);
}