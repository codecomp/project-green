///@description Trigger the camera to shake, can be used to start and stop by use of setting decay to 0 to last uindefinatly or setting intensity to 0 to stop instantly.
///@param {Float} intensity
///@param {Float} [decay]

with(obj_camera){
	shake_intensity = argument[0];
	if(argument_count > 1){
	    shake_decay = argument[1];
	} else {
		shake_decay = 0.1;
	}
}