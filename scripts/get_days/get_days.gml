///@returns {Integer} Days passed in game time

if(instance_exists(obj_time)){
	return obj_time.game_days;
}

return 0;