///@description Updates entity facing depending on state and directional velocity

if( state == states.path || state = states.orbit ){
	//Update based on x,y,xprevious,yprevious
	if( y > yprevious ){
		facing = dir.down;
	} else if( y < yprevious ){
		facing = dir.up;
	} else if( x > xprevious ){
		facing = dir.right;
	} else if( x < xprevious ){
		facing = dir.left;
	}
} else {
	//Update based on directional velocity
	if(vsp[0] > 0){
		facing = dir.down;
	} else if(vsp[0] < 0){
		facing = dir.up
	} else if(hsp[0] > 0){
		facing = dir.right;
	} else if(hsp[0] < 0){
		facing = dir.left
	}
}