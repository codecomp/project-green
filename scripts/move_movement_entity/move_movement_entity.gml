///@description This script updates the position of the movement entity according to its horizontal speeds and vertical speeds.

// If the object is set to static disable movement
if(static){
	exit;
}

#region Calaulate speed and co-ordinates

	// Get the total speeds
	var hspd = hsp[0]+hsp[1];
	var vspd = vsp[0]+vsp[1];

	// Work out our velocity modifier based on tile collisions
	if(!airbourne){
		update_velocity_friction_modifier(hspd, vspd);
	}

	// Apply friction
	hspd *= velocity_modifier;
	vspd *= velocity_modifier;

	// Reapply fractions
	hspd += hsp_fraction;
	vspd += vsp_fraction;

	// Store and remove fractions to work with whole numbers only for collisions
	hsp_fraction = hspd - (floor(abs(hspd)) * sign(hspd));
	hspd -= hsp_fraction;
	vsp_fraction = vspd - (floor(abs(vspd)) * sign(vspd));
	vspd -= vsp_fraction;
	
#endregion

#region Collide Y axis

	// Collide with solid objects
	var collision = instance_place(x, y+vspd, obj_solid);
	if( collision && collision.collide ){
		if( vspd < 0 ){
			y = collision.bbox_bottom + sprite_yoffset + 1;
		} else if( vspd > 0) {
			y = collision.bbox_top - sprite_yoffset;
		}
		stop_movement_entitiy(0);
	} else {
		y += vspd;
	}

	// Check tilemap collision
	check_top = tile_collide_check(dir.up);
	check_bottom = tile_collide_check(dir.down);

	// Vertical collisions
	if( vspd < 0 ){
		if( in_array([tiles.drop_up, tiles.drop_up_right, tiles.drop_up_left], check_top_previous) && !in_array([tiles.wall], check_top_previous) && !in_array([tiles.drop_up, tiles.drop_up_right, tiles.drop_up_left, tiles.wall], check_top) ){ // if we were on a drop up, and not moving into a wall
			collide_movement_entity(dir.up);// Move to drop edge
			check_top = check_top_previous; // Set current check to old top check to stop being able to walk over next step
			if(state = states.patrol_y){
				patrol_y = 0 - patrol_y;
			}
			stop_movement_entitiy(0);
		} else if( in_array([tiles.wall, tiles.drop_down, tiles.drop_continuous], check_top) ){ // If colliding with a wall move to the pixel adjacent to wall
			collide_movement_entity(dir.up);
			if(state = states.patrol_y){
				patrol_y = 0 - patrol_y;
			}
			stop_movement_entitiy(0);
		}
	} else if( vspd > 0) {
		if( in_array([tiles.drop_down, tiles.drop_down_right, tiles.drop_down_left], check_bottom_previous) && !in_array([tiles.wall], check_bottom_previous) && !in_array([tiles.drop_down, tiles.drop_down_right, tiles.drop_down_left, tiles.wall], check_bottom) ){  // if we were on a drop down, and not moving into a wall
			collide_movement_entity(dir.down); // Move to drop edge
			check_bottom = check_bottom_previous; // Set current check to old top check to stop being able to walk over next step
			if(state = states.patrol_y){
				patrol_y = 0 - patrol_y;
			}
			stop_movement_entitiy(0);
		} else if( in_array([tiles.wall, tiles.drop_up, tiles.drop_up_right, tiles.drop_up_left, tiles.drop_continuous], check_bottom) ){ // If colliding with a wall move to the pixel adjacent to wall
			collide_movement_entity(dir.down);
		
			if(state = states.patrol_y){
				patrol_y = 0 - patrol_y;
			}
			stop_movement_entitiy(0);
		}
	}

#endregion;

#region Colide X axis

	// Collide with solid objects
	var collision = instance_place(x+hspd, y, obj_solid);
	if( collision && collision.collide ){
		if( hspd < 0 ){
			x = collision.bbox_right + sprite_xoffset + 1;
		} else if( hspd > 0 ) {
			x = collision.bbox_left - sprite_xoffset;
		}
		stop_movement_entitiy(1);
	} else {
		x += hspd;
	}

	// Check tilemap collisions
	check_right = tile_collide_check(dir.right);
	check_left = tile_collide_check(dir.left);

	// Horizontal collisoons
	if( hspd < 0 ){
		if( in_array([tiles.drop_left, tiles.drop_up_left], check_left_previous) && !in_array([tiles.wall], check_left_previous) && !in_array([tiles.drop_left, tiles.drop_up_left, tiles.wall], check_left) ){ // if we were on a drop left, and not moving into a wall
			collide_movement_entity(dir.left); // Move to drop edge
			check_left = check_left_previous; // Set current check to old top check to stop being able to walk over next step
			if(state = states.patrol_x){
				patrol_x = 1;
			}
			stop_movement_entitiy(1);
		} else if( in_array([tiles.wall, tiles.drop_right, tiles.drop_up_right, tiles.drop_down_right, tiles.drop_continuous], check_left) ){ // If colliding with a wall move to the pixel adjacent to wall
			collide_movement_entity(dir.left);
			if(state = states.patrol_x){
				patrol_x = 1;
			}
			stop_movement_entitiy(1);
		}
	} else if( hspd > 0 ) {
		if( in_array([tiles.drop_right, tiles.drop_up_right], check_right_previous) && !in_array([tiles.wall], check_right_previous) && !in_array([tiles.drop_right, tiles.drop_up_right, tiles.wall], check_right) ){ // if we were on a drop right, and not moving into a wall
			collide_movement_entity(dir.right); // Move to drop edge
			check_right = check_right_previous; // Set current check to old top check to stop being able to walk over next step
			if(state = states.patrol_x){
				patrol_x = -1;
			}
			stop_movement_entitiy(1);
		} else if( in_array([tiles.wall, tiles.drop_left, tiles.drop_up_left, tiles.drop_down_left, tiles.drop_continuous], check_right) ){ // If colliding with a wall move to the pixel adjacent to wall
			collide_movement_entity(dir.right);
			if(state = states.patrol_x){
				patrol_x = -1;
			}
			stop_movement_entitiy(1);
		}
	}

#endregion

// Apply friction
hsp[0] = approach(hsp[0], 0, fric);
vsp[0] = approach(vsp[0], 0, fric);
    
hsp[1] = approach(hsp[1], 0, fric);
vsp[1] = approach(vsp[1], 0, fric);