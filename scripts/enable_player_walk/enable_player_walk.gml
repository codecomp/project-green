///@description Allows player entity to move

if( check_input(input.menu) )
	return;

// Get user input
var haccl = check_input(input.xaxis);
var vaccl = check_input(input.yaxis);

// Calculate the maximim speed to approach
var targethspd = maxhspd * sign(haccl);
var targetvspd = maxvspd * sign(vaccl);

// Set player speed to approach new speed
if(haccl != 0){
	hsp[0] = approach(hsp[0], targethspd, abs(accl));
}
if(vaccl != 0){
	vsp[0] = approach(vsp[0], targetvspd, abs(accl));
}

// Update player state based on collisions with specific tiles
update_player_movement_state();