#region Global definition
	global.tile_size = 4;
	global.c_1 = make_color_rgb(217, 232, 162);
	global.c_2 = make_color_rgb(176, 195, 77);
	global.c_3 = make_color_rgb(83, 127, 57);
	global.c_4 = make_color_rgb(32, 70, 50);
#endregion

#region Initialise system objects
	instance_create_layer(0, 0, "Instances", obj_application_surface);
	instance_create_layer(0, 0, "Instances", obj_console);
	instance_create_layer(0, 0, "Instances", obj_time);
	instance_create_layer(0, 0, "Instances", obj_pathfinder);
	instance_create_layer(0, 0, "Instances", obj_camera);
	instance_create_layer(0, 0, "Instances", obj_lighting);
	instance_create_layer(0, 0, "Instances", obj_pause);
	instance_create_layer(0, 0, "Instances", obj_inventory);
	instance_create_layer(0, 0, "Instances", obj_hud_life);
	instance_create_layer(0, 0, "Instances", obj_hud_counter);
	instance_create_layer(0, 0, "Instances", obj_hunt);
#endregion

#region Enumeration
	// Setup input IDs
	enum input{
		xaxis,
		yaxis,
		right,
		left,
		up,
		down,
		interact,
		attack,
		block,
		draw,
		dash,
		pause,
		menu
	}

	// Setup direction IDs
	enum dir {
		right,
		up,
		left,
		down
	}

	// Setup collision tileset IDs
	enum tiles{
		clear = 0,
		wall = 1,
		water = 2,
		slow = 3,
		drop_up = 4,
		drop_right = 5,
		drop_down = 6,
		drop_left = 7,
		drop_up_right = 8,
		drop_up_left = 9,
		drop_down_right = 10,
		drop_down_left = 11,
		drop_continuous = 12
	}

	// Setup state IDs
	enum states {
		idle,
		move,
		swim,
		attack,
		drop_up,
		drop_right,
		drop_down,
		drop_left,
		patrol_x,
		patrol_y,
		wander,
		converge,
		path,
		orbit,
		stagger,
		death,
	}
#endregion

// Move to game start room
room_goto(room_test);