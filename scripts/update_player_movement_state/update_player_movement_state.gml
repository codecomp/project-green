///@description Updates the player state based on collison with specific tiles eg. water for swimming and movement

// Update player state entering/exiting wayer tiles
if( in_array(tiles.water, check_top) || in_array(tiles.water, check_right) || in_array(tiles.water, check_bottom) || in_array(tiles.water, check_left) ){
	if( state != states.swim ){
		// Handle entering water
		set_state(states.swim);
	}
} else if( hsp[0] == 0 && vsp[0] == 0 ){
	// Handle stopping
	state = states.idle;
} else{
	// Handle moving
	state = states.move;
}