///@description Loop through every tile in the provided tile map and return a 2D array of the tile IDs

///@param {Number} tilemap_element_id

///@returns {Array}

///@author Codecomposer

var tilemap_id = argument0;
var tiles_x = tilemap_get_width(tilemap_id);
var tiles_y = tilemap_get_height(tilemap_id);
var tile_width = tilemap_get_tile_width(tilemap_id);
var tile_height = tilemap_get_tile_height(tilemap_id);
var tilemap_array = [];

for(var xx=0; xx<tiles_x; xx++){
	for(var yy=0; yy<tiles_y; yy++){
		tilemap_array[xx, yy] = tilemap_get_cell_id(tilemap_id, xx, yy, tile_width, tile_height);
	}
}

return tilemap_array;