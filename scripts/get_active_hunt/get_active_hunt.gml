///@description Returns the active hunt
///@returns {(DSList|Boolean)}

if(instance_exists(obj_hunt))
	return ds_list_find_value(obj_hunt.hunts, 0);
	
return false;