///@description Updates the sprite based on state and facing

if( ds_map_exists(sprites, string(state) + "-" + string(facing)) ){
	sprite_index = ds_map_find_value(sprites, string(state) + "-" + string(facing));
} else {
	sprite_index = ds_map_find_value(sprites, string(states.idle) + "-" + string(facing));
}