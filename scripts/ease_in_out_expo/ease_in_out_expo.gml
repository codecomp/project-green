///@description calculates a number on a Exponential ease in out curve

///@param {Real} t - Start time
///@param {Real} b - Base value
///@param {Real} c - Change in value
///@param {Real} d - duration

///@returns {Real}

var t = argument0;
var b = argument1;
var c = argument2;
var d = argument3;

t /= d/2;
if (t < 1) return c/2 * power( 2, 10 * (t - 1) ) + b;
t--;
return c/2 * ( -power( 2, -10 * t) + 2 ) + b;