///@description Push value(s) onto end of array

///@param {Array} array
///@param {...*} var

///@returns {Array}

///@author Codecomposer

var array = argument[0];
var len = array_length_1d(array);
var additiional = argument_count;

for(var i=1; i<additiional; i++){
	array[len+i-1] = argument[i];
}

return array;