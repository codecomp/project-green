///@description Get the current camera width for draw events
///@returns {Number}

if( !instance_exists(obj_camera) ){
	return 0;
}

return obj_camera.width;