///@Description Set the camera to follow a new object
///@param {Object} object

with(obj_camera) follow = argument0;