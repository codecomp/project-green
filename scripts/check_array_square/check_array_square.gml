///@description Recursivly check a co-ordinate in a provided array at greater and greater sizes (in squares) untill a cell is discovered containing the needle returning the distance from the origin to the first detected needle.
///@param {Number} start_iteration
///@param {Number} max_iteration
///@param {Array} array
///@param {Number} start_x
///@param {Number} start_y
///@returns {(Number|Array)}

var start_iteration = argument0;
var max_iteration = argument1;
var arr = argument2;
var xx = argument3;
var yy = argument4;

// Stop us going outside the array bounds
if( xx+start_iteration > array_height_2d(arr)-1 || yy+start_iteration > array_length_2d(arr, 0)-1 ){
	//show_debug_message("Out of bounds");
	return start_iteration;
}

// Check x axis
for(var i=0; i<start_iteration+1; i++){
	//show_debug_message("loop:" + string(i) + " X:" + string(xx+start_iteration) + " Y:" + string(yy+i));
	if(arr[xx+start_iteration, yy+i] == 1){
		return start_iteration;
	}
}

// check y axis
for(var i=0; i<start_iteration; i++){
	//show_debug_message("loop:" + string(i) + " X:" + string(xx+i) + " Y:" + string(yy+start_iteration));
	if(arr[xx+i, yy+start_iteration] == 1){
		return start_iteration;
	}
}


// Stop after reachignthe max iteration
if( start_iteration+1 == max_iteration ){
	return max_iteration;
}

return check_array_square(start_iteration+1, max_iteration, arr, xx, yy);