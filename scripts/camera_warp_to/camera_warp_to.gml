///@description Moves the camera instantly to a specific point in the room and removes the current follow target
///@param {Float} x
///@param {Float} y

var new_x = argument[0];
var new_y = argument[1];

with(obj_camera){
	follow = noone;
	x = new_x;
	x_to = new_x;
	y = new_y;
	y_to = new_y;
}