///@description Pans the camera to a specific point in the room and removes the current follow target
///@param {Float} x
///@param {Float} y

var new_x = argument[0];
var new_y = argument[1];

with(obj_camera){
	follow = noone;
	x_to = new_x;
	y_to = new_y;
}