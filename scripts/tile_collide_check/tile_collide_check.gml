///@description Returns an array of tiles that were collising with the requested direction of an objects bounding box

///@param {Const} direction

///@returns {Boolean}

// Setup variables for building out search arrays
var collision_width = bbox_right - bbox_left;
var collision_height = bbox_bottom - bbox_top;
var width_repeater = ceil(abs(collision_width / global.tile_size));
var height_repeater = ceil(abs(collision_height / global.tile_size));
var search = [];

switch argument0{

	case dir.up:
		for(var i=0; i<width_repeater; i++){
			search = array_push(search, [bbox_left + (i*global.tile_size), bbox_top]);
		}
		search = array_push(search, [bbox_right, bbox_top]);
		return tile_collide_at_points(get_collision_tilemap_id(), search);
		break;
	case dir.right:
		for(var i=0; i<width_repeater; i++){
			search = array_push(search, [bbox_right, bbox_top + (i*global.tile_size)]);
		}
		search = array_push(search, [bbox_right, bbox_bottom]);
		return tile_collide_at_points(get_collision_tilemap_id(), search);
		break;
	case dir.down:
		for(var i=0; i<height_repeater; i++){
			search = array_push(search, [bbox_left + (i*global.tile_size), bbox_bottom]);
		}
		search = array_push(search, [bbox_right, bbox_bottom]);
		return tile_collide_at_points(get_collision_tilemap_id(), search);
		break;
	case dir.left:
		for(var i=0; i<height_repeater; i++){
			search = array_push(search, [bbox_left, bbox_top + (i*global.tile_size)]);
		}
		search = array_push(search, [bbox_left, bbox_bottom]);
		return tile_collide_at_points(get_collision_tilemap_id(), search);
		break;
	case "center":
		return tile_collide_at_points(get_collision_tilemap_id(), [[x, y]]);
		break;
	case "all":
		return tile_collide_at_points(get_collision_tilemap_id(), [[bbox_left, bbox_top], [bbox_left, bbox_bottom], [bbox_right, bbox_top], [bbox_right, bbox_bottom], [x, y]]);
		break;
}