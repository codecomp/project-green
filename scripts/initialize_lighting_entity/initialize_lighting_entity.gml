///@description Setup entity variables to handle lighting
///@param {Float} min_intensity
///@param {Float} max_intensity
///@param {Float} [variation]

light_min = argument[0];
light_max = argument[1];
light_var = 2;
light_counter = 0;
light_size = light_min;

if(argument_count > 2){
	light_var = argument[2];
}