///@description Handle flipping scale of the sprite images based on direction and set sprites

if( facing != dir.right ){
	exit;
}

if( ds_map_exists(sprites, string(state) + "-" + string(dir.right)) && ds_map_find_value(sprites, string(state) + "-" + string(dir.left)) == ds_map_find_value(sprites, string(state) + "-" + string(dir.right)) ){
	image_xscale = -1 * image_xscale;
} else if( ds_map_find_value(sprites, string(states.idle) + "-" + string(dir.left)) == ds_map_find_value(sprites, string(states.idle) + "-" + string(dir.right)) ){
	image_xscale = -1 * image_xscale;
}