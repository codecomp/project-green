///@description Check collision tiles and movement to determine if the friction and velocity modifier need to be updates eg. if in water or a slow tile
///@param {Float} Horizontal speed
///@pratm {Float} Vertical speed

// Check if movement is diagonal and then modify valocity based on 1/sqrt(2)=sin(45)=cos(45)=0.707106781
if(argument[0] != 0 && argument[1] != 0){
	velocity_modifier = 0.707;
} else {
	velocity_modifier = 1;
}

// Update velocity based on slow collisions
if( in_array(tiles.slow, check_top) || in_array(tiles.slow, check_right) || in_array(tiles.slow, check_bottom) || in_array(tiles.slow, check_left) ){
	velocity_modifier *= 0.3;
}

// Update velocity based on water collisions
if( in_array(tiles.water, check_top) || in_array(tiles.water, check_right) || in_array(tiles.water, check_bottom) || in_array(tiles.water, check_left) ){
	velocity_modifier *= 0.5;
	fric = 0.02;
} else {
	fric = 0.1;
}