///@description Logs a value into the console  for tracking
///@param {Number, Foloat, String, Array} value

var value = argument0;

ds_list_add(obj_console.debug_log, value);