///@description Approach a value but not pass the value

///@param {Float} current
///@param {Float} target
///@param {Float} increment

///@returns {Float}

///@author Heartbeast

var current = argument0; // Current value
var target = argument1; // Target value
var increment = argument2; // Amount to approach each step

// approach the value but don't go over
if (current < target) {
    return min(current+increment, target); 
} else {
    return max(current-increment, target);
}