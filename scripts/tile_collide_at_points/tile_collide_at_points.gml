///@description Check arrays of co-ordinates on a specific tile map and return an array of the tile IDs that were detected at those points

///@param {Id} tile_map_id

///@param {Array} coordinates_arrays

var tile_map_id = argument0;
var coordinates = argument1;

// Found variable
var found = false;

// for the point arrays
var vector2_x = 0;
var vector2_y = 1;

// Setup return array
var result = [];

// Loop through the points and check for a tile
for (var i=0; i<array_length_1d(coordinates); i++) {
	var point = coordinates[i];
	result[array_length_1d(result)] = tilemap_get_at_pixel(tile_map_id, point[vector2_x], point[vector2_y]) & tile_index_mask;
}

// return found
return result;