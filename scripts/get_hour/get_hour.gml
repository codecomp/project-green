///@returns {Integer} Current in game hour

if(instance_exists(obj_time)){
	return obj_time.game_hour;
}

return 0;