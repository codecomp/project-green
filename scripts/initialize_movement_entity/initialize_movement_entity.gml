///@description Called in the create event of all entities that require movement and collision
///@param speed

// Input speeds
hsp[0] = 0;
vsp[0] = 0;

// Knockback speeds
hsp[1] = 0;
vsp[1] = 0;

// Speed fractions
hsp_fraction = 0;
vsp_fraction = 0;

// Movement info
fric = argument[0]
accl = argument[1];
maxhspd = argument[2];
maxvspd = argument[3];
velocity_modifier = 1;
airbourne = false;
static = false;
facing = dir.down;
patrol_x = -1;
patrol_y = -1;

// Setup collision check variables for the current frame
check_top = tile_collide_check(dir.up);
check_right = tile_collide_check(dir.right);
check_bottom = tile_collide_check(dir.down);
check_left = tile_collide_check(dir.left);

// Setup previous collision variables
check_top_previous = check_top;
check_right_previous = check_right;
check_bottom_previous = check_bottom;
check_left_previous = check_left;