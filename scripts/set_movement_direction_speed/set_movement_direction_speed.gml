///@description This script sets a direction and acceleration to a movement entity. This acceleration will NOT be relative to the previous speed of that entity.
///@param {Float} direction
///@param {Float} speed

var dir = argument[0];
var spd = argument[1];

if( argument_count > 2 ){
	with(argument[2]){
		hsp[1] = lengthdir_x(spd, dir);
		vsp[1] = lengthdir_y(spd, dir);
	}
} else {
	hsp[1] = lengthdir_x(spd, dir);
	vsp[1] = lengthdir_y(spd, dir);
}