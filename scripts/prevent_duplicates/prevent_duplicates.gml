///@description Prevents duplicate objects by destroying the current object is an instance of the same object exists

///@author Codecomposer

if( instance_number(object_index) > 1 ){
	instance_destroy(id, false);
}