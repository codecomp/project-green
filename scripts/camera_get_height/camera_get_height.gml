///@description Get the current camera height for draw events
///@returns {Number}

if( !instance_exists(obj_camera) ){
	return 0;
}

return obj_camera.height;