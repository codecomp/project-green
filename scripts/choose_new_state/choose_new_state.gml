///@description Choose a new state for the enemy object based on available states and other paramaters

// Don't update states for staggered or dead enemies
if(state == states.stagger || state == states.death){
	exit;
}

// Don't change state if we can still chase that pesky player!
if( state == states.converge && !check_player_out_range() ){
	exit;
}

// If a player has entered range give chase
if( ds_list_find_index(available_states, states.converge) && check_player_in_range() ){
	state = states.converge;
	exit;
}

// Get a randon state that does not require special conditions if it is time to change
if( alarm[0] == -1 ){
	// Set random accelerations for the enemy
	haccl = random(2)-1;
    vaccl = random(2)-1;
	
	// Get a random state
	state = ds_list_random_exclude(available_states, [states.path, states.converge]);
	
	// Reset the state change alarm
	alarm[0] = room_speed * 2;
}