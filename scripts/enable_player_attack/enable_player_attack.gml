///@description allows player to attack

if( !check_input(input.menu) && check_input(input.attack) ){
	switch(facing){
        case dir.up:
            xx = x-7;
            yy = y-11; 
            break;
        case dir.down:
            xx = x-7;
            yy = y+1; 
            break;
        case dir.left:
            xx = x-11;
            yy = y-7; 
            break;
        case dir.right:
            xx = x+1;
            yy = y-7; 
            break;
    }
	var damage = instance_create_depth(xx, yy, -10000, obj_damage);
    damage.creator = id;
	switch(facing){
        case dir.up:
        case dir.down:
            damage.image_xscale = 12;
			damage.image_yscale = 9;
            break;
        case dir.left:
        case dir.right:
			damage.image_xscale = 9;
			damage.image_yscale = 12;
			break;
    }
}