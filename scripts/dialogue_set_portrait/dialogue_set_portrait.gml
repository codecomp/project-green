///@description sets portrait on existing dialogue entity or creates a new entity and sets it
///@param {String} portrait

if( !instance_exists(obj_dialogue) ){
	instance_create_layer(0, 0, "Instances", obj_dialogue);
}

obj_dialogue.current_portrait = argument0;