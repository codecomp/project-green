///@description Position a movement entity against a slid collision tile and reset movement variables
///@param {String} direction

switch argument0{
	case dir.up:
		y = (bbox_top & ~(global.tile_size-1));
		y += global.tile_size+(y-bbox_top);
		vsp[0] = 0;
		vsp[1] = 0;
		break;
	case dir.right:
		x = bbox_right & ~(global.tile_size-1);
		x -= (bbox_right-x) + 1;
		hsp[0] = 0;
		hsp[1] = 0;
		break;
	case dir.down:
		y = (bbox_bottom & ~(global.tile_size-1));
		y -= ((bbox_bottom-y) + 1);
		vsp[0] = 0;
		vsp[1] = 0;
		break;
	case dir.left:
		x = bbox_left & ~(global.tile_size-1);
		x += global.tile_size+(x-bbox_left);
		hsp[0] = 0;
		hsp[1] = 0;
		break;
}