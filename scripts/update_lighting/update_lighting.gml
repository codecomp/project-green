///@description Update lighting variables to flicker light

light_counter++

// Only update every 4 frames
if(light_counter % 4 == 0){
	light_counter = 0;
	
	// Work out a random destination ligth size
	var update = choose(1*light_var, 0, -1*light_var);

	// Apply light size updates
	light_size += update;
	light_size = clamp(light_size, light_min, light_max);	
}