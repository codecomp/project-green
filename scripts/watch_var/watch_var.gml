///@description Watch a value in the console for constant tracking
///@param {String} caption
///@param {Number, Foloat, String, Array} value

var varName = argument0;
var value = argument1;
var count = 0;


for(var i=0; i<array_height_2d(obj_console.info); i++){
    if (obj_console.info[i, 0] == varName){
        obj_console.info[i, 1] = value;
    }
    else count++;
}

if (count == array_height_2d(obj_console.info)){
    obj_console.info[count, 0] = varName;
    obj_console.info[count, 1] = value;
}