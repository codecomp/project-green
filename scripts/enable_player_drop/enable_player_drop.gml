///@description Allows player entity to drop

// If not trying to move reset the drop counter
if(check_input(input.xaxis) == 0 && check_input(input.yaxis) == 0){
	drop_counter = 0;
}

if( in_array([tiles.drop_up, tiles.drop_up_right, tiles.drop_up_left], check_top_previous) && check_input(input.yaxis) < 0 ){
	check_dropable(states.drop_up); // Check drop status
} else if( in_array([tiles.drop_down, tiles.drop_down_right, tiles.drop_down_left], check_bottom_previous) && check_input(input.yaxis) > 0 ){
	check_dropable(states.drop_down); // Check drop status
} else if( in_array([tiles.drop_left, tiles.drop_up_left, tiles.drop_down_left], check_left_previous) && check_input(input.xaxis) < 0 ){
	check_dropable(states.drop_left); // Check drop status
} else if( in_array([tiles.drop_right, tiles.drop_up_right, tiles.drop_down_right], check_right_previous) && check_input(input.xaxis) > 0 ){
	check_dropable(states.drop_right); // Check drop status
} else {
	drop_counter = 0;
}