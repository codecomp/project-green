///@description Return the file tile id of the requested pixel on a specific tile map

///@param {Number} tilemap_id
///@param {Number} xx
///@param {Number} yy

///@returns {Number}

///@author Codecomposer

var tilemap_id = argument0;
var xx = argument1;
var yy = argument2;
var tile_width = argument3;
var tile_height = argument4;

return tilemap_get_at_pixel(tilemap_id, xx*tile_width, yy*tile_height);