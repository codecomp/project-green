///@Description Toggles weather the camera will clamp to the sides of the room or will flow out of bounds

with(obj_camera) room_lock = !room_lock;