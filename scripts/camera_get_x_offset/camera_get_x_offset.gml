///@description Get the current camera offset for draw events
///@returns {Float}

if( !instance_exists(obj_camera) ){
	return 0;
}

return obj_camera.x - obj_camera.width/2;