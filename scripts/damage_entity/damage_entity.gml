///@description Applys damage to target entity
///@param {Object} object
///@param {Float} damage

var damage = argument1;

with(argument0){
	// Handle invincibility
	if(hp == -1){
		exit;
	}
	
	// Lower objhects health
	hp = approach(hp, 0, damage);
	
	if(hp == 0){
		state = states.death;
	} else {
		state = states.stagger;
		alarm[1] = 0.08 * room_speed;
	}
}