///@description Check input key ro button is perssed
///@param {Enum} input
///@returns {Boolean}

var request = argument0;
var controller = 0;

// Stop user input when debug is open and in text input mode
if(global.debug && get_console_input_state())
	return false;

// Setup deadzone of the controller if conencted
if(gamepad_is_connected(controller)){
    gamepad_set_axis_deadzone(controller, .35);
}

// Handle working out the input the user is trying to use
switch (request){
	case input.xaxis:
		var xaxis = keyboard_check(vk_right) - keyboard_check(vk_left);
		if(gamepad_is_connected(0)){
			xaxis = gamepad_axis_value(controller, gp_axislh);
			if(xaxis == 0 && (gamepad_button_check(controller, gp_padr) || gamepad_button_check(controller, gp_padl))){
				xaxis = gamepad_button_check(controller, gp_padr) - gamepad_button_check(controller, gp_padl);
			}
		}
		return sign(xaxis);
		break;
	case input.yaxis:
		var yaxis = keyboard_check(vk_down) - keyboard_check(vk_up);
		if(gamepad_is_connected(0)){
			yaxis = gamepad_axis_value(controller, gp_axislv);
			if(yaxis == 0 && (gamepad_button_check(controller, gp_padd) || gamepad_button_check(controller, gp_padu))){
				yaxis = gamepad_button_check(controller, gp_padd) - gamepad_button_check(controller, gp_padu);
			}
		}
		return sign(yaxis);
		break;
	case input.right:
		if( keyboard_check(vk_right) || gamepad_axis_value(controller, gp_axislh) > 0 || gamepad_button_check(controller, gp_padr) ){
			return true;
		}
		break;
	case input.left:
		if( keyboard_check(vk_left) || gamepad_axis_value(controller, gp_axislh) < 0 || gamepad_button_check(controller, gp_padl) ){
			return true;
		}
		break;
	case input.down:
		if( keyboard_check(vk_down) || gamepad_axis_value(controller, gp_axislv) > 0 || gamepad_button_check(controller, gp_padd) ){
			return true;
		}
		break;
	case input.up:
		if( keyboard_check(vk_up) || gamepad_axis_value(controller, gp_axislv) < 0 || gamepad_button_check(controller, gp_padu) ){
			return true;
		}
		break;
	case input.interact:
		if( keyboard_check_pressed(vk_enter) || gamepad_button_check_pressed(controller, gp_face2) ){
			return true;
		}
		break;
	case input.attack:
		if( keyboard_check_pressed(ord("Z")) || gamepad_button_check_pressed(controller, gp_face2) ){
			return true;
		}
		break;
	case input.block:
		if( keyboard_check_pressed(ord("X")) || gamepad_button_check_pressed(controller, gp_face4) ){
			return true;
		}
		break;
	case input.draw:
		if( keyboard_check_pressed(ord("C")) || gamepad_button_check_pressed(controller, gp_face3) ){
			return true;
		}
		break;
	case input.dash:
		if( keyboard_check_pressed(ord("C")) || gamepad_button_check_pressed(controller, gp_shoulderl) ){
			return true;
		}
		break;
	case input.pause:
		if( keyboard_check_pressed(vk_escape) || gamepad_button_check_pressed(controller, gp_select) ){
			return true;
		}
		break;
	case input.menu:
		if( keyboard_check(vk_space) || gamepad_button_check(controller, gp_start) ){
			return true;
		}
		break;
}

return false;
