///@description Sets the roosm ambient lighting level

///@param {Integer} level - Lighting level 0, 1 or 2

if( instance_exists(obj_lighting) ){
	obj_lighting.ambient_light = argument0;
}