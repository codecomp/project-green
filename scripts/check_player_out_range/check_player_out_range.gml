///@description Check to see if the player has left chase range
///@returns {Boolean}

if( object_exists(obj_player) ){

	if( in_array(available_states, states.path) ){
		// Handle path enemies
	} else if(point_distance(x, y, obj_player.x, obj_player.y) < sight * chase_multiplier){
		return false;
	}
	
}

return true;