///@returns {Integer} Current in game minute

if(instance_exists(obj_time)){
	return obj_time.game_min;
}

return 0;