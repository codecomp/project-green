{
    "id": "25e3c3a1-2898-427e-bc3a-86f1cb51a11d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_swim_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "99f55b1d-aa1f-4e0b-88a8-c5eb494128f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25e3c3a1-2898-427e-bc3a-86f1cb51a11d",
            "compositeImage": {
                "id": "c92eab23-bda4-44d4-824c-a4c46a96fe77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99f55b1d-aa1f-4e0b-88a8-c5eb494128f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6750dfa-4b9e-4248-bbff-9dfb78885889",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99f55b1d-aa1f-4e0b-88a8-c5eb494128f6",
                    "LayerId": "4f1a2cf8-dd23-4afd-8767-f0d6758f6a70"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "4f1a2cf8-dd23-4afd-8767-f0d6758f6a70",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "25e3c3a1-2898-427e-bc3a-86f1cb51a11d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}