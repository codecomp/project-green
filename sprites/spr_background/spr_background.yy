{
    "id": "729e0245-7a0e-4f6d-b0f2-af555f15c6d1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f68e1af6-a1a7-49cd-8755-df0cd186c8ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "729e0245-7a0e-4f6d-b0f2-af555f15c6d1",
            "compositeImage": {
                "id": "faecdb3d-4b35-4dca-aaf1-84be528ed6ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f68e1af6-a1a7-49cd-8755-df0cd186c8ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bd2cc74-ebed-4952-bfbe-cfe4a586aa29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f68e1af6-a1a7-49cd-8755-df0cd186c8ec",
                    "LayerId": "0e0e86f8-d3e0-4df9-a474-9ee177448a38"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "0e0e86f8-d3e0-4df9-a474-9ee177448a38",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "729e0245-7a0e-4f6d-b0f2-af555f15c6d1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}