{
    "id": "f95e608d-0423-42c4-9b61-876415892446",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_palette_15",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "67cd08cb-0292-4e0f-9146-a7e47e4fca2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f95e608d-0423-42c4-9b61-876415892446",
            "compositeImage": {
                "id": "320656cd-9e28-45d7-bab5-5050195ab0ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67cd08cb-0292-4e0f-9146-a7e47e4fca2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e6f2c88-7d5b-4077-97ae-c5d42ea4a312",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67cd08cb-0292-4e0f-9146-a7e47e4fca2c",
                    "LayerId": "03e42173-ad90-44be-99ff-435e4b11c6ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "03e42173-ad90-44be-99ff-435e4b11c6ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f95e608d-0423-42c4-9b61-876415892446",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 0,
    "yorig": 0
}