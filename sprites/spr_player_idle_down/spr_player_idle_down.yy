{
    "id": "a8221807-362e-402d-92db-86dc1d566515",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_idle_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2b9d5cc4-e853-4c8c-abdc-b8c722676dd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8221807-362e-402d-92db-86dc1d566515",
            "compositeImage": {
                "id": "cae48149-7eb8-4d38-80e1-5fda6abdf265",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b9d5cc4-e853-4c8c-abdc-b8c722676dd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0eb456e1-d09e-451f-bd97-4f2f38c8c9b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b9d5cc4-e853-4c8c-abdc-b8c722676dd0",
                    "LayerId": "10590414-1149-4ea3-ad5e-3997c194fdef"
                }
            ]
        },
        {
            "id": "ca8a45c3-78ce-4d01-9c74-cca967c05ba4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8221807-362e-402d-92db-86dc1d566515",
            "compositeImage": {
                "id": "dc78372d-abd8-4de1-8fff-7943f2b43eaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca8a45c3-78ce-4d01-9c74-cca967c05ba4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "862b6964-aaf8-4b20-8f40-2b733bcad0a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca8a45c3-78ce-4d01-9c74-cca967c05ba4",
                    "LayerId": "10590414-1149-4ea3-ad5e-3997c194fdef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "10590414-1149-4ea3-ad5e-3997c194fdef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a8221807-362e-402d-92db-86dc1d566515",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}