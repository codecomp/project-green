{
    "id": "dee9a3eb-a5e1-4ed0-8dcd-1f2cfb6ea76b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_swim_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "2d2848f2-47db-491e-bd6e-42a0d290e94f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dee9a3eb-a5e1-4ed0-8dcd-1f2cfb6ea76b",
            "compositeImage": {
                "id": "48e863c8-e650-4131-a435-c3a3e4887e46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d2848f2-47db-491e-bd6e-42a0d290e94f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fcc36ed-5932-4e2c-9a34-dc3f43ca4d17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d2848f2-47db-491e-bd6e-42a0d290e94f",
                    "LayerId": "c787db29-ffed-44ce-ad04-6ebba038d7b4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "c787db29-ffed-44ce-ad04-6ebba038d7b4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dee9a3eb-a5e1-4ed0-8dcd-1f2cfb6ea76b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}