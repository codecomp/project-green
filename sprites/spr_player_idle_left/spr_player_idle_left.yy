{
    "id": "d2f9e84a-692f-41fb-aa0a-bd44d8c9195b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_idle_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "73e48dc1-26e5-4c72-afa4-bcb0bac94e5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2f9e84a-692f-41fb-aa0a-bd44d8c9195b",
            "compositeImage": {
                "id": "931f3044-0e88-4e1f-a2d6-3b8bfe793480",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73e48dc1-26e5-4c72-afa4-bcb0bac94e5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c3cbf4c-7a26-4017-9854-8790decb265e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73e48dc1-26e5-4c72-afa4-bcb0bac94e5c",
                    "LayerId": "4c882b33-715a-4531-801f-219bf1b39b98"
                }
            ]
        },
        {
            "id": "428e3382-ae2b-47ef-8e21-2fda8b5aca38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2f9e84a-692f-41fb-aa0a-bd44d8c9195b",
            "compositeImage": {
                "id": "628d3390-949d-4c8b-9ed7-596ada7ece36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "428e3382-ae2b-47ef-8e21-2fda8b5aca38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1e91e58-80a1-4b5e-9e50-5442adbc1ee3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "428e3382-ae2b-47ef-8e21-2fda8b5aca38",
                    "LayerId": "4c882b33-715a-4531-801f-219bf1b39b98"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "4c882b33-715a-4531-801f-219bf1b39b98",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d2f9e84a-692f-41fb-aa0a-bd44d8c9195b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}