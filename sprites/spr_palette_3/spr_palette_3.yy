{
    "id": "f2e6eaf8-b245-415c-b72e-e13dc7d28637",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_palette_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7480ba9d-1618-4621-a033-b567a1ad1fbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2e6eaf8-b245-415c-b72e-e13dc7d28637",
            "compositeImage": {
                "id": "4915bca3-5bdf-44c5-b0ff-b41be9bc2127",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7480ba9d-1618-4621-a033-b567a1ad1fbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20ba9306-fec6-4166-b9e6-0b8106c018cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7480ba9d-1618-4621-a033-b567a1ad1fbc",
                    "LayerId": "2d0fb0fa-5020-45a6-9570-7d02d8d158f1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "2d0fb0fa-5020-45a6-9570-7d02d8d158f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f2e6eaf8-b245-415c-b72e-e13dc7d28637",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 0,
    "yorig": 0
}