{
    "id": "90e035e7-a629-4fb3-9534-39fb75029735",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hud_seal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a386fe49-184c-4b95-82ef-65a8c3bced00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90e035e7-a629-4fb3-9534-39fb75029735",
            "compositeImage": {
                "id": "74cd6d8c-7585-475b-9255-84fa1d2efaa3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a386fe49-184c-4b95-82ef-65a8c3bced00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3992cf30-9826-4f85-91ef-ba25369b5c8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a386fe49-184c-4b95-82ef-65a8c3bced00",
                    "LayerId": "11baf91f-8255-48bc-88e7-5c0d36bdfc94"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 14,
    "layers": [
        {
            "id": "11baf91f-8255-48bc-88e7-5c0d36bdfc94",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90e035e7-a629-4fb3-9534-39fb75029735",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 4,
    "yorig": 7
}