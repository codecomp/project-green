{
    "id": "927b5403-0da6-45e3-a2d1-c7595a635c92",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hud",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 4,
    "bbox_right": 132,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "14391244-e4ab-4824-bf55-074a3440a749",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "927b5403-0da6-45e3-a2d1-c7595a635c92",
            "compositeImage": {
                "id": "710ed62a-a737-41dc-80c0-71fdf805191b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14391244-e4ab-4824-bf55-074a3440a749",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8fb02fd-cf94-4c85-a71a-1ccff02dd65e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14391244-e4ab-4824-bf55-074a3440a749",
                    "LayerId": "668293f5-a7b0-4848-a67f-b771485a1d39"
                },
                {
                    "id": "a360c404-b798-4241-89f8-a967e47d9f23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14391244-e4ab-4824-bf55-074a3440a749",
                    "LayerId": "4b485fe0-8987-4e89-8a20-e7b77853fcfb"
                },
                {
                    "id": "b69f1189-282a-49ea-be4b-8665b03575e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14391244-e4ab-4824-bf55-074a3440a749",
                    "LayerId": "ca051528-ba68-41f3-8a9f-3d2fd84d1856"
                },
                {
                    "id": "68acb280-9466-4ea1-9b88-d7b1394e550e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14391244-e4ab-4824-bf55-074a3440a749",
                    "LayerId": "8f32fb9a-5eaf-42c9-a47a-64b8a770a432"
                },
                {
                    "id": "b4a7eadd-94a1-43fa-ad15-e55bfec760ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14391244-e4ab-4824-bf55-074a3440a749",
                    "LayerId": "daca852c-2f94-4920-837f-0128323eecc0"
                },
                {
                    "id": "700d16c2-fd6a-49e1-a312-83d4e38dca58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14391244-e4ab-4824-bf55-074a3440a749",
                    "LayerId": "2ceb692f-dec9-4229-b4ed-c94d6b1c6ff0"
                },
                {
                    "id": "eef106c1-ed96-41d7-a111-331669daa2ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14391244-e4ab-4824-bf55-074a3440a749",
                    "LayerId": "905b2931-f0a6-48c9-bf20-91668e094142"
                }
            ]
        }
    ],
    "gridX": 1,
    "gridY": 1,
    "height": 144,
    "layers": [
        {
            "id": "905b2931-f0a6-48c9-bf20-91668e094142",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "927b5403-0da6-45e3-a2d1-c7595a635c92",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "2ceb692f-dec9-4229-b4ed-c94d6b1c6ff0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "927b5403-0da6-45e3-a2d1-c7595a635c92",
            "blendMode": 0,
            "isLocked": true,
            "name": "Text",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "daca852c-2f94-4920-837f-0128323eecc0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "927b5403-0da6-45e3-a2d1-c7595a635c92",
            "blendMode": 0,
            "isLocked": true,
            "name": "Text area",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "8f32fb9a-5eaf-42c9-a47a-64b8a770a432",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "927b5403-0da6-45e3-a2d1-c7595a635c92",
            "blendMode": 0,
            "isLocked": true,
            "name": "shard",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "668293f5-a7b0-4848-a67f-b771485a1d39",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "927b5403-0da6-45e3-a2d1-c7595a635c92",
            "blendMode": 0,
            "isLocked": true,
            "name": "Heart",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "4b485fe0-8987-4e89-8a20-e7b77853fcfb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "927b5403-0da6-45e3-a2d1-c7595a635c92",
            "blendMode": 0,
            "isLocked": true,
            "name": "Bar",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "ca051528-ba68-41f3-8a9f-3d2fd84d1856",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "927b5403-0da6-45e3-a2d1-c7595a635c92",
            "blendMode": 0,
            "isLocked": true,
            "name": "Layer 3",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}