{
    "id": "21109dbc-b243-4461-b3ec-43a7d162c942",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portcullis_light",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a00646ca-6837-404e-b67f-5350a3663530",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21109dbc-b243-4461-b3ec-43a7d162c942",
            "compositeImage": {
                "id": "d6911138-3431-4591-8ab2-f1a8d3359122",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a00646ca-6837-404e-b67f-5350a3663530",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73e6480d-d704-482a-bbe3-ca5b136ae472",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a00646ca-6837-404e-b67f-5350a3663530",
                    "LayerId": "c8568944-1084-4105-8327-89628fc8421a"
                }
            ]
        },
        {
            "id": "8f63343e-6e9c-46a1-ad24-9adfea7993a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21109dbc-b243-4461-b3ec-43a7d162c942",
            "compositeImage": {
                "id": "020ccccd-6eb3-418e-837a-c43d24d6503c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f63343e-6e9c-46a1-ad24-9adfea7993a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83a1e4e6-6a63-4dd5-9fad-c86ab1230aaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f63343e-6e9c-46a1-ad24-9adfea7993a4",
                    "LayerId": "c8568944-1084-4105-8327-89628fc8421a"
                }
            ]
        },
        {
            "id": "83984ac8-51ae-4ff5-a525-6b9e3e5ca7a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21109dbc-b243-4461-b3ec-43a7d162c942",
            "compositeImage": {
                "id": "2497d5c5-3d12-48b5-a459-ee2fabdaeafc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83984ac8-51ae-4ff5-a525-6b9e3e5ca7a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f1b344f-a629-494a-b01b-658c22b9fc42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83984ac8-51ae-4ff5-a525-6b9e3e5ca7a1",
                    "LayerId": "c8568944-1084-4105-8327-89628fc8421a"
                }
            ]
        },
        {
            "id": "718830ea-b2a6-4f58-86f5-fce8f665b9ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21109dbc-b243-4461-b3ec-43a7d162c942",
            "compositeImage": {
                "id": "31889aff-28d1-4088-b5c0-c497c3dc0d8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "718830ea-b2a6-4f58-86f5-fce8f665b9ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3d7fc6f-91c0-45e2-8feb-130d1bfdc2c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "718830ea-b2a6-4f58-86f5-fce8f665b9ba",
                    "LayerId": "c8568944-1084-4105-8327-89628fc8421a"
                }
            ]
        },
        {
            "id": "8c73f274-fff7-4db5-81a4-3c15ff665f8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21109dbc-b243-4461-b3ec-43a7d162c942",
            "compositeImage": {
                "id": "304cb035-0f5a-4777-955a-af5403a3c217",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c73f274-fff7-4db5-81a4-3c15ff665f8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d192400b-87c2-441f-aef8-ad645e982581",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c73f274-fff7-4db5-81a4-3c15ff665f8f",
                    "LayerId": "c8568944-1084-4105-8327-89628fc8421a"
                }
            ]
        },
        {
            "id": "42bb27f1-3ee2-49f5-acba-b43c9242e6e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21109dbc-b243-4461-b3ec-43a7d162c942",
            "compositeImage": {
                "id": "7b8a573c-1bda-490c-b862-dec75e52d629",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42bb27f1-3ee2-49f5-acba-b43c9242e6e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15fd6485-ce56-46ea-a22d-c76d738ef105",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42bb27f1-3ee2-49f5-acba-b43c9242e6e8",
                    "LayerId": "c8568944-1084-4105-8327-89628fc8421a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c8568944-1084-4105-8327-89628fc8421a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "21109dbc-b243-4461-b3ec-43a7d162c942",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}