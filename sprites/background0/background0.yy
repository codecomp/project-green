{
    "id": "bf3bc516-4871-4c09-a675-bdb2e99be6dc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background0",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "1bbbccca-aad3-4fb7-aa31-041bce375fe8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf3bc516-4871-4c09-a675-bdb2e99be6dc",
            "compositeImage": {
                "id": "4695ce15-6e43-4736-97f4-edc6c2c03a23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bbbccca-aad3-4fb7-aa31-041bce375fe8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d23f51b-1ceb-4e67-85f2-860e753fa0b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bbbccca-aad3-4fb7-aa31-041bce375fe8",
                    "LayerId": "8f71e579-7047-4699-af78-ed9dc34fd6f7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8f71e579-7047-4699-af78-ed9dc34fd6f7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bf3bc516-4871-4c09-a675-bdb2e99be6dc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}