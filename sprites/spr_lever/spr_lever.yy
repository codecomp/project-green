{
    "id": "fa8ef877-5913-41f9-a121-2fde86afb2e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lever",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c640a1f7-ce35-4000-acaa-3cc5a13c6978",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa8ef877-5913-41f9-a121-2fde86afb2e8",
            "compositeImage": {
                "id": "cbce1e19-d1af-4643-b591-d4eda0fff66a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c640a1f7-ce35-4000-acaa-3cc5a13c6978",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7718e69c-0f2b-481a-8d1c-367b7191134f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c640a1f7-ce35-4000-acaa-3cc5a13c6978",
                    "LayerId": "e738391e-2188-407a-b3a4-f38b814e2ec2"
                }
            ]
        },
        {
            "id": "be371e82-852f-4860-938e-0c0a2a5dc997",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa8ef877-5913-41f9-a121-2fde86afb2e8",
            "compositeImage": {
                "id": "ed83268f-8cb3-485f-8fe4-b9fffdd2eda2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be371e82-852f-4860-938e-0c0a2a5dc997",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "131e3ce2-5871-4917-9f0f-beeaa14131cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be371e82-852f-4860-938e-0c0a2a5dc997",
                    "LayerId": "e738391e-2188-407a-b3a4-f38b814e2ec2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e738391e-2188-407a-b3a4-f38b814e2ec2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fa8ef877-5913-41f9-a121-2fde86afb2e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}