{
    "id": "f945022a-8b8b-4d05-b0f3-510061739371",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_palette_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cdca741e-c761-4a56-a234-2f5e9aaa619d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f945022a-8b8b-4d05-b0f3-510061739371",
            "compositeImage": {
                "id": "b2e7ac7f-1744-4729-bea6-bc06a7caf646",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdca741e-c761-4a56-a234-2f5e9aaa619d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "637fd071-6acb-4083-854b-198cfd69a072",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdca741e-c761-4a56-a234-2f5e9aaa619d",
                    "LayerId": "e7123f2d-5363-42d2-8956-941c5ef4adff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "e7123f2d-5363-42d2-8956-941c5ef4adff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f945022a-8b8b-4d05-b0f3-510061739371",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 0,
    "yorig": 0
}