{
    "id": "21127485-7517-4289-9131-9251f72f2c6f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_coin_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a5e9ae4b-fa1c-4c6d-bfed-973b48b780c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21127485-7517-4289-9131-9251f72f2c6f",
            "compositeImage": {
                "id": "04ca008a-14f8-43bb-a72a-f2072be64f42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5e9ae4b-fa1c-4c6d-bfed-973b48b780c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "597471a3-3a90-4739-af1d-572d824c5b84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5e9ae4b-fa1c-4c6d-bfed-973b48b780c2",
                    "LayerId": "48ad7e7d-72e2-4616-97fa-1bab707839bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "48ad7e7d-72e2-4616-97fa-1bab707839bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "21127485-7517-4289-9131-9251f72f2c6f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}