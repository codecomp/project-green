{
    "id": "d301bfd9-db73-461f-ac56-b11ee88df57b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_transition",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d5d72892-b80c-4d9a-8214-a4c4e6f4e94d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d301bfd9-db73-461f-ac56-b11ee88df57b",
            "compositeImage": {
                "id": "5641f498-0517-471e-a2bf-4d37289628e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5d72892-b80c-4d9a-8214-a4c4e6f4e94d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f883526-f74c-4716-b34d-bbec60ae131f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5d72892-b80c-4d9a-8214-a4c4e6f4e94d",
                    "LayerId": "785efc1e-f2c4-46d2-a4e4-0d9cb8463e0c"
                }
            ]
        },
        {
            "id": "b1b9c040-dcd9-4297-a340-8b03ca71274e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d301bfd9-db73-461f-ac56-b11ee88df57b",
            "compositeImage": {
                "id": "b1fd3631-dda5-40f2-9ad7-808da8c51b51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1b9c040-dcd9-4297-a340-8b03ca71274e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9e89ce2-1b55-44a5-b19f-6a4a5cdb65be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1b9c040-dcd9-4297-a340-8b03ca71274e",
                    "LayerId": "785efc1e-f2c4-46d2-a4e4-0d9cb8463e0c"
                }
            ]
        },
        {
            "id": "6be7d5c2-02d2-425e-a0b2-ec83de33af27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d301bfd9-db73-461f-ac56-b11ee88df57b",
            "compositeImage": {
                "id": "8f27b6e0-d2cd-49b5-8c07-4d70eed5cd84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6be7d5c2-02d2-425e-a0b2-ec83de33af27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ce535e5-333a-4b92-9c47-96fa1f15799d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6be7d5c2-02d2-425e-a0b2-ec83de33af27",
                    "LayerId": "785efc1e-f2c4-46d2-a4e4-0d9cb8463e0c"
                }
            ]
        },
        {
            "id": "6c6dbd67-475b-45af-88bc-7c143cd8271e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d301bfd9-db73-461f-ac56-b11ee88df57b",
            "compositeImage": {
                "id": "19d34dc1-5694-43d9-b349-17223697016d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c6dbd67-475b-45af-88bc-7c143cd8271e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a460a84f-7fbf-4fdc-8f23-552b60429fb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c6dbd67-475b-45af-88bc-7c143cd8271e",
                    "LayerId": "785efc1e-f2c4-46d2-a4e4-0d9cb8463e0c"
                }
            ]
        },
        {
            "id": "98ecba2d-7975-49e1-9aa6-5fa903c2b20b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d301bfd9-db73-461f-ac56-b11ee88df57b",
            "compositeImage": {
                "id": "71db554f-9057-40f2-aba5-80b8f469183c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98ecba2d-7975-49e1-9aa6-5fa903c2b20b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f62dc9b3-1e3d-470e-88b0-72d2f2b25af8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98ecba2d-7975-49e1-9aa6-5fa903c2b20b",
                    "LayerId": "785efc1e-f2c4-46d2-a4e4-0d9cb8463e0c"
                }
            ]
        },
        {
            "id": "f760e272-1786-4f8d-b36b-7ef1dec28968",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d301bfd9-db73-461f-ac56-b11ee88df57b",
            "compositeImage": {
                "id": "226cb999-ce2f-4769-9310-bf4af25bbcc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f760e272-1786-4f8d-b36b-7ef1dec28968",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "475025ae-81ca-44ad-8310-dbfc8f743052",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f760e272-1786-4f8d-b36b-7ef1dec28968",
                    "LayerId": "785efc1e-f2c4-46d2-a4e4-0d9cb8463e0c"
                }
            ]
        },
        {
            "id": "95399754-3e56-4092-aaab-75cc1ed5671c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d301bfd9-db73-461f-ac56-b11ee88df57b",
            "compositeImage": {
                "id": "98f53f2b-d953-44e8-af66-7b38d07e934a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95399754-3e56-4092-aaab-75cc1ed5671c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36135842-1cb1-4bda-8203-10d3531008b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95399754-3e56-4092-aaab-75cc1ed5671c",
                    "LayerId": "785efc1e-f2c4-46d2-a4e4-0d9cb8463e0c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "785efc1e-f2c4-46d2-a4e4-0d9cb8463e0c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d301bfd9-db73-461f-ac56-b11ee88df57b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 25,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}