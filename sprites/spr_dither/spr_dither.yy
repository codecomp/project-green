{
    "id": "83dfb65d-a2e3-42ce-8249-c844ccc1eb59",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dither",
    "For3D": true,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "fdd2a717-da9c-4ab1-afe7-17dc47454ada",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83dfb65d-a2e3-42ce-8249-c844ccc1eb59",
            "compositeImage": {
                "id": "f207b964-62a5-4f95-9a92-ceccf642dff1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdd2a717-da9c-4ab1-afe7-17dc47454ada",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb1f304a-c118-4e9c-983d-6144be527e8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdd2a717-da9c-4ab1-afe7-17dc47454ada",
                    "LayerId": "8a3ab27b-a496-4e28-814f-4d09ed12c2c1"
                }
            ]
        }
    ],
    "gridX": 4,
    "gridY": 4,
    "height": 8,
    "layers": [
        {
            "id": "8a3ab27b-a496-4e28-814f-4d09ed12c2c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83dfb65d-a2e3-42ce-8249-c844ccc1eb59",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}