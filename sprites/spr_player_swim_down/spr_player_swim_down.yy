{
    "id": "dc02be4b-b45b-46df-8879-35aece4d7297",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_swim_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "aa5bd204-af2c-4ca4-ae2d-16aa32a85383",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc02be4b-b45b-46df-8879-35aece4d7297",
            "compositeImage": {
                "id": "75798a59-95a3-45e7-96d8-bc0230093c53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa5bd204-af2c-4ca4-ae2d-16aa32a85383",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4988e7e-acbb-480d-82dd-be6f279df41a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa5bd204-af2c-4ca4-ae2d-16aa32a85383",
                    "LayerId": "27639d47-a29a-4025-a1b6-b5c278e29f60"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "27639d47-a29a-4025-a1b6-b5c278e29f60",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc02be4b-b45b-46df-8879-35aece4d7297",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}