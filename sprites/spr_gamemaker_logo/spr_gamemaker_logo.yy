{
    "id": "7665fdbe-85bc-44a2-828b-c921e74537a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gamemaker_logo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 34,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a4b457a9-6c3a-4107-82b2-66b3e2c97d81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7665fdbe-85bc-44a2-828b-c921e74537a9",
            "compositeImage": {
                "id": "81dead55-6457-4d0a-bf34-f30630a6d0aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4b457a9-6c3a-4107-82b2-66b3e2c97d81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fb85c63-6b64-4a21-b126-ddf8850164e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4b457a9-6c3a-4107-82b2-66b3e2c97d81",
                    "LayerId": "40ddcc94-6b88-48d4-9510-e15af0e9bbca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 35,
    "layers": [
        {
            "id": "40ddcc94-6b88-48d4-9510-e15af0e9bbca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7665fdbe-85bc-44a2-828b-c921e74537a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 17
}