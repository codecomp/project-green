{
    "id": "d137302b-8878-41d1-b507-b0758860add0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nslice_special",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f323e946-b2de-4f04-885f-c700886a9eb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d137302b-8878-41d1-b507-b0758860add0",
            "compositeImage": {
                "id": "30187a89-4aed-42aa-8fd5-de1f7297f336",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f323e946-b2de-4f04-885f-c700886a9eb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c065d4a6-0627-489c-a0ec-43a115053831",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f323e946-b2de-4f04-885f-c700886a9eb0",
                    "LayerId": "ac676c6a-fb7d-4e28-85fc-a43a02a213b8"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 24,
    "layers": [
        {
            "id": "ac676c6a-fb7d-4e28-85fc-a43a02a213b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d137302b-8878-41d1-b507-b0758860add0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}