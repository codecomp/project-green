{
    "id": "4610b4d8-7636-4712-8f02-5408054b4e29",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_palette_10",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "46fd1d2b-bcfc-475a-b5f1-f59bb154aa7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4610b4d8-7636-4712-8f02-5408054b4e29",
            "compositeImage": {
                "id": "eec46cb3-b92b-4f86-bf2b-e47f51c5b0e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46fd1d2b-bcfc-475a-b5f1-f59bb154aa7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37bce64a-ba31-46fd-94c3-e3d0ce1ab39c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46fd1d2b-bcfc-475a-b5f1-f59bb154aa7f",
                    "LayerId": "e48eba1b-436b-4894-9de9-097775467e0c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "e48eba1b-436b-4894-9de9-097775467e0c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4610b4d8-7636-4712-8f02-5408054b4e29",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 0,
    "yorig": 0
}