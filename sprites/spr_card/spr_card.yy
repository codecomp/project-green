{
    "id": "72af4d9d-6d3a-483a-877f-e9d48c4029df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_card",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 0,
    "bbox_right": 81,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a6b6e09a-1a6d-4c5e-8fc5-ff61a3ec0aab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72af4d9d-6d3a-483a-877f-e9d48c4029df",
            "compositeImage": {
                "id": "4dbb7732-daf1-4e71-99f6-848af03d77b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6b6e09a-1a6d-4c5e-8fc5-ff61a3ec0aab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e7fbbf0-aad6-4889-965e-4b1ad09d2773",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6b6e09a-1a6d-4c5e-8fc5-ff61a3ec0aab",
                    "LayerId": "5f2c016e-b336-48f7-bf18-59cfbd41547b"
                },
                {
                    "id": "4755c2a0-1372-4da6-b051-f7911bb4be4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6b6e09a-1a6d-4c5e-8fc5-ff61a3ec0aab",
                    "LayerId": "ff250a9f-f87f-46fd-a38c-02eb6708e980"
                }
            ]
        }
    ],
    "gridX": 2,
    "gridY": 2,
    "height": 118,
    "layers": [
        {
            "id": "5f2c016e-b336-48f7-bf18-59cfbd41547b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "72af4d9d-6d3a-483a-877f-e9d48c4029df",
            "blendMode": 0,
            "isLocked": true,
            "name": "Border",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "ff250a9f-f87f-46fd-a38c-02eb6708e980",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "72af4d9d-6d3a-483a-877f-e9d48c4029df",
            "blendMode": 0,
            "isLocked": true,
            "name": "Background",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 82,
    "xorig": 0,
    "yorig": 0
}