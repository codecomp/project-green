{
    "id": "6f7a4dd2-f163-442a-a0a2-f2b56192a3fc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_collision",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0162109d-ff84-47d7-9096-d3080c7ac71d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f7a4dd2-f163-442a-a0a2-f2b56192a3fc",
            "compositeImage": {
                "id": "c708ce93-f6d8-4d02-a3c1-d3be36d68613",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0162109d-ff84-47d7-9096-d3080c7ac71d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34d3809b-5b31-4c90-b34a-4c1312135ca6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0162109d-ff84-47d7-9096-d3080c7ac71d",
                    "LayerId": "8ce07f90-2002-4e8c-baaa-1e57c5455a19"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8ce07f90-2002-4e8c-baaa-1e57c5455a19",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f7a4dd2-f163-442a-a0a2-f2b56192a3fc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}