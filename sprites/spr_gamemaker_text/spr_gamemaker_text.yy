{
    "id": "2acb8e3b-7f5e-4884-999c-fbcf722b09ef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gamemaker_text",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 146,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1ffb3944-0bb3-458f-b7de-0e5a891497c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2acb8e3b-7f5e-4884-999c-fbcf722b09ef",
            "compositeImage": {
                "id": "c465a387-8f82-4c9c-aa35-db0b46780f09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ffb3944-0bb3-458f-b7de-0e5a891497c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32ed13d2-bc2c-4eff-a185-b2395fb359ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ffb3944-0bb3-458f-b7de-0e5a891497c2",
                    "LayerId": "15fd7cf4-4af7-44f2-a831-5482dad313ae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "15fd7cf4-4af7-44f2-a831-5482dad313ae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2acb8e3b-7f5e-4884-999c-fbcf722b09ef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 147,
    "xorig": 73,
    "yorig": 8
}