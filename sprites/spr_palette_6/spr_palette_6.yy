{
    "id": "b233f26b-39e3-47c6-8750-01149ae4307b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_palette_6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "069c8cdc-6bdd-4f1e-83fe-4b2afb839f7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b233f26b-39e3-47c6-8750-01149ae4307b",
            "compositeImage": {
                "id": "7ec91664-e972-42c6-a36f-952f242df946",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "069c8cdc-6bdd-4f1e-83fe-4b2afb839f7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10d2bdb2-7260-4ef3-8d6c-c4f83a793c7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "069c8cdc-6bdd-4f1e-83fe-4b2afb839f7a",
                    "LayerId": "ec53040a-3d72-4467-b478-662244ad48f9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "ec53040a-3d72-4467-b478-662244ad48f9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b233f26b-39e3-47c6-8750-01149ae4307b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 0,
    "yorig": 0
}