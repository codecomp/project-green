{
    "id": "c3e4a489-e5bc-4ee3-a812-08aab37f25ae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hud_health_bar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 79,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "7cd69769-3c86-4d75-a6ce-3ceb73d0c348",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3e4a489-e5bc-4ee3-a812-08aab37f25ae",
            "compositeImage": {
                "id": "f5ae942b-2997-4c3a-8d43-49baa63c834c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cd69769-3c86-4d75-a6ce-3ceb73d0c348",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a0a6f53-fb94-4047-81ce-a08845c72935",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cd69769-3c86-4d75-a6ce-3ceb73d0c348",
                    "LayerId": "4d1cbfe3-125d-4c7e-9293-a9682e70c775"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "4d1cbfe3-125d-4c7e-9293-a9682e70c775",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c3e4a489-e5bc-4ee3-a812-08aab37f25ae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 0,
    "yorig": 3
}