{
    "id": "1281a17d-8b8e-42c5-8e57-fe2ab0d8fa16",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_coin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "4dab18d1-7905-41df-b9a4-e5524be7af6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1281a17d-8b8e-42c5-8e57-fe2ab0d8fa16",
            "compositeImage": {
                "id": "a6390716-ed23-4de7-b2f9-20b4631a5367",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dab18d1-7905-41df-b9a4-e5524be7af6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf0a36b9-6367-462f-9e5e-c53dc65c460a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dab18d1-7905-41df-b9a4-e5524be7af6b",
                    "LayerId": "938bb068-aa67-48d8-b82b-1f15bbae683b"
                }
            ]
        },
        {
            "id": "454f42bb-f7e1-428c-8a58-984a3e88144c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1281a17d-8b8e-42c5-8e57-fe2ab0d8fa16",
            "compositeImage": {
                "id": "8f8eccc8-c776-446c-9cd2-86f1c2e19097",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "454f42bb-f7e1-428c-8a58-984a3e88144c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84647a8a-d3d0-4625-abd8-22e500f81b5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "454f42bb-f7e1-428c-8a58-984a3e88144c",
                    "LayerId": "938bb068-aa67-48d8-b82b-1f15bbae683b"
                }
            ]
        },
        {
            "id": "77ac21a4-d8f8-45b5-a016-9f54f278f2f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1281a17d-8b8e-42c5-8e57-fe2ab0d8fa16",
            "compositeImage": {
                "id": "424868de-c020-40aa-b5e0-0bd4b38dc379",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77ac21a4-d8f8-45b5-a016-9f54f278f2f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da56dad1-fa91-4227-ab30-93732b3c852e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77ac21a4-d8f8-45b5-a016-9f54f278f2f5",
                    "LayerId": "938bb068-aa67-48d8-b82b-1f15bbae683b"
                }
            ]
        },
        {
            "id": "c7524d45-407b-4f23-ada5-98d3bbc13182",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1281a17d-8b8e-42c5-8e57-fe2ab0d8fa16",
            "compositeImage": {
                "id": "6e44e6e1-c063-4714-b59e-a25d8febc864",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7524d45-407b-4f23-ada5-98d3bbc13182",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24c99d96-8e17-4d02-933c-df1b76f8f554",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7524d45-407b-4f23-ada5-98d3bbc13182",
                    "LayerId": "938bb068-aa67-48d8-b82b-1f15bbae683b"
                }
            ]
        }
    ],
    "gridX": 16,
    "gridY": 16,
    "height": 4,
    "layers": [
        {
            "id": "938bb068-aa67-48d8-b82b-1f15bbae683b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1281a17d-8b8e-42c5-8e57-fe2ab0d8fa16",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 6.7,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 2,
    "yorig": 2
}