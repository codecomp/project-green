{
    "id": "c0d1e5f0-ae77-40cf-afcb-36c0029d3942",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_palette_16",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "502b9ecb-9349-40c6-9d1a-404d877d1ea4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0d1e5f0-ae77-40cf-afcb-36c0029d3942",
            "compositeImage": {
                "id": "98d9cb50-eb41-47c7-932d-084b13f694ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "502b9ecb-9349-40c6-9d1a-404d877d1ea4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05e9c18a-c8a8-4dc1-823c-d48505f60528",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "502b9ecb-9349-40c6-9d1a-404d877d1ea4",
                    "LayerId": "dc2e7d5d-5f26-4d27-aa08-697cfd3f7085"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "dc2e7d5d-5f26-4d27-aa08-697cfd3f7085",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c0d1e5f0-ae77-40cf-afcb-36c0029d3942",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 0,
    "yorig": 0
}