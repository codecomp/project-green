{
    "id": "30cb43df-2438-4139-95b6-cdb97b76d09c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_card_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 0,
    "bbox_right": 81,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "14fb9558-486c-4174-a685-d67803c1b4fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30cb43df-2438-4139-95b6-cdb97b76d09c",
            "compositeImage": {
                "id": "041d6f47-c5c4-4305-81a2-eede7d4dcf4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14fb9558-486c-4174-a685-d67803c1b4fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91943809-a96c-489a-bf69-25c7ecaf309f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14fb9558-486c-4174-a685-d67803c1b4fd",
                    "LayerId": "280321bf-4463-4fdf-9200-ab8d7c005deb"
                }
            ]
        }
    ],
    "gridX": 2,
    "gridY": 2,
    "height": 118,
    "layers": [
        {
            "id": "280321bf-4463-4fdf-9200-ab8d7c005deb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "30cb43df-2438-4139-95b6-cdb97b76d09c",
            "blendMode": 0,
            "isLocked": false,
            "name": "Background",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 82,
    "xorig": 0,
    "yorig": 0
}