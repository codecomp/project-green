{
    "id": "c537ef17-fdff-44bf-bc2f-18a694ae4039",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_slime_idle_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "50ca2d45-109a-4c61-8920-73bfbad66a05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c537ef17-fdff-44bf-bc2f-18a694ae4039",
            "compositeImage": {
                "id": "70e60ec8-f19e-42a5-aa46-71bf860796fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50ca2d45-109a-4c61-8920-73bfbad66a05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "433b3daf-7393-4d9c-a9e4-1b212e3edfdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50ca2d45-109a-4c61-8920-73bfbad66a05",
                    "LayerId": "9f82b339-9462-49f6-a1e9-a36df2409941"
                }
            ]
        },
        {
            "id": "ac6f6087-edae-40b5-aa5f-098434f48d05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c537ef17-fdff-44bf-bc2f-18a694ae4039",
            "compositeImage": {
                "id": "4f515e4d-484d-4e7b-b81e-689d0b13b781",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac6f6087-edae-40b5-aa5f-098434f48d05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcf29f12-9b9c-49e0-80ba-23bcf2abef4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac6f6087-edae-40b5-aa5f-098434f48d05",
                    "LayerId": "9f82b339-9462-49f6-a1e9-a36df2409941"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 5,
    "layers": [
        {
            "id": "9f82b339-9462-49f6-a1e9-a36df2409941",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c537ef17-fdff-44bf-bc2f-18a694ae4039",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 3,
    "yorig": 2
}