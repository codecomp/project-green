{
    "id": "287b658b-fc88-4627-bb5d-eb00de38b4bc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_move_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "0e369a28-5e40-4150-88f3-ce5f547f8b30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "287b658b-fc88-4627-bb5d-eb00de38b4bc",
            "compositeImage": {
                "id": "13ee450c-698f-4a5b-976a-5bc5bbd0fde0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e369a28-5e40-4150-88f3-ce5f547f8b30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1dd0164a-d0c8-46c3-893f-afb673b74517",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e369a28-5e40-4150-88f3-ce5f547f8b30",
                    "LayerId": "2d7a8282-f9c7-48bd-8065-b7ff1cb8c42c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "2d7a8282-f9c7-48bd-8065-b7ff1cb8c42c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "287b658b-fc88-4627-bb5d-eb00de38b4bc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}