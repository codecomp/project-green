{
    "id": "0c6817e8-03b0-4e10-b62c-e6697510674e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_coin_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "e8442c0a-2e65-4cc6-836c-d8e475bdfe8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c6817e8-03b0-4e10-b62c-e6697510674e",
            "compositeImage": {
                "id": "1b9e22b4-6661-4b41-9a3a-9a52f9d4b020",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8442c0a-2e65-4cc6-836c-d8e475bdfe8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61b3d91c-b08c-4d01-8b2b-26ce6727540f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8442c0a-2e65-4cc6-836c-d8e475bdfe8c",
                    "LayerId": "4a514712-a89a-45fb-8cb1-6dbe5089fc2b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "4a514712-a89a-45fb-8cb1-6dbe5089fc2b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0c6817e8-03b0-4e10-b62c-e6697510674e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 3,
    "yorig": 2
}