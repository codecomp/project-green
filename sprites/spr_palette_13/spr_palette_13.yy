{
    "id": "37188121-9212-493f-870e-0e2d3240e034",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_palette_13",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b5e8fd90-871e-4ae1-975c-d4018b30c82c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37188121-9212-493f-870e-0e2d3240e034",
            "compositeImage": {
                "id": "454e0119-7f54-40fe-a7ba-6d795fdd2259",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5e8fd90-871e-4ae1-975c-d4018b30c82c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50944e71-af9a-42cb-994a-1fc1e9513377",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5e8fd90-871e-4ae1-975c-d4018b30c82c",
                    "LayerId": "bf63f389-e375-4bf3-ae41-554e567dce89"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "bf63f389-e375-4bf3-ae41-554e567dce89",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "37188121-9212-493f-870e-0e2d3240e034",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 0,
    "yorig": 0
}