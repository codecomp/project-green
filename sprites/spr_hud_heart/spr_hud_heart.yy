{
    "id": "10e7a266-ffe9-4470-acd6-310dfe8fcea9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hud_heart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "b054bbbd-3c20-4e8c-b90f-38e9dac3d814",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e7a266-ffe9-4470-acd6-310dfe8fcea9",
            "compositeImage": {
                "id": "0481963e-8f02-47ae-ab33-68ff9e7a4205",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b054bbbd-3c20-4e8c-b90f-38e9dac3d814",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c2df88f-32b1-4cdd-a046-6c37916534c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b054bbbd-3c20-4e8c-b90f-38e9dac3d814",
                    "LayerId": "e7f4f698-b242-409c-b88e-14c1acdf42b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 13,
    "layers": [
        {
            "id": "e7f4f698-b242-409c-b88e-14c1acdf42b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "10e7a266-ffe9-4470-acd6-310dfe8fcea9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 7,
    "yorig": 6
}