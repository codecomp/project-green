{
    "id": "d34e59d8-1a5d-4b61-85aa-5a541f617817",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_palette_8",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2f26fe72-5cb2-42d7-a073-17392a942d6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d34e59d8-1a5d-4b61-85aa-5a541f617817",
            "compositeImage": {
                "id": "2992af9a-d332-4d06-b16d-c835f1592c59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f26fe72-5cb2-42d7-a073-17392a942d6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a67023af-a10c-4907-bc88-5e378e8a97b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f26fe72-5cb2-42d7-a073-17392a942d6e",
                    "LayerId": "df00370e-05a7-482a-8b17-608b60495462"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "df00370e-05a7-482a-8b17-608b60495462",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d34e59d8-1a5d-4b61-85aa-5a541f617817",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 0,
    "yorig": 0
}