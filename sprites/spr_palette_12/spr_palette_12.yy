{
    "id": "1d365c03-cb27-48ea-a5fa-ee7c6a8e8136",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_palette_12",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d2b55696-2c18-420d-af5d-3571628cabef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d365c03-cb27-48ea-a5fa-ee7c6a8e8136",
            "compositeImage": {
                "id": "9d67c7e0-8faa-4ccc-b48f-8091596d1921",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2b55696-2c18-420d-af5d-3571628cabef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b75bffb-5d8c-4579-a60c-c425621dab8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2b55696-2c18-420d-af5d-3571628cabef",
                    "LayerId": "2dbf5598-2e81-49ea-9208-b6746affa7f6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "2dbf5598-2e81-49ea-9208-b6746affa7f6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d365c03-cb27-48ea-a5fa-ee7c6a8e8136",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 0,
    "yorig": 0
}