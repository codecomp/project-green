{
    "id": "bf6b7087-58ae-4862-8497-6c548a843ceb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background1",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "b314b458-3df1-4d6a-a580-eedc35718bde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf6b7087-58ae-4862-8497-6c548a843ceb",
            "compositeImage": {
                "id": "19301035-9668-41a9-9f44-aff9c56192a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b314b458-3df1-4d6a-a580-eedc35718bde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7eaf0a0-1bf8-4e79-ac39-0d46330a46d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b314b458-3df1-4d6a-a580-eedc35718bde",
                    "LayerId": "80b64aa9-82c0-4308-995d-1db7067bf157"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "80b64aa9-82c0-4308-995d-1db7067bf157",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bf6b7087-58ae-4862-8497-6c548a843ceb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": -29,
    "yorig": 9
}