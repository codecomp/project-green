{
    "id": "6598280f-ba3a-4320-8a86-b667f8d53b58",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_weapon_sword",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "82163576-2012-4dde-a086-a99e680415f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6598280f-ba3a-4320-8a86-b667f8d53b58",
            "compositeImage": {
                "id": "fc29c29e-4e00-4999-9dc0-ab6bb4e3161c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82163576-2012-4dde-a086-a99e680415f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ab7370a-4ac3-472c-bab2-e297e9cabb6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82163576-2012-4dde-a086-a99e680415f7",
                    "LayerId": "be35e7d2-9b83-4c7b-8617-82c28b1c4ab7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "be35e7d2-9b83-4c7b-8617-82c28b1c4ab7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6598280f-ba3a-4320-8a86-b667f8d53b58",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}