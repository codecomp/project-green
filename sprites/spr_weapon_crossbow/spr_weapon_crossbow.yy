{
    "id": "0b661b73-a2e8-47f2-9ead-50bed12487b5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_weapon_crossbow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "58b56437-e306-4268-ab9f-57ef2b57c4c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b661b73-a2e8-47f2-9ead-50bed12487b5",
            "compositeImage": {
                "id": "d467148b-aff7-4cdc-a889-26e46e2173dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58b56437-e306-4268-ab9f-57ef2b57c4c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5f1455c-a094-4df4-8a4e-78854e449e2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58b56437-e306-4268-ab9f-57ef2b57c4c9",
                    "LayerId": "998e049f-d84b-4702-8807-9543a19862b6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "998e049f-d84b-4702-8807-9543a19862b6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b661b73-a2e8-47f2-9ead-50bed12487b5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}