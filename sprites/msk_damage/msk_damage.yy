{
    "id": "fa96587e-d5e1-47c2-9bda-516414aa426d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "msk_damage",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "9fe9bb6f-8c5c-4b1b-9bbf-ca56a80bd76e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa96587e-d5e1-47c2-9bda-516414aa426d",
            "compositeImage": {
                "id": "2adacdbd-2c5b-403c-abd0-e70d7a8b38aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fe9bb6f-8c5c-4b1b-9bbf-ca56a80bd76e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fc1d2d4-f246-4495-af81-2799d4aebca7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fe9bb6f-8c5c-4b1b-9bbf-ca56a80bd76e",
                    "LayerId": "f884be31-fbae-4cb3-820b-d07186132052"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "f884be31-fbae-4cb3-820b-d07186132052",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fa96587e-d5e1-47c2-9bda-516414aa426d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}