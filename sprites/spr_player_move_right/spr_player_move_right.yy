{
    "id": "556706cc-473e-4ae9-95c9-21257e4164b4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_move_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "1ab36626-143b-42c2-a9c4-491f107005a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "556706cc-473e-4ae9-95c9-21257e4164b4",
            "compositeImage": {
                "id": "0376eead-7b8a-4261-8410-a32da763aadc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ab36626-143b-42c2-a9c4-491f107005a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ff2494b-a80c-47d4-8571-3264b170b167",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ab36626-143b-42c2-a9c4-491f107005a8",
                    "LayerId": "daaaf43b-90e6-46f4-a406-a926d86bc0b0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "daaaf43b-90e6-46f4-a406-a926d86bc0b0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "556706cc-473e-4ae9-95c9-21257e4164b4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}