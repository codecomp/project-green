{
    "id": "55c69140-33ec-4e09-a7fe-947a44d510d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_palette_4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "51a450bf-4732-464f-ab68-18462609d983",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55c69140-33ec-4e09-a7fe-947a44d510d9",
            "compositeImage": {
                "id": "c5036325-df40-461d-82bd-d97a62149f4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51a450bf-4732-464f-ab68-18462609d983",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a399c9cf-a36a-4a0e-aa3b-802ccf3c95e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51a450bf-4732-464f-ab68-18462609d983",
                    "LayerId": "b4fcfc09-cab3-4eef-adb4-586a3d37be7d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "b4fcfc09-cab3-4eef-adb4-586a3d37be7d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "55c69140-33ec-4e09-a7fe-947a44d510d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 0,
    "yorig": 0
}