{
    "id": "a8f6c494-a62a-4b34-b284-cae6043fa1b3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_coin_6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "6ac4bb4f-9de5-423a-99b3-d620c79f2510",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8f6c494-a62a-4b34-b284-cae6043fa1b3",
            "compositeImage": {
                "id": "05049354-f698-4fc1-82f7-9ca0767e0004",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ac4bb4f-9de5-423a-99b3-d620c79f2510",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65d7c701-134b-4e66-b06f-e000646e102b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ac4bb4f-9de5-423a-99b3-d620c79f2510",
                    "LayerId": "8c5838ab-6526-4378-b9a1-61319203dbd7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "8c5838ab-6526-4378-b9a1-61319203dbd7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a8f6c494-a62a-4b34-b284-cae6043fa1b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 8,
    "yorig": 11
}