{
    "id": "484ec6e4-6546-491e-a8d4-56f179eecd8c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_coin_5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "dc1bf083-6720-4594-b988-7bb6a6579440",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "484ec6e4-6546-491e-a8d4-56f179eecd8c",
            "compositeImage": {
                "id": "0690e4dd-a904-4fc3-bdf6-81ffb3a242b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc1bf083-6720-4594-b988-7bb6a6579440",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd723a7a-3b19-492f-9afa-49e7fd99f018",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc1bf083-6720-4594-b988-7bb6a6579440",
                    "LayerId": "5a469bd6-74fa-4868-89bc-df6fa668e02d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "5a469bd6-74fa-4868-89bc-df6fa668e02d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "484ec6e4-6546-491e-a8d4-56f179eecd8c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 9
}