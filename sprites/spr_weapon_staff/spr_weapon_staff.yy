{
    "id": "717a116c-b09a-41c1-97a9-d5959fc95d52",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_weapon_staff",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "9a8769b2-9bd7-40d9-ba0a-0567cfee1f28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "717a116c-b09a-41c1-97a9-d5959fc95d52",
            "compositeImage": {
                "id": "7972ef15-3b33-4b00-9879-063cb562ca33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a8769b2-9bd7-40d9-ba0a-0567cfee1f28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f72c44f-2ad3-49bd-8ad0-be4795f10073",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a8769b2-9bd7-40d9-ba0a-0567cfee1f28",
                    "LayerId": "06ae3795-d1ac-4fee-a891-f58cefcb8d6f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "06ae3795-d1ac-4fee-a891-f58cefcb8d6f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "717a116c-b09a-41c1-97a9-d5959fc95d52",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}