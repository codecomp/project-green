{
    "id": "ac9e50b2-1a37-4ad9-891e-9e555510e483",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_weapon_knuckles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "422f088c-4283-481e-9013-60c384a8a1ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac9e50b2-1a37-4ad9-891e-9e555510e483",
            "compositeImage": {
                "id": "54824ef2-e1b8-4170-89cb-06a08e98549f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "422f088c-4283-481e-9013-60c384a8a1ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "465a9d3b-2f3b-4ad2-b75e-038527441218",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "422f088c-4283-481e-9013-60c384a8a1ba",
                    "LayerId": "669337a7-6ce0-4331-8d3c-dd28819a9e37"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "669337a7-6ce0-4331-8d3c-dd28819a9e37",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ac9e50b2-1a37-4ad9-891e-9e555510e483",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}