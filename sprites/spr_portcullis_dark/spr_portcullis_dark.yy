{
    "id": "50a1f3bb-1f7f-4383-bba1-7ad25f42567f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portcullis_dark",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7074249e-98aa-4d8f-a131-b1ac6a0fa3c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50a1f3bb-1f7f-4383-bba1-7ad25f42567f",
            "compositeImage": {
                "id": "f4cd2816-ccc6-4c3b-84b4-2b8a8d80199e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7074249e-98aa-4d8f-a131-b1ac6a0fa3c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06a4173e-5285-4d6c-b1e5-1f390d5feeb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7074249e-98aa-4d8f-a131-b1ac6a0fa3c0",
                    "LayerId": "8e16b35c-c4d4-4bc9-a1df-bb1c307bb97e"
                }
            ]
        },
        {
            "id": "04793e9a-ceba-4ad8-aa3f-485d6e10635b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50a1f3bb-1f7f-4383-bba1-7ad25f42567f",
            "compositeImage": {
                "id": "859540fe-7a5d-4e62-aac3-294948e30840",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04793e9a-ceba-4ad8-aa3f-485d6e10635b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec875ab2-95fd-4a42-b5a0-d70160969236",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04793e9a-ceba-4ad8-aa3f-485d6e10635b",
                    "LayerId": "8e16b35c-c4d4-4bc9-a1df-bb1c307bb97e"
                }
            ]
        },
        {
            "id": "2062bb74-6733-422f-b052-9cd7c6a407b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50a1f3bb-1f7f-4383-bba1-7ad25f42567f",
            "compositeImage": {
                "id": "52209425-45e5-404b-a956-c79e16f5dcb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2062bb74-6733-422f-b052-9cd7c6a407b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bf1c45b-6c08-4040-b6d0-8e0ada12166d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2062bb74-6733-422f-b052-9cd7c6a407b4",
                    "LayerId": "8e16b35c-c4d4-4bc9-a1df-bb1c307bb97e"
                }
            ]
        },
        {
            "id": "b392a765-2d8e-4706-ada7-f84cd5110b3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50a1f3bb-1f7f-4383-bba1-7ad25f42567f",
            "compositeImage": {
                "id": "efc569e1-653c-4fc9-b14c-c428de8602e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b392a765-2d8e-4706-ada7-f84cd5110b3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29ec7ad5-88a5-43aa-a000-1c43e9ed9d6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b392a765-2d8e-4706-ada7-f84cd5110b3e",
                    "LayerId": "8e16b35c-c4d4-4bc9-a1df-bb1c307bb97e"
                }
            ]
        },
        {
            "id": "8aa1e2fb-9537-48df-b28a-d86e1167e11d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50a1f3bb-1f7f-4383-bba1-7ad25f42567f",
            "compositeImage": {
                "id": "7fa2d365-3a90-4a38-a5a4-7669004e8f57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8aa1e2fb-9537-48df-b28a-d86e1167e11d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6448fd05-527a-4220-bd75-b989bd64b2c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8aa1e2fb-9537-48df-b28a-d86e1167e11d",
                    "LayerId": "8e16b35c-c4d4-4bc9-a1df-bb1c307bb97e"
                }
            ]
        },
        {
            "id": "61be2158-8e6f-44cc-91b0-9eadd820763c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50a1f3bb-1f7f-4383-bba1-7ad25f42567f",
            "compositeImage": {
                "id": "3df47851-919a-4f6e-9a93-1ce6ac9b7df0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61be2158-8e6f-44cc-91b0-9eadd820763c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0154d3c2-4cf7-4668-82f8-33c73d777d5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61be2158-8e6f-44cc-91b0-9eadd820763c",
                    "LayerId": "8e16b35c-c4d4-4bc9-a1df-bb1c307bb97e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8e16b35c-c4d4-4bc9-a1df-bb1c307bb97e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50a1f3bb-1f7f-4383-bba1-7ad25f42567f",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}