{
    "id": "09697098-6760-41ff-afb9-52f791cda544",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_torch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 6,
    "bbox_right": 9,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "aae75148-9e55-4433-9547-f0261f866234",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09697098-6760-41ff-afb9-52f791cda544",
            "compositeImage": {
                "id": "7fcb2f2d-eab0-4abe-9c05-718442a70ae7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aae75148-9e55-4433-9547-f0261f866234",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7871f022-92ad-472a-b678-d148fe04eed7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aae75148-9e55-4433-9547-f0261f866234",
                    "LayerId": "78080a90-77f3-409a-9e3e-8617e8fc0afb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 23,
    "layers": [
        {
            "id": "78080a90-77f3-409a-9e3e-8617e8fc0afb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "09697098-6760-41ff-afb9-52f791cda544",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 1.7,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 11
}