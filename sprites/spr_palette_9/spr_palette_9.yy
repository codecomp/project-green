{
    "id": "10836d90-4509-473a-8df7-a9c09b6cc0fc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_palette_9",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8f696ae4-605c-40b1-894c-85e3ea155568",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10836d90-4509-473a-8df7-a9c09b6cc0fc",
            "compositeImage": {
                "id": "621e4e52-fd86-44f5-85c6-a363463ad874",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f696ae4-605c-40b1-894c-85e3ea155568",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8b62e32-d107-45fa-b044-4ac18c4eb818",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f696ae4-605c-40b1-894c-85e3ea155568",
                    "LayerId": "79ef5e0f-680e-406f-abbd-faf2b5001b50"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "79ef5e0f-680e-406f-abbd-faf2b5001b50",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "10836d90-4509-473a-8df7-a9c09b6cc0fc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 0,
    "yorig": 0
}