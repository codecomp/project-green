{
    "id": "835930e2-1837-4737-b9f7-eb41b0397119",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_crawler",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f897dbc6-fdb4-4e37-a13c-d992d3fe21ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "835930e2-1837-4737-b9f7-eb41b0397119",
            "compositeImage": {
                "id": "b4e17188-3f9f-443d-a04c-a8290f191d15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f897dbc6-fdb4-4e37-a13c-d992d3fe21ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7d4e6e9-85f9-4c07-90bd-3c5a129e4e1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f897dbc6-fdb4-4e37-a13c-d992d3fe21ca",
                    "LayerId": "a293e4ff-f695-46da-8072-1bb1aba0567a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "a293e4ff-f695-46da-8072-1bb1aba0567a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "835930e2-1837-4737-b9f7-eb41b0397119",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 2,
    "yorig": 2
}