{
    "id": "0f7e9c9f-1a65-4533-abae-864db99a2a1f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_weapon_whip",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f616834f-880d-417d-8ef3-d600290b5442",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f7e9c9f-1a65-4533-abae-864db99a2a1f",
            "compositeImage": {
                "id": "2b43efed-5488-452d-a27f-85385ee91521",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f616834f-880d-417d-8ef3-d600290b5442",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32173618-a1ad-41b0-9709-c5ecf5ef6304",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f616834f-880d-417d-8ef3-d600290b5442",
                    "LayerId": "b5ae4c6f-69a9-435b-9ac9-6d1c0ef20a7a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b5ae4c6f-69a9-435b-9ac9-6d1c0ef20a7a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f7e9c9f-1a65-4533-abae-864db99a2a1f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}