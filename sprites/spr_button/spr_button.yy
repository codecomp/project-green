{
    "id": "d93bee6a-6be0-4243-8649-051c52cc3bc7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a3b12b39-13b3-4dea-b892-812886977da7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d93bee6a-6be0-4243-8649-051c52cc3bc7",
            "compositeImage": {
                "id": "fff918f8-3446-4f4e-813d-1642a5f2b44f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3b12b39-13b3-4dea-b892-812886977da7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb6876da-0957-47c7-ac94-94b6892ca10d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3b12b39-13b3-4dea-b892-812886977da7",
                    "LayerId": "0600461f-176e-45e6-a80c-2e80e542301a"
                }
            ]
        },
        {
            "id": "904b0906-bbe8-437a-8756-d10264908796",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d93bee6a-6be0-4243-8649-051c52cc3bc7",
            "compositeImage": {
                "id": "1189a4e0-c091-4fb0-bbf5-febf4536ee8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "904b0906-bbe8-437a-8756-d10264908796",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c431b98d-5775-4b08-a732-43064c56ba18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "904b0906-bbe8-437a-8756-d10264908796",
                    "LayerId": "0600461f-176e-45e6-a80c-2e80e542301a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "0600461f-176e-45e6-a80c-2e80e542301a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d93bee6a-6be0-4243-8649-051c52cc3bc7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}