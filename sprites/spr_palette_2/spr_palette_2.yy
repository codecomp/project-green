{
    "id": "faddff7d-9c0e-4aee-9468-65e4866b669b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_palette_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b1ef3912-ead9-4efa-8ba2-5d927277e2ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "faddff7d-9c0e-4aee-9468-65e4866b669b",
            "compositeImage": {
                "id": "ed5c3ed2-d96e-4651-9dd2-88b110275178",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1ef3912-ead9-4efa-8ba2-5d927277e2ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47b9d6db-c043-4f9a-a560-de4afb507983",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1ef3912-ead9-4efa-8ba2-5d927277e2ed",
                    "LayerId": "c19112b2-93d6-4b17-8882-e4cb24a2d41b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "c19112b2-93d6-4b17-8882-e4cb24a2d41b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "faddff7d-9c0e-4aee-9468-65e4866b669b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 0,
    "yorig": 0
}