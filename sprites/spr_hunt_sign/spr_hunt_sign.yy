{
    "id": "786c8ac4-4fdf-40d0-9894-e0f19d0d2da3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hunt_sign",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0956341d-4e34-40d6-826e-1adcf55c0e3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "786c8ac4-4fdf-40d0-9894-e0f19d0d2da3",
            "compositeImage": {
                "id": "a80804d5-b009-4f8b-92ff-debd81a64016",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0956341d-4e34-40d6-826e-1adcf55c0e3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc206c9a-f8e8-4f32-9016-c650933eea77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0956341d-4e34-40d6-826e-1adcf55c0e3c",
                    "LayerId": "cf352205-8807-4325-9a20-582877d7116a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "cf352205-8807-4325-9a20-582877d7116a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "786c8ac4-4fdf-40d0-9894-e0f19d0d2da3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 0,
    "yorig": 0
}