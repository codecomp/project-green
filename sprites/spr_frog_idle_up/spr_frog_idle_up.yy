{
    "id": "c3e61c3b-0ebb-477d-9e2f-4832a4ddd93a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_frog_idle_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "e9b1b081-e165-4d20-9e8f-c4ff7583506a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3e61c3b-0ebb-477d-9e2f-4832a4ddd93a",
            "compositeImage": {
                "id": "3d94fea3-c320-45cd-9818-39765a5a57cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9b1b081-e165-4d20-9e8f-c4ff7583506a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6f1176a-e78a-4f0e-b45b-4731c22c1d55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9b1b081-e165-4d20-9e8f-c4ff7583506a",
                    "LayerId": "703a45fd-7ff7-4dd1-af93-cab578e200e2"
                }
            ]
        },
        {
            "id": "a0eb6e2f-4032-441d-9ccf-a4533428779a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3e61c3b-0ebb-477d-9e2f-4832a4ddd93a",
            "compositeImage": {
                "id": "ef2ef323-55bf-4594-9479-5838e62f7d24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0eb6e2f-4032-441d-9ccf-a4533428779a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "484b8558-2c9d-4176-b186-f7863eecc20e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0eb6e2f-4032-441d-9ccf-a4533428779a",
                    "LayerId": "703a45fd-7ff7-4dd1-af93-cab578e200e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 5,
    "layers": [
        {
            "id": "703a45fd-7ff7-4dd1-af93-cab578e200e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c3e61c3b-0ebb-477d-9e2f-4832a4ddd93a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 3,
    "yorig": 2
}