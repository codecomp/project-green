{
    "id": "960fa871-681c-4867-9b8c-30634b37fd3d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_palette_14",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4e4cd257-c5b3-4101-8bc9-2d762a609a69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "960fa871-681c-4867-9b8c-30634b37fd3d",
            "compositeImage": {
                "id": "f8f2af0c-c1ec-4f14-ae12-1d92dc89902b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e4cd257-c5b3-4101-8bc9-2d762a609a69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5b8ca62-487c-4db1-9419-bcfb9621f942",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e4cd257-c5b3-4101-8bc9-2d762a609a69",
                    "LayerId": "a1920581-b34c-450d-bcc9-21ebde75adb7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "a1920581-b34c-450d-bcc9-21ebde75adb7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "960fa871-681c-4867-9b8c-30634b37fd3d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 0,
    "yorig": 0
}