{
    "id": "30913476-42ff-482c-bce2-8a5f95fc5866",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_frog_idle_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "de1b78de-15bf-4a70-aa4a-e1207ae0b222",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30913476-42ff-482c-bce2-8a5f95fc5866",
            "compositeImage": {
                "id": "dd82b94c-8fda-4d05-9592-83d935a74cfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de1b78de-15bf-4a70-aa4a-e1207ae0b222",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "179c5ae2-7929-4d9b-bbbd-01b2115b7917",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de1b78de-15bf-4a70-aa4a-e1207ae0b222",
                    "LayerId": "2d0ea7ca-ecd8-4520-9126-fbf6826dc474"
                }
            ]
        },
        {
            "id": "722025c0-fbc7-4123-be2a-12eb05e47734",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30913476-42ff-482c-bce2-8a5f95fc5866",
            "compositeImage": {
                "id": "1fa11a0f-5d90-4185-8a99-9ca64fee6b5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "722025c0-fbc7-4123-be2a-12eb05e47734",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bf485cd-5750-4fc4-827f-44a10bf716d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "722025c0-fbc7-4123-be2a-12eb05e47734",
                    "LayerId": "2d0ea7ca-ecd8-4520-9126-fbf6826dc474"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 5,
    "layers": [
        {
            "id": "2d0ea7ca-ecd8-4520-9126-fbf6826dc474",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "30913476-42ff-482c-bce2-8a5f95fc5866",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 3,
    "yorig": 2
}