{
    "id": "6047839c-a939-467b-808e-697cc13cfb88",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_transition_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 143,
    "bbox_left": 0,
    "bbox_right": 159,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "8f42bf91-13f1-4461-927d-6a79090fe94b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6047839c-a939-467b-808e-697cc13cfb88",
            "compositeImage": {
                "id": "fc5fe7da-6166-4fa3-979d-5367afcebd49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f42bf91-13f1-4461-927d-6a79090fe94b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "011d09b4-5d72-4f7e-a224-daa272f3e303",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f42bf91-13f1-4461-927d-6a79090fe94b",
                    "LayerId": "d4a9dd48-6981-498e-9905-a2302be8e727"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 144,
    "layers": [
        {
            "id": "d4a9dd48-6981-498e-9905-a2302be8e727",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6047839c-a939-467b-808e-697cc13cfb88",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}