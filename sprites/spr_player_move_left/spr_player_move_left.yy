{
    "id": "ad2464d7-37f7-433b-aba8-373d48dcc426",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_move_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "85e62654-3e3b-4764-a9e6-8275e5d76771",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad2464d7-37f7-433b-aba8-373d48dcc426",
            "compositeImage": {
                "id": "be12a1d1-f3a2-44fb-a54f-60a693eb39c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85e62654-3e3b-4764-a9e6-8275e5d76771",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55b80ef5-c577-4ec8-8abe-84d1c20c83e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85e62654-3e3b-4764-a9e6-8275e5d76771",
                    "LayerId": "99d74609-538c-4571-a15c-76d8ada196e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "99d74609-538c-4571-a15c-76d8ada196e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad2464d7-37f7-433b-aba8-373d48dcc426",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}