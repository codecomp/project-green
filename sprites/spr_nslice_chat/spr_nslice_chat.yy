{
    "id": "5598394e-9449-401b-b4f3-d51830d27c3f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nslice_chat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "5b6e9513-905f-4977-a263-55b2ebfaba9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5598394e-9449-401b-b4f3-d51830d27c3f",
            "compositeImage": {
                "id": "adf32b89-2890-4e61-b9b6-d3179c4b025e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b6e9513-905f-4977-a263-55b2ebfaba9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8652f75f-6d64-4abd-b089-3a3fef335bd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b6e9513-905f-4977-a263-55b2ebfaba9f",
                    "LayerId": "ddb8363d-a4da-441a-8ee5-9d553daf5d88"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "ddb8363d-a4da-441a-8ee5-9d553daf5d88",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5598394e-9449-401b-b4f3-d51830d27c3f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}