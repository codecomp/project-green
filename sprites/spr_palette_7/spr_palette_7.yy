{
    "id": "26411c35-4372-4ee2-ad41-910e09a962e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_palette_7",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e74b9318-8047-4fb4-ba11-98a8832af0f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26411c35-4372-4ee2-ad41-910e09a962e0",
            "compositeImage": {
                "id": "ea627203-abec-4885-870a-f3b7ec0650ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e74b9318-8047-4fb4-ba11-98a8832af0f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f0d76c4-82ca-4a3a-ab02-aaf9c6d1caa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e74b9318-8047-4fb4-ba11-98a8832af0f7",
                    "LayerId": "c17c9e8d-401c-4669-a0a7-e0e8e3941377"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "c17c9e8d-401c-4669-a0a7-e0e8e3941377",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "26411c35-4372-4ee2-ad41-910e09a962e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 0,
    "yorig": 0
}