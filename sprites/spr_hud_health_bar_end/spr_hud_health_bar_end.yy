{
    "id": "7c0d896b-d07d-42ed-82c6-e0b83d97803e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hud_health_bar_end",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "2dcf2f27-fb0f-447e-ae34-90b9d0491193",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c0d896b-d07d-42ed-82c6-e0b83d97803e",
            "compositeImage": {
                "id": "21c6b621-dc39-4d1f-9fec-6b0eb645163e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dcf2f27-fb0f-447e-ae34-90b9d0491193",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70db3f1f-9c61-49bb-bf02-a8608a103354",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dcf2f27-fb0f-447e-ae34-90b9d0491193",
                    "LayerId": "f7a69898-3823-47c0-8420-1fa7cf1c3dc5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 3,
    "layers": [
        {
            "id": "f7a69898-3823-47c0-8420-1fa7cf1c3dc5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7c0d896b-d07d-42ed-82c6-e0b83d97803e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 0,
    "yorig": 0
}