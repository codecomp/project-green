{
    "id": "8017bb9e-1eaa-4d49-b3b6-632fbe237497",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_palette_11",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6b024e76-ecb7-40e5-96b3-244ad858c69a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8017bb9e-1eaa-4d49-b3b6-632fbe237497",
            "compositeImage": {
                "id": "c4208bc2-173b-4c9d-becc-f4012cee4d9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b024e76-ecb7-40e5-96b3-244ad858c69a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bad9610-e2d8-4b98-9178-368a17d542a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b024e76-ecb7-40e5-96b3-244ad858c69a",
                    "LayerId": "1b2aeafa-1345-42fb-86fe-043a152999fc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "1b2aeafa-1345-42fb-86fe-043a152999fc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8017bb9e-1eaa-4d49-b3b6-632fbe237497",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 0,
    "yorig": 0
}