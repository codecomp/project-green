{
    "id": "adf19e5b-f055-4e9c-8a5c-87979214c5c3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_coin_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "65834ddf-05dc-42b4-b8ca-2103f844f024",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adf19e5b-f055-4e9c-8a5c-87979214c5c3",
            "compositeImage": {
                "id": "b65d28a3-283e-4b88-b723-66fb284f3a77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65834ddf-05dc-42b4-b8ca-2103f844f024",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14c4ad35-62fa-4788-ab3d-34cb3115f19a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65834ddf-05dc-42b4-b8ca-2103f844f024",
                    "LayerId": "d973d828-adb4-4222-84f4-681ba7c74728"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "d973d828-adb4-4222-84f4-681ba7c74728",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "adf19e5b-f055-4e9c-8a5c-87979214c5c3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 3,
    "yorig": 4
}