{
    "id": "3aab40c0-6da7-45ac-ad8a-d479477a1440",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_weapon_scimatar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "9242d9bf-9ad6-4151-8cc9-54811f1722c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3aab40c0-6da7-45ac-ad8a-d479477a1440",
            "compositeImage": {
                "id": "48aa5de4-ef99-4e7e-8f89-155ac4e4a798",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9242d9bf-9ad6-4151-8cc9-54811f1722c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db9627a0-fcbc-4aef-9d7a-00ce36eb609b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9242d9bf-9ad6-4151-8cc9-54811f1722c0",
                    "LayerId": "e412f799-102d-44b0-959b-51dc7641b767"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e412f799-102d-44b0-959b-51dc7641b767",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3aab40c0-6da7-45ac-ad8a-d479477a1440",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}