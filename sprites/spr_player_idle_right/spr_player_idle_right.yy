{
    "id": "607d6f77-9eb9-4e3f-aeca-8c7cea010a2f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_idle_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d81f2bc-5cc2-4033-9769-dbd152682fb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607d6f77-9eb9-4e3f-aeca-8c7cea010a2f",
            "compositeImage": {
                "id": "36ce5572-8bc6-4101-bb2b-cc84f39bee86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d81f2bc-5cc2-4033-9769-dbd152682fb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa2f07b5-7600-438d-9dfe-fb67146035dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d81f2bc-5cc2-4033-9769-dbd152682fb8",
                    "LayerId": "54bb0f6f-bc93-4e4c-a224-5e4009d230c4"
                }
            ]
        },
        {
            "id": "ce244e68-e8d6-4486-83f9-609662098ea0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "607d6f77-9eb9-4e3f-aeca-8c7cea010a2f",
            "compositeImage": {
                "id": "d503ccbf-759f-4059-a6cf-c483efaa7e33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce244e68-e8d6-4486-83f9-609662098ea0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "537900ee-f287-4a23-90b2-806167883a24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce244e68-e8d6-4486-83f9-609662098ea0",
                    "LayerId": "54bb0f6f-bc93-4e4c-a224-5e4009d230c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "54bb0f6f-bc93-4e4c-a224-5e4009d230c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "607d6f77-9eb9-4e3f-aeca-8c7cea010a2f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}