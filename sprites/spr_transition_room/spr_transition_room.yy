{
    "id": "9f992060-ce1b-4f86-8e48-91c7a17ae7f3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_transition_room",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a4009e3a-6ed7-4714-b659-46af2329d3ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f992060-ce1b-4f86-8e48-91c7a17ae7f3",
            "compositeImage": {
                "id": "0507e89c-2950-4c78-ad16-1516645b2963",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4009e3a-6ed7-4714-b659-46af2329d3ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a38e7100-e271-4a60-b1e9-2feb675c10c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4009e3a-6ed7-4714-b659-46af2329d3ff",
                    "LayerId": "4b19da25-7192-47fc-b008-70beef16b7c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "4b19da25-7192-47fc-b008-70beef16b7c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f992060-ce1b-4f86-8e48-91c7a17ae7f3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}