{
    "id": "f8ccc53b-023e-42ae-b048-1d3719035629",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_key",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "1e8dd24d-acf1-4f02-a496-b83f531119d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ccc53b-023e-42ae-b048-1d3719035629",
            "compositeImage": {
                "id": "c72147cd-c1ca-4e2e-bc4d-b27bf8ad47df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e8dd24d-acf1-4f02-a496-b83f531119d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f02d59a-79da-4982-b608-bdb7fbd2f6ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e8dd24d-acf1-4f02-a496-b83f531119d1",
                    "LayerId": "a50439ce-2b0a-4e0b-8d52-7adcdd069b55"
                }
            ]
        },
        {
            "id": "5b9ce31c-dbd9-46df-9268-591e042e4db7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ccc53b-023e-42ae-b048-1d3719035629",
            "compositeImage": {
                "id": "91748b42-c4cc-4ec2-93f9-38336fc3b55e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b9ce31c-dbd9-46df-9268-591e042e4db7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cad75134-bea4-4a84-bfeb-771ebd7e8bff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b9ce31c-dbd9-46df-9268-591e042e4db7",
                    "LayerId": "a50439ce-2b0a-4e0b-8d52-7adcdd069b55"
                }
            ]
        },
        {
            "id": "ee62006d-0025-4f4b-a32e-c2aeb4f6495c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ccc53b-023e-42ae-b048-1d3719035629",
            "compositeImage": {
                "id": "2e372930-1014-4c74-bff8-9eae3a3c8f8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee62006d-0025-4f4b-a32e-c2aeb4f6495c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f55697db-b6d5-434a-9bf9-ee38f172554f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee62006d-0025-4f4b-a32e-c2aeb4f6495c",
                    "LayerId": "a50439ce-2b0a-4e0b-8d52-7adcdd069b55"
                }
            ]
        },
        {
            "id": "af5d2190-e2c5-4f70-9bea-42eaf91d0cd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ccc53b-023e-42ae-b048-1d3719035629",
            "compositeImage": {
                "id": "9b9c1fd7-3ec4-41aa-9c6c-5ceb6ad43f69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af5d2190-e2c5-4f70-9bea-42eaf91d0cd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7df71377-3b69-4a03-8c1f-4389db4c1000",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af5d2190-e2c5-4f70-9bea-42eaf91d0cd0",
                    "LayerId": "a50439ce-2b0a-4e0b-8d52-7adcdd069b55"
                }
            ]
        },
        {
            "id": "d3fe53c0-6426-43b7-b480-7aa802fa6994",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ccc53b-023e-42ae-b048-1d3719035629",
            "compositeImage": {
                "id": "05094f42-7b6b-4c75-bedd-121a9c077538",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3fe53c0-6426-43b7-b480-7aa802fa6994",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c4afdcb-cbeb-42a7-abbe-857685673ce6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3fe53c0-6426-43b7-b480-7aa802fa6994",
                    "LayerId": "a50439ce-2b0a-4e0b-8d52-7adcdd069b55"
                }
            ]
        },
        {
            "id": "78e2f9f0-d70a-4d32-b165-883bc8b6ac35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ccc53b-023e-42ae-b048-1d3719035629",
            "compositeImage": {
                "id": "ade13cc4-1628-4875-9c70-8d9be828c5ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78e2f9f0-d70a-4d32-b165-883bc8b6ac35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc365959-a64e-46cf-aa57-6b440ad60bea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78e2f9f0-d70a-4d32-b165-883bc8b6ac35",
                    "LayerId": "a50439ce-2b0a-4e0b-8d52-7adcdd069b55"
                }
            ]
        },
        {
            "id": "95aa95ce-ae8d-48ba-bb17-b6a63fffee88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ccc53b-023e-42ae-b048-1d3719035629",
            "compositeImage": {
                "id": "1bdc37a7-2b5e-447c-917d-206d26a201db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95aa95ce-ae8d-48ba-bb17-b6a63fffee88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91ae047f-a6af-43ab-8356-4ff94dd709c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95aa95ce-ae8d-48ba-bb17-b6a63fffee88",
                    "LayerId": "a50439ce-2b0a-4e0b-8d52-7adcdd069b55"
                }
            ]
        },
        {
            "id": "5d162a2e-f723-4cfa-ab48-f2b588185bfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ccc53b-023e-42ae-b048-1d3719035629",
            "compositeImage": {
                "id": "738f53fa-ee84-4e89-8557-7e3901818b80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d162a2e-f723-4cfa-ab48-f2b588185bfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0dfbd11-ca9d-4020-9830-09cb278f492c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d162a2e-f723-4cfa-ab48-f2b588185bfb",
                    "LayerId": "a50439ce-2b0a-4e0b-8d52-7adcdd069b55"
                }
            ]
        },
        {
            "id": "269b04d8-e166-43a4-babd-af0fb6ef9463",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ccc53b-023e-42ae-b048-1d3719035629",
            "compositeImage": {
                "id": "7a4acdc0-aa90-4e48-93f9-03007dc2e496",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "269b04d8-e166-43a4-babd-af0fb6ef9463",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ef00939-22c4-4fea-8b9c-89cf835a0197",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "269b04d8-e166-43a4-babd-af0fb6ef9463",
                    "LayerId": "a50439ce-2b0a-4e0b-8d52-7adcdd069b55"
                }
            ]
        },
        {
            "id": "60c22787-7486-4eb9-9f98-fe7b805b78db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ccc53b-023e-42ae-b048-1d3719035629",
            "compositeImage": {
                "id": "bbb0449a-8ac8-41c9-8aef-272e076017de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60c22787-7486-4eb9-9f98-fe7b805b78db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aadffdbc-7093-4956-b002-86493a13029f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60c22787-7486-4eb9-9f98-fe7b805b78db",
                    "LayerId": "a50439ce-2b0a-4e0b-8d52-7adcdd069b55"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "a50439ce-2b0a-4e0b-8d52-7adcdd069b55",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f8ccc53b-023e-42ae-b048-1d3719035629",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}