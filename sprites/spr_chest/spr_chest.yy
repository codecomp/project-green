{
    "id": "622492b1-480f-4aac-8544-e22c4c258ce6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_chest",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "02ecda87-3abc-4bf6-b2aa-b4c866db89db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "622492b1-480f-4aac-8544-e22c4c258ce6",
            "compositeImage": {
                "id": "a13d2545-bda0-46b4-9018-b404004b433e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02ecda87-3abc-4bf6-b2aa-b4c866db89db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46df1a27-6fd6-4f53-8093-12e9ef8131e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02ecda87-3abc-4bf6-b2aa-b4c866db89db",
                    "LayerId": "9172766c-32a5-462c-9715-13e1edf59828"
                }
            ]
        },
        {
            "id": "50df2670-935c-4fb9-8416-d4880b2c5f62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "622492b1-480f-4aac-8544-e22c4c258ce6",
            "compositeImage": {
                "id": "34fe133c-0ac2-4ffe-aa55-7cb21de10d8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50df2670-935c-4fb9-8416-d4880b2c5f62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9f3fa2a-c568-430d-9498-4e92b81062f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50df2670-935c-4fb9-8416-d4880b2c5f62",
                    "LayerId": "9172766c-32a5-462c-9715-13e1edf59828"
                }
            ]
        },
        {
            "id": "0200686f-b758-4381-83de-73b0544e83d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "622492b1-480f-4aac-8544-e22c4c258ce6",
            "compositeImage": {
                "id": "177aa52e-7914-4a8b-a524-fca0fde7a99b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0200686f-b758-4381-83de-73b0544e83d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aeff7f91-620a-458f-a29f-2d3d69d4af3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0200686f-b758-4381-83de-73b0544e83d4",
                    "LayerId": "9172766c-32a5-462c-9715-13e1edf59828"
                }
            ]
        },
        {
            "id": "6865a91f-4051-4c63-b8d7-7e847e65e151",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "622492b1-480f-4aac-8544-e22c4c258ce6",
            "compositeImage": {
                "id": "97847883-dd5c-4513-86d0-63fbd4e9eacf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6865a91f-4051-4c63-b8d7-7e847e65e151",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e29f944-479c-410f-955d-7ceb00fb3201",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6865a91f-4051-4c63-b8d7-7e847e65e151",
                    "LayerId": "9172766c-32a5-462c-9715-13e1edf59828"
                }
            ]
        },
        {
            "id": "ef8badd6-57b9-4b02-86c6-fce07db10e3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "622492b1-480f-4aac-8544-e22c4c258ce6",
            "compositeImage": {
                "id": "4c7b4495-7433-4e65-b5e8-1e98e396008b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef8badd6-57b9-4b02-86c6-fce07db10e3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a9c3e06-e8fa-4d16-b0cb-36c2a9182c3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef8badd6-57b9-4b02-86c6-fce07db10e3e",
                    "LayerId": "9172766c-32a5-462c-9715-13e1edf59828"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "9172766c-32a5-462c-9715-13e1edf59828",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "622492b1-480f-4aac-8544-e22c4c258ce6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}