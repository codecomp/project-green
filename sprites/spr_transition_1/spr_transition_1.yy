{
    "id": "7ee09523-7cc6-471b-83a4-791880e1e514",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_transition_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 143,
    "bbox_left": 0,
    "bbox_right": 159,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ec9a9bbd-51c0-4e6f-83b8-22eda4d01500",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ee09523-7cc6-471b-83a4-791880e1e514",
            "compositeImage": {
                "id": "e9f1dbd4-2ca4-4ca6-b8f8-18e4f97b02e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec9a9bbd-51c0-4e6f-83b8-22eda4d01500",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04853523-78eb-4590-ba46-31cdeb0ba8fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec9a9bbd-51c0-4e6f-83b8-22eda4d01500",
                    "LayerId": "a90dbe05-83ab-4daa-84d7-c3f10c229b7d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 144,
    "layers": [
        {
            "id": "a90dbe05-83ab-4daa-84d7-c3f10c229b7d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7ee09523-7cc6-471b-83a4-791880e1e514",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}