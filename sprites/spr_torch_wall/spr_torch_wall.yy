{
    "id": "6f5f8feb-b4a6-4d19-9ff1-ed9a92e6e54e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_torch_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "5e5bdb0a-b21f-4ac9-8af1-50025c4ac10b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f5f8feb-b4a6-4d19-9ff1-ed9a92e6e54e",
            "compositeImage": {
                "id": "d46b6013-9715-4dbd-a895-c4a0f99c2a89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e5bdb0a-b21f-4ac9-8af1-50025c4ac10b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da6a0a46-8210-4bee-8cb4-2877eb721a28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e5bdb0a-b21f-4ac9-8af1-50025c4ac10b",
                    "LayerId": "3d1204bf-f2ca-46bf-8744-fbcd70d6d91c"
                }
            ]
        },
        {
            "id": "057312b3-1942-461d-a34f-50727f1d787c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f5f8feb-b4a6-4d19-9ff1-ed9a92e6e54e",
            "compositeImage": {
                "id": "dc62451b-b182-4f6f-b68c-31e9e33816bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "057312b3-1942-461d-a34f-50727f1d787c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6f7e533-c20c-4bd5-9b27-d5d14d3235fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "057312b3-1942-461d-a34f-50727f1d787c",
                    "LayerId": "3d1204bf-f2ca-46bf-8744-fbcd70d6d91c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 23,
    "layers": [
        {
            "id": "3d1204bf-f2ca-46bf-8744-fbcd70d6d91c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f5f8feb-b4a6-4d19-9ff1-ed9a92e6e54e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 1.7,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 11
}