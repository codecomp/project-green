{
    "id": "3c5ef35f-768e-4f53-981c-afa6fed736af",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_move_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f2c55d2d-fbb5-4f55-aed1-cff10e7811cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c5ef35f-768e-4f53-981c-afa6fed736af",
            "compositeImage": {
                "id": "83c3bbea-c4d1-414e-8a7b-e6f074009e76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2c55d2d-fbb5-4f55-aed1-cff10e7811cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fea0effd-0060-4dfe-beae-b80fc6a03660",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2c55d2d-fbb5-4f55-aed1-cff10e7811cd",
                    "LayerId": "dd303af2-38bf-40e9-adcc-0772dd32381d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "dd303af2-38bf-40e9-adcc-0772dd32381d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3c5ef35f-768e-4f53-981c-afa6fed736af",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}