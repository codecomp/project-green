{
    "id": "fe89d015-359b-4340-9854-74dd8762ee17",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_urchin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c8eb9dce-0fba-4ae6-80cd-b7999950b2a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe89d015-359b-4340-9854-74dd8762ee17",
            "compositeImage": {
                "id": "ee873cf2-2816-43bd-93da-66e1780ab217",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8eb9dce-0fba-4ae6-80cd-b7999950b2a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f913e11-0ba6-4570-88dd-9fd67b84a926",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8eb9dce-0fba-4ae6-80cd-b7999950b2a6",
                    "LayerId": "205e993c-08b4-4b17-b4fd-4a2bd79a155e"
                }
            ]
        },
        {
            "id": "21ad84ce-c536-4b4d-869e-fd6ac43b8205",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe89d015-359b-4340-9854-74dd8762ee17",
            "compositeImage": {
                "id": "a811d0a9-a106-40c2-b1f5-11ba0dbfc796",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21ad84ce-c536-4b4d-869e-fd6ac43b8205",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eca58769-4cb3-4eca-aff4-280b4b757543",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21ad84ce-c536-4b4d-869e-fd6ac43b8205",
                    "LayerId": "205e993c-08b4-4b17-b4fd-4a2bd79a155e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 5,
    "layers": [
        {
            "id": "205e993c-08b4-4b17-b4fd-4a2bd79a155e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe89d015-359b-4340-9854-74dd8762ee17",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 2,
    "yorig": 2
}