{
    "id": "631f9f0c-d1ab-4e09-87a8-642ec4b0af97",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_weapon_spear",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "9d8ffe53-ce18-4917-973a-68233b1a0170",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "631f9f0c-d1ab-4e09-87a8-642ec4b0af97",
            "compositeImage": {
                "id": "2d431389-fc02-4d19-a3ef-82dc5dabbe43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d8ffe53-ce18-4917-973a-68233b1a0170",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0b18420-a53d-4195-8a6b-7e65040073b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d8ffe53-ce18-4917-973a-68233b1a0170",
                    "LayerId": "276aba53-d497-448c-bca1-80e687c30dc8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "276aba53-d497-448c-bca1-80e687c30dc8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "631f9f0c-d1ab-4e09-87a8-642ec4b0af97",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}