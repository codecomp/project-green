{
    "id": "ee23b92c-46cc-47e5-9c51-e6be101403c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_drop_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "5faaee5f-7be5-4542-b3d0-b7e84e46a169",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee23b92c-46cc-47e5-9c51-e6be101403c5",
            "compositeImage": {
                "id": "743dc968-922f-4edd-a194-1cec693da6b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5faaee5f-7be5-4542-b3d0-b7e84e46a169",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dbafa18-d854-4c57-9fb4-6b7339235c3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5faaee5f-7be5-4542-b3d0-b7e84e46a169",
                    "LayerId": "bc23b325-7958-49f7-89f9-4cac1ef41494"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "bc23b325-7958-49f7-89f9-4cac1ef41494",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ee23b92c-46cc-47e5-9c51-e6be101403c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}