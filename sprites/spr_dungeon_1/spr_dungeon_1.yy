{
    "id": "196c2ca9-5007-480f-b281-966e7873703f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dungeon_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 279,
    "bbox_left": 0,
    "bbox_right": 247,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "71b9fdc9-dba9-4e06-a16c-ed4b382e5e59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "196c2ca9-5007-480f-b281-966e7873703f",
            "compositeImage": {
                "id": "a49b9350-2d4e-48fd-9de1-1bd404f0c792",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71b9fdc9-dba9-4e06-a16c-ed4b382e5e59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff9b611f-e340-4683-86ea-de3532b95ccb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71b9fdc9-dba9-4e06-a16c-ed4b382e5e59",
                    "LayerId": "41a46381-0b5e-42eb-8894-b65509bf2dcc"
                },
                {
                    "id": "6ed76c2b-7a0a-46fc-9a22-8c4fdbf3526f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71b9fdc9-dba9-4e06-a16c-ed4b382e5e59",
                    "LayerId": "e677fe9d-df22-4930-8856-432cb28d88a1"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 280,
    "layers": [
        {
            "id": "e677fe9d-df22-4930-8856-432cb28d88a1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "196c2ca9-5007-480f-b281-966e7873703f",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "41a46381-0b5e-42eb-8894-b65509bf2dcc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "196c2ca9-5007-480f-b281-966e7873703f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 481,
    "yorig": 139
}