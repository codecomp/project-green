{
    "id": "6b088f81-0a51-4c90-9665-3f61024c1afd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_idle_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "db58d528-5d04-47d2-8490-e670919de4d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b088f81-0a51-4c90-9665-3f61024c1afd",
            "compositeImage": {
                "id": "41883aaf-a1c5-4797-b3ae-78d75923a725",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db58d528-5d04-47d2-8490-e670919de4d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f723787-977b-48b7-981f-a455cf35b39c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db58d528-5d04-47d2-8490-e670919de4d9",
                    "LayerId": "988e32bb-77d4-4552-9e61-318c83d72699"
                }
            ]
        },
        {
            "id": "1bd02a0a-3d65-47e8-a28c-919cf9ee62f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b088f81-0a51-4c90-9665-3f61024c1afd",
            "compositeImage": {
                "id": "9decea77-0ae2-46dd-be55-69960080641e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bd02a0a-3d65-47e8-a28c-919cf9ee62f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "570975b8-65e5-420f-a6cf-6c91a257567a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bd02a0a-3d65-47e8-a28c-919cf9ee62f9",
                    "LayerId": "988e32bb-77d4-4552-9e61-318c83d72699"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "988e32bb-77d4-4552-9e61-318c83d72699",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b088f81-0a51-4c90-9665-3f61024c1afd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}