{
    "id": "ec6714cf-a5e1-40ad-97e7-272847c7e30d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_swim_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "326339ae-be10-4a60-af42-45fe71109fcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec6714cf-a5e1-40ad-97e7-272847c7e30d",
            "compositeImage": {
                "id": "425869aa-2105-46bd-8fd6-c0cd3b28ed91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "326339ae-be10-4a60-af42-45fe71109fcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df4ea5f9-8322-488a-8113-2a5c732c8e27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "326339ae-be10-4a60-af42-45fe71109fcb",
                    "LayerId": "8f5a72ab-5bbe-4b9c-a31a-97a9d287e5c0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "8f5a72ab-5bbe-4b9c-a31a-97a9d287e5c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec6714cf-a5e1-40ad-97e7-272847c7e30d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}