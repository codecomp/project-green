{
    "id": "172bbf8c-9fce-42e4-a2f0-f1aeac108bbc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nslice_simple",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "309cfe7d-bccb-41db-b4ed-12c0764d1e47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "172bbf8c-9fce-42e4-a2f0-f1aeac108bbc",
            "compositeImage": {
                "id": "47f71a4b-ae0d-4e89-9f79-fefc5acb9cbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "309cfe7d-bccb-41db-b4ed-12c0764d1e47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7f92f4e-6f22-4d34-bb08-bdccff41104f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "309cfe7d-bccb-41db-b4ed-12c0764d1e47",
                    "LayerId": "816630b2-1b73-417c-a93a-9d4bc8736b8f"
                }
            ]
        }
    ],
    "gridX": 4,
    "gridY": 4,
    "height": 12,
    "layers": [
        {
            "id": "816630b2-1b73-417c-a93a-9d4bc8736b8f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "172bbf8c-9fce-42e4-a2f0-f1aeac108bbc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 0,
    "yorig": 0
}