{
    "id": "f9453ff4-d8d2-457f-8393-e0b50b6ef8a5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_palette_5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fa419491-a629-44a4-857b-020a3de1526f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9453ff4-d8d2-457f-8393-e0b50b6ef8a5",
            "compositeImage": {
                "id": "260a6229-fcc0-478e-8823-e3de6197eafe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa419491-a629-44a4-857b-020a3de1526f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80aaea2d-ebd7-4900-99fd-171b29829538",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa419491-a629-44a4-857b-020a3de1526f",
                    "LayerId": "edee8962-5019-4b41-a5c2-0c7510646e3d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "edee8962-5019-4b41-a5c2-0c7510646e3d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f9453ff4-d8d2-457f-8393-e0b50b6ef8a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 0,
    "yorig": 0
}