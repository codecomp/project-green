{
    "id": "3d376afd-c8a3-438e-82d1-694910b9bd9b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_drop_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a13dd661-d93a-4525-a962-2a802f696502",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d376afd-c8a3-438e-82d1-694910b9bd9b",
            "compositeImage": {
                "id": "58beb0eb-1f56-45d4-996f-2a2fafdf41c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a13dd661-d93a-4525-a962-2a802f696502",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c722af8-4154-42b7-a76a-d114657e4b35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a13dd661-d93a-4525-a962-2a802f696502",
                    "LayerId": "5e94ec0e-7ffd-4175-bef0-65849166f654"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "5e94ec0e-7ffd-4175-bef0-65849166f654",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3d376afd-c8a3-438e-82d1-694910b9bd9b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}