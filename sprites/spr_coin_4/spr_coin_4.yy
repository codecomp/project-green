{
    "id": "ade7b9ad-cd8f-4af3-a6c4-88cf182ab218",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_coin_4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "7a3d3348-0453-4a56-bc4a-7d4d51fe761f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ade7b9ad-cd8f-4af3-a6c4-88cf182ab218",
            "compositeImage": {
                "id": "0dacd13e-0310-481b-a859-82c5de9f9b30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a3d3348-0453-4a56-bc4a-7d4d51fe761f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae893283-d4ae-4916-a85b-db785763e3de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a3d3348-0453-4a56-bc4a-7d4d51fe761f",
                    "LayerId": "54b4ce4b-f92b-4cd8-955d-63a33bced192"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 11,
    "layers": [
        {
            "id": "54b4ce4b-f92b-4cd8-955d-63a33bced192",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ade7b9ad-cd8f-4af3-a6c4-88cf182ab218",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 8
}