{
    "id": "bacd2a65-f82d-4020-b88c-20a1adfa3f76",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_drop_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "fe12a578-7f3c-4848-ba51-7bcf62d8faa7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bacd2a65-f82d-4020-b88c-20a1adfa3f76",
            "compositeImage": {
                "id": "f8215a1b-b537-4f3b-80d4-b5b762111fc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe12a578-7f3c-4848-ba51-7bcf62d8faa7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5eea4afc-7f01-43db-bf68-160e6ba1eae6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe12a578-7f3c-4848-ba51-7bcf62d8faa7",
                    "LayerId": "49b17df3-2982-4e64-bb10-e1593bbc5637"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "49b17df3-2982-4e64-bb10-e1593bbc5637",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bacd2a65-f82d-4020-b88c-20a1adfa3f76",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}