{
    "id": "1af05e3c-c283-4a16-bdf5-2083e854578c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hunt_poster",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 6,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "82e77762-c9cf-48fd-a3a9-fd98f674420a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1af05e3c-c283-4a16-bdf5-2083e854578c",
            "compositeImage": {
                "id": "0ac9ac7f-0dd7-4a46-9e2b-b539ef8701f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82e77762-c9cf-48fd-a3a9-fd98f674420a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5517dfb-3646-44c0-8819-b4b7d850eae9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82e77762-c9cf-48fd-a3a9-fd98f674420a",
                    "LayerId": "47e5128e-44e4-45f5-9967-b569acd9ea9e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "47e5128e-44e4-45f5-9967-b569acd9ea9e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1af05e3c-c283-4a16-bdf5-2083e854578c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 7,
    "xorig": 82,
    "yorig": 16
}