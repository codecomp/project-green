//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;
varying vec2 v_vPosition;

uniform float u_cutoff;

void main()
{
    // Get colour so we can access the pixels alpha chanel
	vec4 Color = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	
	float alpha = Color.a - (v_vPosition.x / 160.) + (u_cutoff - 1.);
	alpha = alpha - (v_vPosition.y / 144.) + 0.43;
	
	gl_FragColor = vec4(0, 0, 0, alpha);
	
}