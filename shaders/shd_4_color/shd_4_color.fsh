//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform sampler2D u_palette;
uniform vec4 u_uvs;
uniform vec2 u_texel;

void main()
{
	// Get colour so we can access the pixels alpha chanel
	vec4 Color = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	// Liminosity to colour correct to greyscale accuratly
	vec3 lum = vec3( 0.299, 0.587, 0.114 );
	// Calclate the band the colour fits into
	float band = floor( dot( Color.rgb, lum ) * 4. );
	// Find the required pixel in the pallet in the texture page
	vec2 new_coords = vec2(u_uvs.x + u_texel.x * band, u_uvs.y);
	// Set the colour to the found pixel
	gl_FragColor = vec4(texture2D(u_palette, new_coords).rgb, Color.a);
}
