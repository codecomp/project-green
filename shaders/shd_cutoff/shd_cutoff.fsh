//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;
uniform float cutoff;

void main(){
    vec4 Color = texture2D( gm_BaseTexture, v_vTexcoord );
    
    if (Color.r > cutoff){
        gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);
    } else{
        gl_FragColor = vec4(0.1254, 0.2745, 0.1960, 1.0);
    }
}