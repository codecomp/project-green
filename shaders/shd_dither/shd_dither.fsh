varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform vec2 texSize;
uniform sampler2D ditherTex;

float height = 2.;
vec2 size = vec2(4, 4);
vec2 tSize = vec2(8, 8);

vec2 rSize = size / tSize;

void main()
{
    float band = floor(texture2D( gm_BaseTexture, v_vTexcoord ).r * 3.);
	vec2 offset = vec2(rSize.x * floor(band / height), rSize.y * mod(band, height));
    vec2 texcoord = vec2(
		(mod((v_vTexcoord.x * texSize.x), size.x) / (tSize.x * 1.)),
        (mod((v_vTexcoord.y * texSize.y), size.y) / (tSize.y * 1.))
	);
    gl_FragColor = texture2D(ditherTex, texcoord + offset);
}