# Project Green
---

##### Theme / Setting / Genre
Traditional top down Zelda style RPG focusing on avoiding damage and adventuring. Designed to fit with a larger screen GameBoy style aesthetic. Akin to if graphical fidelity did not advance since 1990.

##### Core Gameplay Mechanics Brief
 - 2D melee combat
 - Archery ranged damage and special effects
 - Magic system for combat and utility
 - Leveling up via trainers in key locations and artifacts
 - Monsters re-spawn on death, rest, or save leaving players able to travel to an extended degree without getting stuck
 - Simple switch, key and block puzzles
 - Differing hazards such as water, spikes, slow terrain and drops
 - Multiple tiers of environment that player can drop down/climb up

##### Influences
 - **Zelda**
   Expansive contiguous world with ability to return to older areas with new skills and powers. Simple map that reveals during exploration.
 - **Ultima Underworld**
   Sprawling world with deep lore that remains hidden without delving into conversations/items. Full control over gear, magic and stat distribution. Map reveals during gamneplay, full ability to put notes on map. Great conversation system for giving players user agency in their character.
 - **Hollow Knight**
   Modern take on metroidvania, simple design and controls for ease of pickup. Simple interaction world environment, up to talk/enter/interact buildings, simple interface, limited options for advancement but open ended progression. Amazing visual effects, lots of foreground/background interactions. Simple but well used monster AI.
 - **Dark Souls**
   Unforgiving mechanics that require player to learn the layout/mechanics of an area before they're fully able to progress through it. Ability to return to earlier areas with upgraded stats/equipment and feel powerful.
   
##### Road map
 - Game save / loading
 - Player dash / roll state
 - Player death state
 - Player animations
 - Player Re-spawning
 - Item pickups
 - Upgrades (flippers, magic, etc)
 - Locked chests
 - Locked doors
 - Pushable blocks
 - Clean up tilesets
 - Create sprite sheets outside GMS for importing
 - Seperate tilesets into flooring / walls
 - Standardise room layeing
 - Sound efefcts
 - Music
 
 ##### Ideas
  - Classical music 8 bit tracks for audio
  - instead of multiple hot swapped items mapped to A and B mutlipel weapons with alternate B moves, roll forwards, jump bakc, shield, teleport...
  - Instead of money dropping award "marks" for killing monsters
  - Cause the player trauma the more horrific monsters they encounter / kill, traua might cause the game to go, weird, glitchy untill rest is done
  - Timer untill the hunts reset? Player has to get to the board, find new enemy and get back in time?
  - Open world, no lockouts of exploration all over
  - Calming enviromental soundtrack (Fi's Gratitude, Crystal bearers main theme)