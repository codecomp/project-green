/// @description Update previous collision data

// Update animations
update_facing();
update_sprite_scale();
update_sprite_index();

// Store old collision data for next step
check_top_previous = check_top;
check_right_previous = check_right;
check_bottom_previous = check_bottom;
check_left_previous = check_left;