///@description Initialise

// Run parent initialisation
event_inherited();

// Initialise the collision detection and movement variables
initialize_movement_entity(0.1, 0.2, 0.9, 0.9);

// Setup array to contain directional and state sprites
sprites = ds_map_create();

// Game variables
hp = -1; // Health -1 immortal, 0 dead, positive numbers alive

// Determine the size of pathfinding grid this object will use
path_size = max(ceil(sprite_width / 4), ceil(sprite_height / 4)) - 1;

hit = false;