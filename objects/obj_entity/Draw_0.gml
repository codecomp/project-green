/// @description Stagger flash

if(alarm[1] > -1) { 
    shader_set(shd_hit);
    draw_self();
    shader_reset();
} else {
    draw_self();
}