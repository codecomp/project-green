{
    "id": "877e45fd-7c28-4439-bdb7-a255b5a05944",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_entity",
    "eventList": [
        {
            "id": "4f6aa35b-9b0a-473a-a547-1dda826281de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "877e45fd-7c28-4439-bdb7-a255b5a05944"
        },
        {
            "id": "f40e82e9-d79a-4061-a908-37eecce82cf3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "877e45fd-7c28-4439-bdb7-a255b5a05944"
        },
        {
            "id": "e46b4bcd-ef57-4489-884f-4367cb213c79",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "877e45fd-7c28-4439-bdb7-a255b5a05944"
        },
        {
            "id": "b80f4078-552c-4bba-9b7b-a4cce272c036",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "877e45fd-7c28-4439-bdb7-a255b5a05944"
        },
        {
            "id": "27e7ca2b-e36e-4e6e-ade5-798d20100681",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "877e45fd-7c28-4439-bdb7-a255b5a05944"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}