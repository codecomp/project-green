/// @description Initilise

// Time in seconds between hunts
duration = 600 * room_speed;

// Define the hunts list
hunts = ds_list_create();

// Add enemies to the gutn listings [name, object, quantity, score]
ds_list_add(hunts, ["Slime", obj_slime, 10, 10]);
ds_list_add(hunts, ["Frog", obj_frog, 5, 10]);
ds_list_add(hunts, ["Urchin", obj_urchin, 1, 10]);

// Randomise the hunts and start the timer
set_random_hunt();