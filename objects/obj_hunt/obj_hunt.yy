{
    "id": "119f38b4-0f2c-4a1d-bf78-d88b1af0fd83",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hunt",
    "eventList": [
        {
            "id": "b8a846b4-6bcc-43a8-bf71-c27996ebcdf0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "119f38b4-0f2c-4a1d-bf78-d88b1af0fd83"
        },
        {
            "id": "6da41ae1-f762-4f30-9cac-ab4c11592823",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "119f38b4-0f2c-4a1d-bf78-d88b1af0fd83"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}