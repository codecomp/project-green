/// @description Increment the counter

// Increment the cutoff timer
counter += duration / room_speed;

// Destroy the instance if we're finished
if( !persistant && (duration > 0 && counter > duration) || (duration < 0 && counter < 0) ){
	instance_destroy();
}