/// @description Draw the transiton

//Activate the cutoff shader
shader_set(shd_cutoff);

//Send our cutoff value to the shader
shader_set_uniform_f(cutoff, counter);

//Draw our transition
draw_sprite(transition, 0, 0, 0);

//Deactivate the cutoff shader
shader_reset();