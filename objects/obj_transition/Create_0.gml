/// @description Initalise

prevent_duplicates();

// Set how many seconds the aniamtion will run for
duration = 1;

// Set weather the animation should persist and not be auto destroyed
persistant = false;

// Get the cutoff uniform from the shader
cutoff = shader_get_uniform(shd_cutoff, "cutoff");

// Set the cutoff start (0 for start 1 for end)
counter = 0;

// Set the transition sprite to be used
transition = spr_transition_1;

// Setup the layer for transitions to run on
if( !layer_exists(layer_get_id("Transition")) ){
	layer = layer_create(-1, "Transition");
} else {
	layer = layer_get_id("Transition");
}