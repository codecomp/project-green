/// @description Knockback player

if(other.state == states.death){
	exit;
}

if(knockback_touch > 0){
	var dir = point_direction(x, y, other.x, other.y);
	set_movement_direction_speed(dir, knockback_touch, other);
	
	camera_shake(1, 0.1);
}

if(damage_touch){
	damage_entity(other, damage_touch);
}