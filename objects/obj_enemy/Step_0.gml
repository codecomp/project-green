/// @description Handle enemy state

// Update state based on available states and paramaters
choose_new_state();

switch(state){
	
	#region Idle
	case states.idle:
		
		// Move entity and collide 
		move_movement_entity();
		
		break;
	#endregion
	
	#region Patrol X
	case states.patrol_x:
	
		// Calculate the maximim speed to approach
		var targethspd = maxhspd * patrol_x;

		// Set player speed to approach new speed
		hsp[0] = approach(hsp[0], targethspd, abs(accl));
	
		// Move entity and collide 
		move_movement_entity();
		
		break;
	#endregion
	
	#region Patrol Y
	case states.patrol_y:
	
		// Calculate the maximim speed to approach
		var targetvspd = maxvspd * patrol_y;

		// Set player speed to approach new speed
		vsp[0] = approach(vsp[0], targetvspd, abs(accl));
	
		// Move entity and collide 
		move_movement_entity();
		
		break;
	#endregion
	
	#region Wander
	case states.wander:
		
		// Calculate the maximim speed to approach
		var targethspd = (maxhspd * 0.5) * haccl;
		var targetvspd = (maxvspd * 0.5) * vaccl;

		// Set player speed to approach new speed
		if(haccl != 0){
			hsp[0] = approach(hsp[0], targethspd, abs(accl));
		}
		if(vaccl != 0){
			vsp[0] = approach(vsp[0], targetvspd, abs(accl));
		}
	
		// Move entity and collide 
		move_movement_entity();
		
		break;
	#endregion
	
	#region Converge
	case states.converge:
	
		var dir = point_direction(x, y, obj_player.x, obj_player.y);
		haccl = lengthdir_x(1, dir);
		vaccl = lengthdir_y(1, dir);

		// Calculate the maximim speed to approach
		var targethspd = maxhspd * sign(haccl);
		var targetvspd = maxvspd * sign(vaccl);

		// Set player speed to approach new speed
		if(haccl != 0){
			hsp[0] = approach(hsp[0], targethspd, abs(accl));
		}
		if(vaccl != 0){
			vsp[0] = approach(vsp[0], targetvspd, abs(accl));
		}
	
		// Move entity and collide 
		move_movement_entity();
		
		break;
	#endregion
	
	#region Path
	case states.path:
		// If we don't have a player or the entity is too big change state
		if( !instance_exists(obj_pathfinder) || path_size > array_length_1d(obj_pathfinder.grid_ground) ){
			choose_new_state();
			exit;
		}
		
		// Make a new path and try to path to the player, if no path available change state
		path = path_add();
		if( mp_grid_path(obj_pathfinder.grid_ground[path_size], path, x, y, obj_player.x, obj_player.y, true) ){
			path_start(path, 0.5, 3, 0);
		} else if( mp_grid_path(obj_pathfinder.grid_ground[path_size], path, x, y, obj_player.x-1, obj_player.y-1, true) ){
			path_start(path, 0.5, 3, 0);
		} else {
			choose_new_state();
		}
		
		break;
	#endregion
	
	#region Orbit
	case states.orbit:
	
		// Move entity and collide 
		move_movement_entity();
		
		break;
	#endregion
	
	#region Stagger
	case states.stagger:
	
		// End the stagger state
		if(alarm[1] == 0){
			choose_new_state();
		}
	
		// Move entity and collide 
		move_movement_entity();
		
		break;
	#endregion
	
	#region Death
	case states.death:
		
		// Destroy the object
		instance_destroy();
		
		break;
	#endregion
	
}