/// @description Setup object

// Run parent initialisation
event_inherited();

// enemy specific movement variables
haccl = 0;
vaccl = 0;

// Setup enemy variables
state = states.idle;
sight = 50; // Distance between enemy and player for chasing
chase_multiplier = 1.4; // Difference between triggering chase and going back to idle
available_states = ds_list_create();
available_attacks = [];
knockback_resistance = 1; // Resistance to player knockback
path = -1;

// Setup touch variables
damage_touch = 1; // Determines damage inflicted to player on collision
knockback_touch = 1.5; // Knockback applied to player on collision