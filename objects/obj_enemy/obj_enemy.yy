{
    "id": "f7abd7f6-8c38-4083-a167-a39ac18cb420",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy",
    "eventList": [
        {
            "id": "e28b0916-ce71-430b-81f1-45ea3dcb5b71",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f7abd7f6-8c38-4083-a167-a39ac18cb420"
        },
        {
            "id": "5a7ab86d-a900-47eb-8d2b-e7b5752bd534",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "019492c9-982a-4b86-8249-b342c2411c49",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f7abd7f6-8c38-4083-a167-a39ac18cb420"
        },
        {
            "id": "86bd64f6-debd-4547-a6a9-b4244a8342eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f7abd7f6-8c38-4083-a167-a39ac18cb420"
        },
        {
            "id": "c54e2160-6361-4205-9a02-b5810eacce18",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "f7abd7f6-8c38-4083-a167-a39ac18cb420",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f7abd7f6-8c38-4083-a167-a39ac18cb420"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "877e45fd-7c28-4439-bdb7-a255b5a05944",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}