{
    "id": "8ec298ab-657f-400b-a23f-d36d5432de3e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hunt_sign",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "0731cbda-729b-4121-8263-927a7c3269ba",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "786c8ac4-4fdf-40d0-9894-e0f19d0d2da3",
    "visible": true
}