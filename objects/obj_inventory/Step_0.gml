/// @description 

if( check_input(input.menu) ){

	// Ease in the inventory
	radius = approach(radius, max_radius, 2);

	// Handle user input
    if( animating == 0 && alarm[0] == -1 ){
		if( check_input(input.left) ){
			animating = 1;
		
	        if(active < array_length_1d(items)-1){
	            active++;
	        } else {
	            active = 0;
	        }
	        alarm[0] = room_speed / 2;
	    } else if( check_input(input.right) ){
			animating = -1;
		
	        if(active > 0){
	            active--;
	        } else {
	            active = array_length_1d(items)-1;
	        }
	        alarm[0] = room_speed / 2;
	    }
	}
	
	// Calculate rotation angles
	if(animating != 0){
		var item_count = array_length_1d(items);

		// Calaulate the new angle to move to be a consistant speed no matter the item count
		angle += rotation_speed / item_count * animating;
		
		// Keep the angle positive
		if( angle < 0 )
			angle += 360;
		else if (angle > 360)
			angle -= 360;
		
		// Calculate the andle to animate to and clamp there
		var max_angle = (360/item_count) * active;
		
		// Flip the absolute angles to allow checking if we passed the angles fluidly
		if( max_angle == 0  && animating > 0 )
			max_angle = 360;
		else if (max_angle == 360 && animating < 0)
			max_angle = 0;
		
		// Stop aniamtion once we reach or go past our max angle
		if( (animating > 0 && angle >= max_angle) || (animating < 0 && angle <= max_angle) ){
			animating = 0;
			angle = max_angle;
		}
	}
} else if( animating != 0 ){
	var item_count = array_length_1d(items);

	// Set to the old active position
	active -= animating;
	if( active < 0 )
		active = item_count-1;
	else if( active > item_count-1 )
		active = 0;
		
	// Stop reaniamting on reopen of menu
	animating = 0;
	
	// Move the angle back to the old active position
	angle = (360/item_count) * active;
}

if( radius > 0 && !check_input(input.menu) ) {
	// Ease out the inventory
	radius = approach(radius, 0, 3.2);
}