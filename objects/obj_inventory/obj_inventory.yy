{
    "id": "c336db3e-2627-4615-a38f-71e691d59357",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_inventory",
    "eventList": [
        {
            "id": "f6481564-55ea-41fb-a480-7455b3e57db8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c336db3e-2627-4615-a38f-71e691d59357"
        },
        {
            "id": "a56dc20f-c701-4b4d-945b-44a7dd734146",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "c336db3e-2627-4615-a38f-71e691d59357"
        },
        {
            "id": "b69f376a-bc9c-4cd9-95d7-13276cfd3223",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c336db3e-2627-4615-a38f-71e691d59357"
        },
        {
            "id": "c7dcf405-eede-442e-aaff-037bab39471c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c336db3e-2627-4615-a38f-71e691d59357"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}