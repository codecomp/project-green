/// @description Draw Inventory

if( radius > 0 ){
	var item_count = array_length_1d(items);
	
	// Draw items on screen
	for(var i=0; i<item_count; i++){
		var _angle = (i * (360/item_count) + angle) + 90;
		var _x = camera_get_width()/2+lengthdir_x(radius,_angle);
		var _y = camera_get_height()/2+lengthdir_y(radius,_angle);
		
		// Draw outline
		draw_set_color(global.c_1)
		draw_circle(_x,_y,7,false);
		
		// Draw interior
		draw_set_color(global.c_4)
		draw_circle(_x,_y,6,false);
		
		// TODO draw item backdrop
		
		draw_sprite(items[i], 0, _x+1, _y+1);
	}
}