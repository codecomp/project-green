/// @description Initalise

// Setup surface variable
surface = noone;

// Get texture from sprite
dither_tex = sprite_get_texture(spr_dither, 0);

// Get references to variables in shader
u_size = shader_get_uniform(shd_dither, "texSize");
u_dither_tex = shader_get_sampler_index(shd_dither, "ditherTex");

// Setup colour for player lighting gradient
light_dark = make_color_rgb(180, 180, 180);

// Set the ambient light level of the room
ambient_light = 0; // 0 none | 1 medium | 2 full

// Lighting modifier based on current time
time_mod = 0;