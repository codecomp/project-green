/// @description Draw lighting

// Don't draw shadows/lighting if ambient light is full
if( ambient_light == 2 )
	exit;

// Setup texture smoothness and blending to merge light sources together
gpu_set_texfilter(false);
gpu_set_blendmode(bm_add);

// Make sure we have a surface to draw on
if(!surface_exists(surface)){
	surface = surface_create(obj_camera.width, obj_camera.height);
}

// Setup the surface and paint default colour
surface_set_target(surface);
draw_clear_alpha(c_black, 1.0);

// Draw lighting
switch( ambient_light ){
	case 0:
		with(obj_player){
			var size = (light_size + obj_lighting.time_mod);
			draw_circle_colour(x-camera_get_x_offset(), y-camera_get_y_offset(), size*0.6, c_white, c_black, false);
			draw_circle_colour(x-camera_get_x_offset(), y-camera_get_y_offset(), size, c_white, c_black, false);
		}
		with(obj_torch){
			draw_circle_colour(x-camera_get_x_offset(), y-camera_get_y_offset(), light_size*0.5, c_white, c_black, false);
		    draw_circle_colour(x-camera_get_x_offset(), y-camera_get_y_offset(), light_size, c_white, c_black, false);
		}
		break;
	case 1:
		draw_set_color(c_white);
		draw_set_alpha(0.5);
		draw_rectangle(camera_get_x_offset() - camera_get_width(), camera_get_y_offset() - camera_get_height(), camera_get_x_offset() + camera_get_width(), camera_get_y_offset() + camera_get_height(), false);
		with(obj_player){
			var size = (light_size + obj_lighting.time_mod);
			draw_circle_colour(x-camera_get_x_offset(), y-camera_get_y_offset(), size*0.9, c_white, c_black, false);
		}
		with(obj_torch){
			draw_circle_colour(x-camera_get_x_offset(), y-camera_get_y_offset(), light_size*0.8, c_white, c_black, false);
		}
		draw_set_alpha(1);
		break;
}

// Reset target to main canvas
surface_reset_target();

// Reset blending
gpu_set_blendmode(bm_normal);

// Setup the shader
shader_set(shd_dither);
shader_set_uniform_f(u_size, obj_camera.width, obj_camera.height);
texture_set_stage(u_dither_tex, dither_tex);

// Draw the surface onto the screen relative to the camera
draw_surface(surface, camera_get_x_offset(), camera_get_y_offset());

// Reset shader used on main canvas
shader_reset();