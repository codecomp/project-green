/// @description Increment / Decrement time

// Calculate the time lighting modifier
var day_ms = obj_time.day_length*1000
if( obj_time.day_ms > day_ms/24*6 && obj_time.day_ms < day_ms/24*7 ){ // Dawn
	time_mod = ease_in_out_expo(obj_time.day_ms-day_ms/24*6, 0, 200, day_ms/24);
} else if( obj_time.day_ms > day_ms/24*21 && obj_time.day_ms < day_ms/24*22 ) { // Dusk
	time_mod = ease_in_out_expo(obj_time.day_ms-day_ms/24*21, 200, -200, day_ms/24);
}