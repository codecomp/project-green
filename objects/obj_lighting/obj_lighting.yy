{
    "id": "d81f420b-dd26-45a3-8707-3cd7d418f1f6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_lighting",
    "eventList": [
        {
            "id": "e762e962-b367-4017-adcb-762403bdac66",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d81f420b-dd26-45a3-8707-3cd7d418f1f6"
        },
        {
            "id": "557a3d38-50af-483a-9ad1-e81a31a1838a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "d81f420b-dd26-45a3-8707-3cd7d418f1f6"
        },
        {
            "id": "e941c316-35c0-4112-8653-a95aa0861f06",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d81f420b-dd26-45a3-8707-3cd7d418f1f6"
        },
        {
            "id": "b5883516-1665-41cb-88e7-00f2c3528867",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d81f420b-dd26-45a3-8707-3cd7d418f1f6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}