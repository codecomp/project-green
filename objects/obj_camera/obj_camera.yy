{
    "id": "a73d0c10-e9ac-4c54-b6b9-96e2f78889a9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_camera",
    "eventList": [
        {
            "id": "e0224dbe-2472-4858-87df-98bb76e5da8a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a73d0c10-e9ac-4c54-b6b9-96e2f78889a9"
        },
        {
            "id": "593a49be-c69e-482b-8a18-8f11cb100bc3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a73d0c10-e9ac-4c54-b6b9-96e2f78889a9"
        },
        {
            "id": "9a35f1c0-7ba5-4fb6-9014-6b79a9957897",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "a73d0c10-e9ac-4c54-b6b9-96e2f78889a9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}