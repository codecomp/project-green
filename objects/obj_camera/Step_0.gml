///@description Move camera

// Move camera to new position
if( abs(x - x_to) > 1 ) x = lerp(x, x_to, 0.1);
if( abs(y - y_to) > 1 ) y = lerp(y, y_to, 0.1);

// Apply screen shake if required
if(shake_intensity > 0){
	x += random_range(-shake_intensity, shake_intensity);
	y += random_range(-shake_intensity, shake_intensity);
	shake_intensity = approach(shake_intensity, 0, shake_decay);
}

// Stop camera moving out of bounds of the room
if(room_lock){
	x = clamp(x,0+(width/2),room_width-(width/2));
	y = clamp(y,0+(height/2),room_height-(height/2));
}

// Sets the camera destination to the rollow target
if( follow != noone && instance_exists(follow) ){
	x_to = follow.x;
	y_to = follow.y;
}

// Rebuilds and reassigns the matric with the current position of the camera
var vm = matrix_build_lookat(x,y,-10,x,y,0,0,1,0);
camera_set_view_mat(camera, vm);