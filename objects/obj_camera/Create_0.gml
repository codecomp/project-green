/// @description Initilise

prevent_duplicates();

// Setup camera properties
camera = camera_create();
follow = obj_player;
x_to = x;
y_to = y;
width = 160;
height = 144;
room_lock = true;
shake_intensity = 0;
shake_decay = 0.9;

display_set_gui_size(width, height);

// Build camera matrix
var vm = matrix_build_lookat(x,y,-10,x,y,0,0,1,0);
var pm = matrix_build_projection_ortho(width, height, 1, 10000);

// Assign camera matrix
camera_set_view_mat(camera, vm);
camera_set_proj_mat(camera, pm);