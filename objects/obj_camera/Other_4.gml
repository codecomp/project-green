/// @description Reassign camera to view

// Reassign the camera when we enter a new room
view_set_camera(0, camera);

// Reposition camera to player
if( instance_exists(follow) ){
	var _follow = follow;
	camera_warp_to(follow.x, follow.y);
	camera_set_follow(_follow);
}