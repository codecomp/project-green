{
    "id": "d7ae3ab2-eb31-4436-989b-6e6cce832196",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_urchin",
    "eventList": [
        {
            "id": "12b08d20-7c4b-4840-b75b-b5bdba10d3ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d7ae3ab2-eb31-4436-989b-6e6cce832196"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "f7abd7f6-8c38-4083-a167-a39ac18cb420",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "fe89d015-359b-4340-9854-74dd8762ee17",
    "visible": true
}