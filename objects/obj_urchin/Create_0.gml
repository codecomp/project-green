/// @description Update base variables

// Run parent initialisation
event_inherited();

// Add enemy states
ds_list_add(available_states, states.idle);

// Setup animation sprites
add_animation_sprites(
	states.idle,
	spr_urchin,
	spr_urchin,
	spr_urchin,
	spr_urchin
);