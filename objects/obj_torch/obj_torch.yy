{
    "id": "6066424d-4abd-4407-aa11-9f75608260d8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_torch",
    "eventList": [
        {
            "id": "227f14b0-68de-414e-904d-e683c15ba30a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6066424d-4abd-4407-aa11-9f75608260d8"
        },
        {
            "id": "90868a56-ef4d-42ae-b2d0-cac1cc1551ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6066424d-4abd-4407-aa11-9f75608260d8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "6f5f8feb-b4a6-4d19-9ff1-ed9a92e6e54e",
    "visible": true
}