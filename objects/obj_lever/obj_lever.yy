{
    "id": "b731ad9e-eea7-46ee-8d75-5f42d0b51156",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_lever",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "d1e8442d-6980-49a3-a644-cbd20de10539",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "fa8ef877-5913-41f9-a121-2fde86afb2e8",
    "visible": true
}