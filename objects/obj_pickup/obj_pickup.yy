{
    "id": "31ffe00a-711f-4d1b-976b-9ad36b4fb502",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_pickup",
    "eventList": [
        {
            "id": "cd6f4309-4760-4e22-823d-883e96f44498",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "019492c9-982a-4b86-8249-b342c2411c49",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "31ffe00a-711f-4d1b-976b-9ad36b4fb502"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}