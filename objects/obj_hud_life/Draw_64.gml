/// @description Draw health bar

// Drawe the dark health bar backgrond
draw_set_color(global.c_4);
draw_rectangle(x, y-1, x+width, y+1, false);

// Get the players HP% and draw the bar
if(instance_exists(obj_player)){
	var fill = obj_player.hp/obj_player.max_hp * width - 4;
	draw_set_color(global.c_3);
	draw_rectangle(x, y-1, x+fill, y+1, false);
	
	// Draw the end cap sprite
	draw_sprite(spr_hud_health_bar_end, 0, x+fill, y-1);
}

// Draw the health bar border
draw_sprite(spr_hud_health_bar, 0, x, y);

// Draw the heart sprite
draw_self();