{
    "id": "89639f37-f808-48c9-ba1c-8abd6c475dd8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hud_life",
    "eventList": [
        {
            "id": "6afa87b3-2033-4f9c-a3b0-2d47d3a164e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "89639f37-f808-48c9-ba1c-8abd6c475dd8"
        },
        {
            "id": "b7b55da4-46c3-41f1-b601-577b12ca772a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "89639f37-f808-48c9-ba1c-8abd6c475dd8"
        },
        {
            "id": "8a146326-ae14-4029-bcb0-c12c975f6ff9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "89639f37-f808-48c9-ba1c-8abd6c475dd8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "10e7a266-ffe9-4470-acd6-310dfe8fcea9",
    "visible": true
}