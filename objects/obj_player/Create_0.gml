/// @description Update base variables

// Run parent initialisation
event_inherited();

// Setup entity lighting
initialize_lighting_entity(65, 65, 0);

max_hp = 10;
hp = 10
coins = 0;

// Make player faster than default entities
maxhspd = 1.1;
maxvspd = 1.1;

// Setup player default state
state = states.idle;

// Track if user is trying to drop down a cliff
drop_counter = 0;

// Track duration in water
swim_counter = 0;

// Setup animation sprites
add_animation_sprites(
	states.idle,
	spr_player_idle_up,
	spr_player_idle_right,
	spr_player_idle_down,
	spr_player_idle_left
);

add_animation_sprites(
	states.move,
	spr_player_idle_up,
	spr_player_idle_right,
	spr_player_idle_down,
	spr_player_idle_left
);

add_animation_sprites(
	states.swim,
	spr_player_swim_up,
	spr_player_swim_right,
	spr_player_swim_down,
	spr_player_swim_left
);

add_animation_sprites(states.drop_up,spr_player_drop_up,spr_player_drop_up,spr_player_drop_up,spr_player_drop_up);
add_animation_sprites(states.drop_right,spr_player_drop_right,spr_player_drop_right,spr_player_drop_right,spr_player_drop_right);
add_animation_sprites(states.drop_down,spr_player_drop_down,spr_player_drop_down,spr_player_drop_down,spr_player_drop_down);
add_animation_sprites(states.drop_left,spr_player_drop_left,spr_player_drop_left,spr_player_drop_left,spr_player_drop_left);