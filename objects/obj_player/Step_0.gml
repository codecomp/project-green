/// @description Handle player state

trace(x, y, bbox_top, bbox_bottom, bbox_left, bbox_right);

// Update lighting variables
update_lighting();

switch(state){
	
	#region Movement
	case states.idle:
	case states.move:
	
		// Allow free movement and attacking
		enable_player_walk();
		enable_player_drop();
		move_movement_entity();
		enable_player_attack();
		
		break;
	#endregion
	
	#region Swimming
	case states.swim:
		
		// Only allow movement no attacking
		enable_player_walk();
		enable_player_drop();
		move_movement_entity();
		
		break;
	#endregion
		
	#region Drop
	case states.drop_up:
	
		// Disallow updates to movement & knockback
		drop_movement_entity(dir.up);
		
		break;
		
	case states.drop_right:
	
		// Disallow updates to movement & knockback
		drop_movement_entity(dir.right);
		
		break;
		
	case states.drop_down:
	
		// Disallow updates to movement & knockback
		drop_movement_entity(dir.down);
		
		break;
		
	case states.drop_left:
	
		// Disallow updates to movement & knockback
		drop_movement_entity(dir.left);
		
		break;
	#endregion
	
	#region Stagger
	case states.stagger:
		
		// End the stagger state
		if(alarm[1] == 0){
			state = states.idle;
		}
		
		// Move entity and collide 
		move_movement_entity();
		
		break;
	#endregion
		
	#region Death
	case states.death:
	
		// TODO reset stage
		
		break;
	#endregion
	
}

// Debugging watch vars
watch_var("Facing", facing);
watch_var("Pos", string(x) + " : " + string(y));
watch_var("vspd", vsp[0]+vsp[1]);
watch_var("hspd", hsp[0]+hsp[1]);