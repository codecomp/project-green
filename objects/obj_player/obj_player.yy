{
    "id": "019492c9-982a-4b86-8249-b342c2411c49",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "e18f5000-8e7e-4cf2-ab5d-7986a51ef224",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "019492c9-982a-4b86-8249-b342c2411c49"
        },
        {
            "id": "c6407b66-f961-4790-ac3b-d882a5be1f35",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "019492c9-982a-4b86-8249-b342c2411c49"
        },
        {
            "id": "edfef93b-f8ca-4121-b0c5-ecc9c1a6790e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "019492c9-982a-4b86-8249-b342c2411c49"
        },
        {
            "id": "9db77021-a58c-474b-a02f-a21ffd2686c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "019492c9-982a-4b86-8249-b342c2411c49"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "877e45fd-7c28-4439-bdb7-a255b5a05944",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "a8221807-362e-402d-92db-86dc1d566515",
    "visible": true
}