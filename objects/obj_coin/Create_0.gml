/// @description Initialise

// Setup sprite to use
value = 1;

// Set actual coin increment per value
coins[0] = 1;
coins[1] = 2;
coins[2] = 5;
coins[3] = 10;
coins[4] = 15;
coins[5] = 25;