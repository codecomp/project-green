{
    "id": "aa3fbdc9-d166-4c80-8723-61a5fe30724c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_coin",
    "eventList": [
        {
            "id": "f9844785-e6f1-4cc3-a77e-4bf8f9aeef75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "aa3fbdc9-d166-4c80-8723-61a5fe30724c"
        },
        {
            "id": "0b6a0ffa-da24-4964-9a70-43febbe18f95",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "aa3fbdc9-d166-4c80-8723-61a5fe30724c"
        },
        {
            "id": "6d933762-f51c-4bcd-bf6f-b0b67aaff54f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "019492c9-982a-4b86-8249-b342c2411c49",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "aa3fbdc9-d166-4c80-8723-61a5fe30724c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "31ffe00a-711f-4d1b-976b-9ad36b4fb502",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "1281a17d-8b8e-42c5-8e57-fe2ab0d8fa16",
    "visible": true
}