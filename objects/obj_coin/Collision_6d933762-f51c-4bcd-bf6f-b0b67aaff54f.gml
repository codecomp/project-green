/// @description Pickup

// Run parent collision
event_inherited();

// Increase coin count
other.coins += coins[value-1];