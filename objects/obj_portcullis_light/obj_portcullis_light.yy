{
    "id": "bd6f492a-df3f-402d-a28c-c8eeba5db391",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_portcullis_light",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "d6581456-0fd2-476a-af86-92c92a1cabe3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "21109dbc-b243-4461-b3ec-43a7d162c942",
    "visible": true
}