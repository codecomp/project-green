/// @description Move & Controls

// Start game
if(timer > 360){
	room_goto_next();
	
	// Destroy the existing camera
	if( instance_exists(obj_camera) ){
		instance_destroy(obj_camera);
	}
		
	// Make a new camera
	instance_create_layer(0, 0, "Instances", obj_camera);
}

if(alarm[0] == -1){
	if( check_input(input.up) && active_direction != input.up ){
		active_direction = input.up;
		set_application_palette(spr_palette_2);
	} else if( check_input(input.down) && active_direction != input.down ){
		active_direction = input.down;
		set_application_palette(spr_palette_3);
	} else if( check_input(input.left) && active_direction != input.left){
		active_direction = input.left;
		set_application_palette(spr_palette_4);
	} else if( check_input(input.right) && active_direction != input.right ){
		active_direction = input.right;
		set_application_palette(spr_palette_5);
	} else {
		active_direction = noone;
	}
}

if( check_input(input.up) && check_input(input.attack) ){
	set_application_palette(spr_palette_6);
	alarm[0] = room_speed/3;
} else if( check_input(input.up) && check_input(input.block) ){
	set_application_palette(spr_palette_10);
	alarm[0] = room_speed/3;
} else if( check_input(input.down) && check_input(input.attack) ){
	set_application_palette(spr_palette_7);
	alarm[0] = room_speed/3;
} else if( check_input(input.down) && check_input(input.block) ){
	set_application_palette(spr_palette_11);
	alarm[0] = room_speed/3;
} else if( check_input(input.left) && check_input(input.attack) ){
	set_application_palette(spr_palette_8);
	alarm[0] = room_speed/3;
} else if( check_input(input.left) && check_input(input.block) ){
	set_application_palette(spr_palette_12);
	alarm[0] = room_speed/3;
} else if( check_input(input.right) && check_input(input.attack) ){
	set_application_palette(spr_palette_9);
	alarm[0] = room_speed/3;
} else if( check_input(input.right) && check_input(input.block) ){
	set_application_palette(spr_palette_13);
	alarm[0] = room_speed/3;
}
