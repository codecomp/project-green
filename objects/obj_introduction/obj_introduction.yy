{
    "id": "42f9db54-6b77-48a5-a74b-8cf99510c2b9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_introduction",
    "eventList": [
        {
            "id": "f35daf3e-efe5-4e33-89f5-20ffbe0bea64",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "42f9db54-6b77-48a5-a74b-8cf99510c2b9"
        },
        {
            "id": "4928c618-eaf5-4820-956e-7f28a6d9a969",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "42f9db54-6b77-48a5-a74b-8cf99510c2b9"
        },
        {
            "id": "15627707-c0b6-4850-a62d-fbce47324d1c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "42f9db54-6b77-48a5-a74b-8cf99510c2b9"
        },
        {
            "id": "60806ae4-0a60-4abf-9522-83c4b3697d2a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "42f9db54-6b77-48a5-a74b-8cf99510c2b9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}