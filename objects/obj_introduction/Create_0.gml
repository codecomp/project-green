/// @description Initalize

timer = 0;

logo_alpha = 0;
logo_offset = 20;

u_cutoff = shader_get_uniform(shd_gradient_cutoff, "u_cutoff");

active_direction = noone;