/// @description Draw animations

timer++;

// Clear the screen
draw_set_color(global.c_1);
draw_rectangle(0, 0, camera_get_width(), camera_get_height(), false);

// Draw the logo
logo_alpha = approach(logo_alpha, 1, 0.0175);
logo_offset = approach(logo_offset, 0, 0.35);
draw_sprite_ext(spr_gamemaker_logo, 0, camera_get_width()/2, 100 + logo_offset, 1, 1, 0, c_black, logo_alpha);

// Draw the txt
if(timer > 120){
	shader_set(shd_gradient_cutoff);
	shader_set_uniform_f(u_cutoff, min((timer-120)/60, 1));
	draw_sprite(spr_gamemaker_text, 0, camera_get_width()/2, 60);
	shader_reset();
}

// Transition accross screns
if(timer > 300){
	// Create the inward transition
	instance_create_depth(0, 0, 0, obj_transition);
}