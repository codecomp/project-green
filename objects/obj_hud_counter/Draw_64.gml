/// @description Draw coin counter

// Draw counter
draw_set_font(fnt_pixeled);
draw_set_valign(fa_middle);
draw_set_color(global.c_1);
draw_text_colour_outline_exact(x+8, y-3, string_lpad(string(counter), 3, "0"), 1, global.c_4);
draw_set_valign(fa_top);

// Draw the heart sprite
draw_self();