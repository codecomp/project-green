{
    "id": "df83b2e3-3f8a-41ae-be6a-d34790042fa5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hud_counter",
    "eventList": [
        {
            "id": "c734d555-f2c8-4e34-b67e-af8bd9ec118c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "df83b2e3-3f8a-41ae-be6a-d34790042fa5"
        },
        {
            "id": "b380e366-6fad-4535-a88b-66667e20e756",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "df83b2e3-3f8a-41ae-be6a-d34790042fa5"
        },
        {
            "id": "1c452abc-dea3-493d-bea4-729ff7184e50",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "df83b2e3-3f8a-41ae-be6a-d34790042fa5"
        },
        {
            "id": "375b860a-8a58-47ea-b93a-231dc3043366",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "df83b2e3-3f8a-41ae-be6a-d34790042fa5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "90e035e7-a629-4fb3-9534-39fb75029735",
    "visible": true
}