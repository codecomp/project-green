/// @description Increment counter

timer++;

if( timer % 4 == 0 ){
	if(instance_exists(obj_player)){
		counter = approach(counter, obj_player.coins, 1);
	}
	timer = 0;
}