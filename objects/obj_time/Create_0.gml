/// @description Initialise variables

prevent_duplicates();

day_length = 600; // Length of a day in seconds
previous_time = 0;
seconds_elapsed = 0;
game_day = 1;
game_hour = 0;
game_min = 0;

day_ms = 0; // Miliseconds passed this game day to run game logic on that requires sub second accuracy