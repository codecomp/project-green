/// @description Calulate time

// Increment timer
previous_time += delta_time;
day_ms += delta_time*0.001;

// check to see if a second or more has passed
if(previous_time > 1000000){
	var s = floor(previous_time/1000000);
	previous_time -= s*1000000;
	
	// Recalculate time passed
	seconds_elapsed += s;
	
	game_day = floor(seconds_elapsed/day_length)+1;
	game_hour = floor(seconds_elapsed/day_length*24) % 24;
	game_min = floor(seconds_elapsed/day_length*24*60) % 60;

	// If a whole day has passed reset the milisecond counter
	if(seconds_elapsed % day_length == 0){
		day_ms = 0;
	}
}

watch_var("Day", game_day);
watch_var("Time", string_lpad(string(game_hour), 2, "0") + ":" + string_lpad(string(game_min), 2, "0"));