/// @description Draw extra GUI

// Display bounding boxes
if (display_bounds){
    for(var i=0; i < instance_count; i++){
        with(instance_id_get( i )){
            draw_rectangle_colour(bbox_left,bbox_top,bbox_right,bbox_bottom,c_red,c_red,c_red,c_red,true);
        }
    }
}

// Display enemy converge radius
if( display_converge && object_exists(obj_enemy) ){
	with obj_enemy{
		if( ds_list_find_index(available_states, states.converge) ){
			var xx = x - camera_get_x_offset();
			var yy = y - camera_get_y_offset();
			
			draw_set_alpha(1);
			draw_set_color(c_red);
			draw_circle(xx, yy, sight, true);
			draw_set_color(c_purple);
			draw_circle(xx, yy, sight * chase_multiplier, true);
		}
	}
}

// Display enemy pathing
if( display_path && object_exists(obj_enemy) ){
	with obj_enemy{
		if( state == states.path && path ){
			var xx = x - camera_get_x_offset();
			var yy = y - camera_get_y_offset();
			
			draw_set_alpha(1);
			draw_set_color(c_teal);
			draw_path(path, xx, yy, false);
		}
	}
}