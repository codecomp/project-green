/// @description Debug Variables

prevent_duplicates();

if(!layer_exists("Console")){
	var layer_id = layer_create(-10000, "Console");
	layer = layer_id;
}

// Global variable so you can check within other objects if the console is on
global.debug = false;

// Handle weather we are entering text into the debugger
input_state = false;

// Log of past debug commands you've typed
debug_log = ds_list_create();
ds_list_add(debug_log, "Type help to see commands");

// Current code you've typed
debug_textinput = "";
command = "";

// Info string
info[0, 0] = "";
info[0, 1] = "";
infowidth = 0;

// Target object to move when you've clicked on it
drag = false;
target = -1;

// Boolean to show hitboxes
display_bounds = false;

// Boolean to move camera
free_camera = false;

// Boolean to show converve bounds
display_converge = false;

// Boolean to show path
display_path = false;

// Camera move speed
camera_speed = 5;

// Timer for the cursor in the input
cursor_timer = 0;

// Window Positions
dragging = false;
draglog = false;
draginfo = false;
logx = 0;
logy = window_get_height();
infox = 0;
infoy = 0;

// Setup surface
gui_surface = noone;