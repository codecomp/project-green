{
    "id": "cab25106-3eb2-42dc-8a2c-203e9cd14243",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_console",
    "eventList": [
        {
            "id": "183ad280-89d4-4019-8cfc-aa2d28de9cdc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cab25106-3eb2-42dc-8a2c-203e9cd14243"
        },
        {
            "id": "619bca01-96a6-42dd-8ac8-5c88d04698e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cab25106-3eb2-42dc-8a2c-203e9cd14243"
        },
        {
            "id": "d12e78cc-3cab-40de-bec6-dd7facf15959",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "cab25106-3eb2-42dc-8a2c-203e9cd14243"
        },
        {
            "id": "74d7c524-4e2e-48c8-a1f4-ed190b3ae8ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "cab25106-3eb2-42dc-8a2c-203e9cd14243"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}