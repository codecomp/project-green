/// @description Console

// Turn debug on/off keys are ` and F1
if (keyboard_check_released(192) || keyboard_check_released(vk_f1)){
    debug_textinput = "";
    keyboard_string = "";
    global.debug = !global.debug;
	
	// Turn text input on by default
	if(global.debug == true){
		input_state = true;
	}
}

// Debug is on
if (global.debug && input_state){
    // Keyboard input
    debug_textinput = keyboard_string;
    if (string_width(string_hash_to_newline(keyboard_string)) > 142) keyboard_string = string_copy(keyboard_string, 0, string_length(keyboard_string)-1);
    
    // Delete oldest entry in log if more than 10
    if (ds_list_size(debug_log) > 20){
        while (ds_list_size(debug_log) > 20) ds_list_delete(debug_log, 0);
    }
    
    // Execute
    if (keyboard_check_pressed(vk_enter)){
        // Print to log
        ds_list_add(debug_log, debug_textinput);
        
        // Get Command
        if (string_pos(" ", debug_textinput) != 0){
            var i = string_pos(" ", debug_textinput);
            command = string_copy(debug_textinput, 0, i-1);
            debug_textinput = string_copy(debug_textinput, i+1, string_length(debug_textinput));
        }
        else{
            command = debug_textinput;
        }
        
        // Execute Command
        switch(command){
            // Drag objects
            case "drag":
                drag = !drag;
                if (drag) ds_list_replace(debug_log, ds_list_size(debug_log)-1, "drag ON");
                else ds_list_replace(debug_log, ds_list_size(debug_log)-1, "drag OFF");
                break;
            // Move Camera
            case "cam":
                free_camera = !free_camera;
                if (free_camera) ds_list_replace(debug_log, ds_list_size(debug_log)-1, "cam ON");
                else ds_list_replace(debug_log, ds_list_size(debug_log)-1, "cam OFF");
                break;
            // Switch room
            case "room":
                if (room_exists(asset_get_index(debug_textinput))) room_goto(asset_get_index(debug_textinput));
                else ds_list_replace(debug_log, ds_list_size(debug_log)-1, "invalid room");
                break;
            // Spawn object
            case "spawn":
                var obj = asset_get_index(debug_textinput);
                //Spawns in the middle of the screen assuming you use views
                if (obj > 0) instance_create_layer(obj_camera.x, obj_camera.y, "Instances", obj);
                else ds_list_replace(debug_log, ds_list_size(debug_log)-1, "invalid object");
                break;
            // Restart
            case "restart":
                if (debug_textinput == "room") room_restart();
                else if (debug_textinput == "game") game_restart();
                else ds_list_replace(debug_log, ds_list_size(debug_log)-1, "invalid");
                break;
            // Show Hitboxes
            case "hitbox":
                display_bounds = !display_bounds;
                if (display_bounds) ds_list_replace(debug_log, ds_list_size(debug_log)-1, "hitbox ON");
                else ds_list_replace(debug_log, ds_list_size(debug_log)-1, "hitbox OFF");
                break;
			// Show collision layer
			case "collision":
				var colision_tilemap_id = layer_get_id("TilesCollision");
				layer_set_visible(colision_tilemap_id, !layer_get_visible(colision_tilemap_id));
				if( layer_get_visible(colision_tilemap_id) ){
					ds_list_replace(debug_log, ds_list_size(debug_log)-1, "collision ON");
				} else {
					ds_list_replace(debug_log, ds_list_size(debug_log)-1, "collision OFF");
				}
				break;
			// Show enemy converge radius
			case "converge":
				display_converge = !display_converge;
				if(display_converge){
					ds_list_replace(debug_log, ds_list_size(debug_log)-1, "convrege ON");
				} else {
					ds_list_replace(debug_log, ds_list_size(debug_log)-1, "convrege OFF");
				}
				break;
			// Show enemy pathing
			case "path":
				display_path = !display_path;
				if(display_path){
					ds_list_replace(debug_log, ds_list_size(debug_log)-1, "pathing ON");
				} else {
					ds_list_replace(debug_log, ds_list_size(debug_log)-1, "pathing OFF");
				}
				break;
			// Set ambient lighting
			case "ambient":
				set_ambient_light(real(debug_textinput));
				break;
			// Set colour palette
			case "palette":
				set_application_palette(asset_get_index(debug_textinput));
				break;
            // Help
            case "help":
                ds_list_replace(debug_log, ds_list_size(debug_log)-1, "Commands:");
                ds_list_add(debug_log, "drag - enable object drag");
                ds_list_add(debug_log, "cam - enable free cam");
                ds_list_add(debug_log, "room - switch rooms");
                ds_list_add(debug_log, "spawn - spawn object");
                ds_list_add(debug_log, "restart - restart game/room");
                ds_list_add(debug_log, "hitbox - show hitboxes");
				ds_list_add(debug_log, "ambient - set ambient light level");
				ds_list_add(debug_log, "collision - toggle collision layer");
				ds_list_add(debug_log, "          - toggle enemy radius");
				ds_list_add(debug_log, "          - toggle enemy pathing");
                break;
            // Invalid Code
            default:
                ds_list_replace(debug_log, ds_list_size(debug_log)-1, "invalid code");
                break;
        }
        
        // Clear text
        command = "";
        keyboard_string = "";
    }
}

/// Free Camera

// This assumes you use views.
// Adjust this code if it does not work in your game.
if (global.debug && free_camera && view_enabled)
{
    if (keyboard_check_direct(vk_up)){
        obj_camera.y -= camera_speed;
    }
    else if (keyboard_check_direct(vk_down)){
        obj_camera.y += camera_speed;
    }
    if (keyboard_check_direct(vk_right)){
        obj_camera.x += camera_speed;
    }
    else if (keyboard_check_direct(vk_left)){
        obj_camera.x -= camera_speed;
    }
}

/// Move Objects
if (global.debug){
    // Get object from mouse's position
    if (target < 0 && mouse_check_button(mb_left)
    && !point_in_rectangle(device_mouse_x_to_gui(0), device_mouse_y_to_gui(0), logx, logy-155, logx+152, logy)
    && !point_in_rectangle(device_mouse_x_to_gui(0), device_mouse_y_to_gui(0), infox-3, infoy, infox+15+infowidth, infoy+(array_height_2d(info)*12)))
    {
        target = instance_position(mouse_x, mouse_y, all);
    }
    // If object selected and dragging is enabled
    if (instance_exists(target) && drag){
        if (mouse_check_button(mb_left)){
            target.x = mouse_x;
            target.y = mouse_y;
        }
        else target = -1;
    }
}

/// Watch Variables
watch_var("FPS", fps_real);
watch_var("Objects", instance_count);
watch_var("Room", room_get_name(room));