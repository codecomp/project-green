/// @description Draw Debug Screen

if (global.debug){
	
	if(!surface_exists(gui_surface)){
		gui_surface = surface_create(window_get_width(), window_get_height());
	}
	
	// Set GUI scale to be real pixel sizes for console
	var pos = application_get_position();
	var gui_width = display_get_gui_width();
	var gui_height = display_get_gui_height();
	display_set_gui_maximise(1, 1, pos[0], pos[1]);
	
	// Setup the surface and paint default colour
	surface_set_target(gui_surface);
	draw_clear_alpha(c_black, 0);

    // Draw input text box
    draw_set_colour(c_black);
    draw_set_alpha(0.7);
    draw_rectangle(logx, logy, logx+300, logy-12, false);
    draw_set_alpha(1);
	
    // Draw input text
    tX = logx+3;
    tY = logy-10;
    draw_set_font(fnt_console);
    draw_set_colour(c_white);
    draw_set_halign(fa_left);
    draw_text(tX, tY, string_hash_to_newline(">"));
    draw_text(tX+9, tY, string_hash_to_newline(debug_textinput));
    if (cursor_timer > 40) cursor_timer = 0;
    if (cursor_timer > 20 && input_state) draw_text(tX+9+string_width(string_hash_to_newline(debug_textinput)), tY, string_hash_to_newline("|"));
    cursor_timer++;
	
    // Debug log box
    draw_set_colour(c_black);
    draw_set_alpha(0.5);
    draw_rectangle(logx, logy-12, logx+300, logy-218, false);
    draw_set_alpha(1);
	
    // Draw log text
    tX = logx+3;
    tY = logy-15;
    draw_set_colour(c_gray);
    draw_set_halign(fa_left);
    for(var i=0; i < ds_list_size(debug_log); i++){
        draw_text(tX, tY-(ds_list_size(debug_log)*10)+(i*10), string_hash_to_newline(debug_log[| i]));
    }
	
    // Draw Info Box
    draw_set_colour(c_black);
    draw_set_alpha(0.7);
    draw_rectangle(infox-3, infoy, infox+15+infowidth, infoy+(array_height_2d(info)*12)-9, false);
    draw_set_alpha(1);
	
    // Draw Info
    tX = infox+3;
    tY = infoy+3;
    draw_set_colour(c_white);
    draw_set_halign(fa_left);
    for(var i=1; i<array_height_2d(info); i++){
        draw_text(tX, tY+(12*(i-1)), string_hash_to_newline(string(info[i, 0]) + ": " + string(info[i, 1])));
        if (string_width(string_hash_to_newline(string(info[i, 0])+string(info[i, 1]))) > infowidth) infowidth = string_width(string_hash_to_newline(string(info[i, 0])+string(info[i, 1])));
    }
    
    // Drag windows
    if (point_in_rectangle(device_mouse_x_to_gui(0), device_mouse_y_to_gui(0), logx, logy-218, logx+300, logy-12) && mouse_check_button_pressed(mb_left)){
        draglog = true;
    } else if (point_in_rectangle(device_mouse_x_to_gui(0), device_mouse_y_to_gui(0), infox-3, infoy, infox+15+infowidth, infoy+(array_height_2d(info)*12)) && mouse_check_button_pressed(mb_left)){
        draginfo = true;
    }
    
    if (draglog){
        if (mouse_check_button(mb_left)){
            logx = device_mouse_x_to_gui(0)-75;
            logy = device_mouse_y_to_gui(0)+75;
        }
        else draglog = false;
    }
    if (draginfo){
        if (mouse_check_button(mb_left)){
            infox = device_mouse_x_to_gui(0)-(15+infowidth)/2;
            infoy = device_mouse_y_to_gui(0)-(array_height_2d(info)*12)/2;
        }
        else draginfo = false;
    }
	
	// Turn text inptu on/off
	if (point_in_rectangle(device_mouse_x_to_gui(0), device_mouse_y_to_gui(0), logx, logy-218, logx+300, logy) && mouse_check_button_pressed(mb_left)){
	    input_state = true;
	} else if ( mouse_check_button_pressed(mb_left) ){
	    input_state = false;
	}
	
	// Reset the surface
	surface_reset_target();
	
	// Reset GUI scale
	display_set_gui_maximise(-1, -1);
	display_set_gui_size(gui_width, gui_height);
}


