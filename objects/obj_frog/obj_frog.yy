{
    "id": "128c5827-ed15-4079-8bad-aad792113eae",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_frog",
    "eventList": [
        {
            "id": "3f10023b-d9d2-4796-a5ae-ba2802a96549",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "128c5827-ed15-4079-8bad-aad792113eae"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "f7abd7f6-8c38-4083-a167-a39ac18cb420",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "30913476-42ff-482c-bce2-8a5f95fc5866",
    "visible": true
}