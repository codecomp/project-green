/// @description Update base variables

// Run parent initialisation
event_inherited();

facing = dir.right;

hp = 5;

// Add enemy states
ds_list_add(available_states, states.idle);
ds_list_add(available_states, states.wander);
ds_list_add(available_states, states.converge);
//ds_list_add(available_states, states.path);

// Setup animation sprites
add_animation_sprites(
	states.idle,
	spr_frog_idle_up,
	spr_frog_idle_down,
	spr_frog_idle_down,
	spr_frog_idle_down
);