{
    "id": "4d66dd54-c7d7-4a04-a37a-d18682a09edd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_pathfinder",
    "eventList": [
        {
            "id": "0f566ab6-55ec-4dd7-ad70-0c406a093544",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4d66dd54-c7d7-4a04-a37a-d18682a09edd"
        },
        {
            "id": "3731bfe3-14c3-4c0c-ba15-784d6b096407",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "4d66dd54-c7d7-4a04-a37a-d18682a09edd"
        },
        {
            "id": "8ca34ae1-d3dd-45b5-bc43-1a6b3cd534c8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4d66dd54-c7d7-4a04-a37a-d18682a09edd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}