/// @description Generate MPs

/*
	Setup pathfinding maps based off the rooms collision tile layter.
	Create seperate grids for every size of entity, as the tile grid is
	4x4 we check squares of space for eack size 4x4, 8x8, 12x12 etc
	ointo a seperate path finding grid they can use to avoid clipping.
*/

var layr = layer_get_id("TilesCollision");
tilemap_id = layer_tilemap_get_id(layr);
max_clearance = 4;
tiles_x = tilemap_get_width(tilemap_id);
tiles_y = tilemap_get_height(tilemap_id);
tile_width = tilemap_get_tile_width(tilemap_id);
tile_height = tilemap_get_tile_height(tilemap_id);
tilemap_array = tilemap_get_array(tilemap_id);

// Setup ground collision maps for every available size
grid_ground = [];
for(var c=0; c<max_clearance; c++){
	grid_ground[c] = mp_grid_create((c*tile_width)/2, (c*tile_height)/2, tiles_x, tiles_y, tile_width, tile_height);
}

// Calculate the clearence of every tile
for(var xx=0; xx<tiles_x; xx++){
	for(var yy=0; yy<tiles_y; yy++){
		var clearance = check_array_square(0, max_clearance, tilemap_array, xx, yy);
		// Add the clearence of every tile into its respective map
		for(var c=0; c<max_clearance-clearance; c++){
			// Add clearences in reverse order so 1=grid1 2=grid2 etc...
			mp_grid_add_cell(grid_ground[max_clearance-c-1], xx, yy);
		}
	}
}