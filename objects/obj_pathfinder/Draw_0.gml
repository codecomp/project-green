/// @description Test draw MP grids


draw_set_alpha(0.2);

if(keyboard_check(ord("1"))){
	mp_grid_draw(grid_ground[0]);
	draw_set_alpha(1);
}

if(keyboard_check(ord("2"))){
	mp_grid_draw(grid_ground[1]);
	draw_set_alpha(1);
}

if(keyboard_check(ord("3"))){
	mp_grid_draw(grid_ground[2]);
	draw_set_alpha(1);
}

if(keyboard_check(ord("4"))){
	mp_grid_draw(grid_ground[3]);
	draw_set_alpha(1);
}

draw_set_alpha(1);
