/// @description Initilisation

prevent_duplicates();

messages = ds_queue_create();
timer = 0; // Timer for displaying charatcers
delay = 3; // Delay between characters
cutoff = 0; // Counter for displayed characters
current_portrait = "none"; // Current set portrait by dialogue_set_portrait

line_width = 130; // Pixel diwth of the area to write text in
line_start = 7;

t = 0; // Sine Timer
amp = 1; // Amplitude of sine
freq = 2; // Frequency of sine