/// @description Draw dialogue box

// Increment timer
t++;

if( !ds_queue_empty(messages) ){

	// The message
	var message = ds_queue_head(messages);
	var portrait = message[0];
	var text = message[1];

	// Text position
	if( portrait == "none" ) var tx = 7; else var tx = 60;
	var ty = camera_get_height() - 40;
	var tx = line_start;
	
	// Rendering
	var modifier = 0; // Modifier applied to text
	var line = 0; // current line
	var i = 1;
	
	
	// Typewriter
	if(cutoff < string_length(text)){
		if( timer >= delay ){
			cutoff++;
			timer = 0;
		} else {
			timer++;
		}
	}
	
	// Go to next message or end
	if(check_input(input.interact)){
		if( cutoff < string_length(text) ){
			cutoff = string_length(text);
		} else if( ds_queue_size(messages) > 1 ){
			ds_queue_dequeue(messages);
			cutoff = 0;
		} else {
			ds_queue_destroy(messages);
			instance_destroy();
			exit;
		}
	}
	
	// Draw background
	draw_nine_slice(0, camera_get_height()-39, camera_get_width(), camera_get_height(), spr_nslice_chat);
	
	// Draw text
	while(i <= string_length(text) && i<= cutoff){
	
		// Check for modifier
		if( string_char_at(text, i) == "~"){
			modifier = real(string_char_at(text, ++i));
			i++;
		}
	
		// Go to netx line
		if( string_char_at(text, i) == "¦"){
			tx = line_start;
			line++;
		}
	
		// Text
		switch(modifier){
			case 0: // Normal
				draw_set_color(global.c_1);
				draw_text(tx, ty+(10*line), string_char_at(text, i));
				break;
				
			case 1: // Shake
				draw_set_color(global.c_1);
				draw_text(tx+random_range(-0.6, 0.6), ty+(10*line)+random_range(-0.6, 0.6), string_char_at(text, i));
				break;
				
			case 4: // Sine
				var so = t + i;
				var shift = sin(so*pi*freq/room_speed)*amp;
				draw_set_color(global.c_1);
				draw_text(tx, ty+(10*line)+shift, string_char_at(text, i));
				break;
				
			case 5: // Gradient
				var col = make_color_hsv(t + i, 255, 255);
				var col2 = make_color_hsv(t + i + 75, 255, 255);
				draw_text_transformed_color(tx, ty+(10*line), string_char_at(text, i), 1, 1, 0, col, col, col2, col2, 1);
				break;		
		}
		
		// Set the x posiiton of the next character after the one we just drew
		tx += string_width(string_char_at(text, i));
		
		i++;
	}
	
	// Draw portrait
	switch(portrait){
		case "none":
			break;
	}
}