/// @description Collision

if(other.id != creator){
    // Knockback on entities
    if(instance_exists(creator)){
        var dir = point_direction(creator.x, creator.y, other.x, other.y);
    } else {
        var dir = point_direction(x, y, other.x, other.y);
    }
    
	set_movement_direction_speed(dir, knockback, other);
	
	// Damage target
	damage_entity(other, damage);
}