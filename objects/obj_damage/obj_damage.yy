{
    "id": "8461b64c-3ca3-465f-a5b6-5266b708137a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_damage",
    "eventList": [
        {
            "id": "55000999-9d03-4796-8ba3-c2fad03070fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8461b64c-3ca3-465f-a5b6-5266b708137a"
        },
        {
            "id": "81847b59-f129-4bce-9054-d4edb7cf600f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "8461b64c-3ca3-465f-a5b6-5266b708137a"
        },
        {
            "id": "d9bfa66a-d17f-47d4-b170-7cc28f471566",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "877e45fd-7c28-4439-bdb7-a255b5a05944",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8461b64c-3ca3-465f-a5b6-5266b708137a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}