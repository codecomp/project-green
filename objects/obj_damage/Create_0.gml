/// @description Initialise the damage object

// Setup mask for collision
mask_index = msk_damage;

// Initialise the object variables
damage = 1;
knockback = 1;
creator = noone;

// Setup destroy alarm
alarm[0] = 1;