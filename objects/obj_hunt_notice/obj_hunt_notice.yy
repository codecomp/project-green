{
    "id": "0731cbda-729b-4121-8263-927a7c3269ba",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hunt_notice",
    "eventList": [
        {
            "id": "bfbbf109-7427-4050-b8ab-fec311e3f28b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8461b64c-3ca3-465f-a5b6-5266b708137a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0731cbda-729b-4121-8263-927a7c3269ba"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}