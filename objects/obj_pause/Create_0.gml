/// @description Initilise

prevent_duplicates();

// Handle toggling the pause menu
paused = false;

// Pause menu title
title = "Game Paused";

// Pause menu options
option[0] = "continue";
option[1] = "save";
option[2] = "Load";
option[3] = "Quit";

// Currently selected index
menu_index = 0;