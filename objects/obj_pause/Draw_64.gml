/// @description Draw overlay

if( paused ){
	// Draw overlay
	draw_set_color(global.c_4);
	draw_rectangle(0, 0, camera_get_width(), camera_get_height(), 0);
	
	// Draw text overlay box
	draw_nine_slice(20, 40, camera_get_width() - 20, camera_get_height()-40, spr_nslice_special);
	
	// Draw Paused text
	draw_set_halign(fa_center);
	draw_set_color(global.c_1);
	draw_text(camera_get_width()/2, 43, title);
	
	// Draw menu options
	for(var i=0; i<array_length_1d(option); i++){
	    if(menu_index == i)
	        draw_text_colour_outline_exact(camera_get_width()/2, 57+(i*9), option[i], 1, global.c_3);
        else
			draw_text(camera_get_width()/2, 57+(i*9), option[i]);
	}
	
	// Reset text draw
	draw_set_halign(fa_left);
}
