{
    "id": "802a4a2b-a552-44e0-8fef-b731ad885347",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_pause",
    "eventList": [
        {
            "id": "4243be2c-01a0-4da8-9dc9-0686522417fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "802a4a2b-a552-44e0-8fef-b731ad885347"
        },
        {
            "id": "a6ceb0bd-5fe8-4d19-b34d-9ef2ebe7f5bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "802a4a2b-a552-44e0-8fef-b731ad885347"
        },
        {
            "id": "8d32d279-a223-43b1-a885-c1c680c4fd89",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "802a4a2b-a552-44e0-8fef-b731ad885347"
        },
        {
            "id": "af248ff4-b150-4e15-a6f0-b57f789cbaf2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "802a4a2b-a552-44e0-8fef-b731ad885347"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}