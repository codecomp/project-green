/// @description Controls

if( check_input(input.pause) ){
	// Toggle pause state
	paused = !paused;
	
	// Resrt menu position
	menu_index = 0;
	
	// Handle instance activation.deactivation
	if( paused ){
		instance_deactivate_all(true);
		// Keep the camera running so we can draw correctly
		instance_activate_object(obj_camera);
	} else {
		instance_activate_all();
	}
	
	// Stop handling iother controls if entering or exiting the menu
	return;
}

if(paused && alarm[0] == -1){
    if( check_input(input.down) ){
        if(menu_index < array_length_1d(option)-1){
            menu_index++;
        } else {
            menu_index = 0;
        }
        alarm[0] = room_speed / 6;
    } else if( check_input(input.up) ){
        if(menu_index > 0){
            menu_index--;
        } else {
            menu_index = array_length_1d(option)-1;
        }
        alarm[0] = room_speed / 6;
    } else if( check_input(input.interact) ){
        switch(menu_index){
            case 0: // Continue
                paused = !paused;
				instance_activate_all();
                break;
            case 1: // Save
                // TODO Trigger save script
                break;
            case 2: // Load
                // TODO Trigger load script
                break;
            case 3: // Quit
                game_end();
                break;
            default:
                break;
        }
    }
}