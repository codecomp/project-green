{
    "id": "fd719a14-f488-4aa9-8934-2ea33cb099b2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_portcullis_dark",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "d6581456-0fd2-476a-af86-92c92a1cabe3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "50a1f3bb-1f7f-4383-bba1-7ad25f42567f",
    "visible": true
}