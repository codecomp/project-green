{
    "id": "da551d3e-daff-4db3-b49a-adb9731ae85a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hunt_poster",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "0731cbda-729b-4121-8263-927a7c3269ba",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "1af05e3c-c283-4a16-bdf5-2083e854578c",
    "visible": true
}