/// @description Update base variables

// Run parent initialisation
event_inherited();

maxhspd = 0.1;
maxvspd = 0.1;

hp = 2;

// Add enemy states
ds_list_add(available_states, states.patrol_x);
ds_list_add(available_states, states.patrol_y);

// Setup animation sprites
add_animation_sprites(
	states.idle,
	spr_slime_idle_down,
	spr_slime_idle_down,
	spr_slime_idle_down,
	spr_slime_idle_down
);