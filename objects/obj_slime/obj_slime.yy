{
    "id": "206ba316-d870-4533-a2a3-777a4d852d11",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_slime",
    "eventList": [
        {
            "id": "98065275-7d54-4730-8fd6-46e5f11a536c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "206ba316-d870-4533-a2a3-777a4d852d11"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "f7abd7f6-8c38-4083-a167-a39ac18cb420",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "c537ef17-fdff-44bf-bc2f-18a694ae4039",
    "visible": true
}