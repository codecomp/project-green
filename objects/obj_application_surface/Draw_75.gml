/// @description End and draw surface

// Reset target to main canvas
surface_reset_target();

// Setup the shader
shader_set(shd_4_color);

// Pass variables to shader
texture_set_stage(u_palette_tex, palette_tex);
shader_set_uniform_f(u_palette_uvs, uvs_x, uvs_y, uvs_z, uvs_w);
shader_set_uniform_f(u_palette_texel, texel_x, texel_y);

// Draw the surface onto the screen
draw_surface(gui_surface, 0, 0);

// Reset shader
shader_reset();

if(global.debug && surface_exists(obj_console.gui_surface)){
	// Set GUI scale to be real pixel sizes for console
	var pos = application_get_position();
	var gui_width = display_get_gui_width();
	var gui_height = display_get_gui_height();
	display_set_gui_maximise(1, 1, pos[0], pos[1]);
	
	draw_surface(obj_console.gui_surface, 0, 0);
	
	// Reset GUI scale
	display_set_gui_maximise(-1, -1);
	display_set_gui_size(gui_width, gui_height);
}