/// @description Set surface

// Make sure we have a surface to draw on
if(!surface_exists(gui_surface)){
	gui_surface = surface_create(obj_camera.width, obj_camera.height);
}

// Setup the surface and paint default colour
surface_set_target(gui_surface);
draw_clear_alpha(c_black, 0);