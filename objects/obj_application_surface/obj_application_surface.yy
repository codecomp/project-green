{
    "id": "32e26dbf-1663-46d1-a40c-6d38a9728b97",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_application_surface",
    "eventList": [
        {
            "id": "ba62dd40-a11b-4c2b-8813-e6e35e69d0d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "32e26dbf-1663-46d1-a40c-6d38a9728b97"
        },
        {
            "id": "9cfe78da-f22b-4c52-81dc-4c8c00156cd7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "32e26dbf-1663-46d1-a40c-6d38a9728b97"
        },
        {
            "id": "bf246300-76fb-4712-82a1-ef4f17030d8f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 74,
            "eventtype": 8,
            "m_owner": "32e26dbf-1663-46d1-a40c-6d38a9728b97"
        },
        {
            "id": "c60d978b-5cae-413a-849c-d44ae2bf353f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 75,
            "eventtype": 8,
            "m_owner": "32e26dbf-1663-46d1-a40c-6d38a9728b97"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}