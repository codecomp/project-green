/// @description Initialise

// Disable application draw
application_surface_draw_enable(false);

// Setup GUI surface
gui_surface = noone;

// Get references to variables in shader
u_palette_tex = shader_get_sampler_index(shd_4_color, "u_palette");
u_palette_uvs = shader_get_uniform(shd_4_color, "u_uvs");
u_palette_texel = shader_get_uniform(shd_4_color, "u_texel");

set_application_palette(spr_palette_14);