/// @description Draw with shader

// Setup the shader
shader_set(shd_4_color);

// Pass variables to shader
texture_set_stage(u_palette_tex, palette_tex);
shader_set_uniform_f(u_palette_uvs, uvs_x, uvs_y, uvs_z, uvs_w);
shader_set_uniform_f(u_palette_texel, texel_x, texel_y);

// Draw the surface onto the screen
draw_surface(application_surface, 0, 0);

// Reset shader
shader_reset();