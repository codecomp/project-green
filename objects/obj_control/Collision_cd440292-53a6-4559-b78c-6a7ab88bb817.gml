/// @description Trigger

if(other.creator.object_index == obj_player){
	active = !active;
	if(active){
		image_index = 1;
	} else {
		image_index = 0;
	}
	for(var i=0; i<array_length_1d(targets); i++){
		with(targets[i]){
			active = !active;
			if( object_index == obj_control || object_is_ancestor(object_index, obj_control) ){
				if(active){
					image_index = 1;
				} else {
					image_index = 0;
				}
			}
		}
	}
}