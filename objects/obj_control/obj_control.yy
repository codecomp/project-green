{
    "id": "d1e8442d-6980-49a3-a644-cbd20de10539",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_control",
    "eventList": [
        {
            "id": "08f6dac0-859b-4da2-b849-9aad7712f5b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d1e8442d-6980-49a3-a644-cbd20de10539"
        },
        {
            "id": "cd440292-53a6-4559-b78c-6a7ab88bb817",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8461b64c-3ca3-465f-a5b6-5266b708137a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d1e8442d-6980-49a3-a644-cbd20de10539"
        },
        {
            "id": "d72cfae1-ef0d-4b88-a4d0-15c9b9cedda8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "d1e8442d-6980-49a3-a644-cbd20de10539"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "e777ad34-8a55-4fb1-8cbe-f3c67ea4d6c2",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}