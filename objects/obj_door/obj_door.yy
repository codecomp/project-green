{
    "id": "d6581456-0fd2-476a-af86-92c92a1cabe3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_door",
    "eventList": [
        {
            "id": "d320f9a5-88eb-4298-aa88-8e8288e8855f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d6581456-0fd2-476a-af86-92c92a1cabe3"
        },
        {
            "id": "32f45798-ecd6-4add-bbff-a2ddc63baf31",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d6581456-0fd2-476a-af86-92c92a1cabe3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "e777ad34-8a55-4fb1-8cbe-f3c67ea4d6c2",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}