/// @description 

if( active && round(image_index) != image_number ){
	image_speed = 1;
} else if( !active && round(image_index) != 0 ) {
	image_speed = -1;
} else {
	image_speed = 0;
	collide = !active;
}