///@description Activate

// Only collide once
if( !active ){

	// Start the timer to move room
	active = true;

	// Trigger the fade transition
	instance_create_layer(0, 0, "Instances", obj_transition);

}