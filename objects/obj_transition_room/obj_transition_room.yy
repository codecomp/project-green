{
    "id": "13485b1f-9690-4777-89c8-fdf358c6fbb6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_transition_room",
    "eventList": [
        {
            "id": "7780e6c6-4223-43d0-a81f-3cae3c10b1d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "13485b1f-9690-4777-89c8-fdf358c6fbb6"
        },
        {
            "id": "b9137e2f-1f9a-4f1e-85ee-a1fd28b2b7eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "019492c9-982a-4b86-8249-b342c2411c49",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "13485b1f-9690-4777-89c8-fdf358c6fbb6"
        },
        {
            "id": "d02420f0-7cfc-4d6b-9877-2c91cdc3270c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "13485b1f-9690-4777-89c8-fdf358c6fbb6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "9f992060-ce1b-4f86-8e48-91c7a17ae7f3",
    "visible": true
}