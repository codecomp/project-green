///@description Run timer

if( active ){
	timer += 1/room_speed;

	if( timer > 1 ){
		active = false;
		timer = 0;
		
		// If moving in the same room don't move or it will trigger room_restart
		if( room != target_room ){
			room_goto(target_room);
			
			// Move the player
			obj_player.x = target_x;
			obj_player.y = target_y;
			
			// Destroy the existing camera
			if( instance_exists(obj_camera) ){
				instance_destroy(obj_camera);
			}
		
			// Make a new camera
			instance_create_layer(0, 0, "Instances", obj_camera);
		} else {
			// Move the player
			warp_movement_entity(target_x, target_y, obj_player);
			
			// Move the camera
			camera_warp_to(target_x, target_y);
			camera_set_follow(obj_player);
		}
		
		// Create the inward transition
		instance_create_depth(0, 0, 0, obj_transition);
		obj_transition.duration = obj_transition.duration*-1;
		obj_transition.counter = 1;
		obj_transition.transition = spr_transition_2;
	}
}