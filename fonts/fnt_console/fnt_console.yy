{
    "id": "0a807f3c-d3c9-45bb-ae10-d07597a7aec1",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_console",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "pixelmix",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "4b62be3d-a286-4b3b-8138-09f73f4e9a43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 19,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "9a1d8bd1-dae5-445b-bc1d-208913c90a1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 8,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 110,
                "y": 42
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "293ed87a-cbe8-42b1-864f-6b9f64d9238c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 3,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 105,
                "y": 42
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "5e926461-7a8a-4605-a8c1-40689a0e0493",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 65,
                "y": 22
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "4f9250be-501c-43f2-ac5f-3a181fe463a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 72,
                "y": 22
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "ced5e568-4053-48e9-9c51-dca3a1bb06a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 79,
                "y": 22
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "bf065ba0-9c8b-4118-b347-54c41a475baf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 8,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 11,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "f308cf4c-11a3-45e9-a295-d4119593e4ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 3,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 123,
                "y": 42
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "5a390ed4-427f-4f89-af3a-7e089a89bad2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 53,
                "y": 42
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "13e9f526-7b5e-4ff9-a96b-2a4f972d0010",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 68,
                "y": 42
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "e8bd3517-1921-48d9-93d6-b4d8aa3b09c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 4,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 100,
                "y": 42
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "63bceacd-543f-4495-a6ea-ecfd152a0617",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 79,
                "y": 32
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "4f337b26-b6d3-4bda-a5c7-680ce84964b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 8,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 96,
                "y": 42
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "07ba3b4b-76e8-4a68-b679-16796643fcf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 5,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 16,
                "y": 42
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "ff208ad0-24d5-4c71-a212-65570b89e301",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 8,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 88,
                "y": 42
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "325587b7-5528-4213-be12-10f5c6671d9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 48,
                "y": 42
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "2c9ae940-280f-4fbe-bace-7b5eb6d5b331",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 86,
                "y": 22
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "f13cd7fb-da51-45a1-9db3-4002dacbd73b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 28,
                "y": 42
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "5483a938-69bd-473b-9d05-5d979b968a4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 93,
                "y": 22
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "4e4a70cb-5753-436f-9312-136428b518f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 100,
                "y": 22
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "21c70057-fa41-44eb-b915-bc586fce0b4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 23,
                "y": 32
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "156d2932-ab41-4b00-9842-01c88cc2b009",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 107,
                "y": 22
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "a542a1c7-b74e-4b07-bef9-3200bd42ffee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 114,
                "y": 22
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "a1ba7802-5f67-4129-a43a-3500aae398f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 32
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "7f64eda7-7a60-4313-b9e6-77e50d03b001",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 32
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "71d1d449-f2f8-43ac-8da7-911e4eef17ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 16,
                "y": 32
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "d14be910-7ad0-4679-9de8-30dd92654a00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 7,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 116,
                "y": 42
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "97e3db95-d174-4be9-84fe-b11083bcabe1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 8,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 84,
                "y": 42
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "af9bd9b1-24b6-48c1-a269-5084df23db49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 104,
                "y": 32
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "3a87646f-838c-4f0a-99bf-1d8f83055d37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 6,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 42
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "3662a347-1f99-49ec-b80d-d329f55ae53e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 92,
                "y": 32
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "9110521e-51ef-404a-8d27-312fe8307403",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 116,
                "y": 32
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "178f200d-ef4b-462d-b181-644393947b10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "a3f515aa-33ff-42d9-9da0-cfe0db7ba757",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 58,
                "y": 32
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "fdcd561f-3fd0-43b6-a829-bab1b222c270",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 65,
                "y": 32
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "2aca7f63-382c-43c8-b0b2-edede1c65281",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 72,
                "y": 32
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "e63dd2c9-ee8f-4824-9bbe-d83ef2e84bf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 51,
                "y": 32
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "3d88eab1-a569-4564-818c-87db46ed34f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 44,
                "y": 32
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "0a1d6347-e8d4-4dbb-9dcb-d7f0dbeda4fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 37,
                "y": 32
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "6df1aea9-8ed1-4ca1-beb6-1dd46ba72cef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 30,
                "y": 32
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "001d218e-4027-4232-b076-4f963d647ba6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 58,
                "y": 22
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "8bbf9241-d7d7-43f0-b631-3613c4a815d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 63,
                "y": 42
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "34edd9a6-7ee4-466b-963c-32a21cd1a8f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 44,
                "y": 22
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "d8cc581e-cc83-4243-92bf-b008bdd5b688",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 37,
                "y": 22
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "d4fe2cea-e644-4c42-97c8-b99f2ba1cd30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 12
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "ac9d96ac-14ca-4872-9426-5299b6201bf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "fec37607-6251-4eff-9f0b-9ed7ca2c1476",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "506f5513-776f-40a3-a98f-8d2dbcedf844",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 51,
                "y": 22
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "feca2dc7-38a7-4ef8-bfd8-a48a56a115ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d783ea0d-f3a1-4ea7-b0c3-58705cd1f576",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "7ec0be3a-fe9e-44d0-9ecf-4b57b6d07267",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "4fb0522b-8464-455b-b7f3-8649bc254261",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "662a2cfc-15bf-43bb-8b0b-9485502f15d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 12
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "3542e7fb-0604-4675-95b7-d3eab0da9abc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "b7c061f6-0376-458b-a295-81cb095a3622",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "be52634c-b126-4d26-b2fa-ed3d976cb01a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "819b0aaa-aad2-4e73-b618-35fd2bae2cbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "cf0275c1-09c1-488a-9c79-53df25d66054",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "35288f04-5e4b-4938-a3f1-062bfc451ec4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "def4d04c-15dd-4493-a57c-7c43bd2e8cfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 8,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 80,
                "y": 42
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "66566ba2-be2f-4621-b551-c0eec72f3ec3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 58,
                "y": 42
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "9d1635f4-c249-425c-b01a-e27bd41858be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 8,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 92,
                "y": 42
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "1a60d16b-6bca-4d47-85e4-16d23822c8bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 4,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 73,
                "y": 42
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "67a1596c-0925-4c47-9677-48146e8df237",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 110,
                "y": 32
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "af2b8a85-90a4-4249-ae73-f208585ccb15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 3,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 119,
                "y": 42
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "ad80bbae-d132-494f-9105-de2f2ed52d9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "81189fb2-a572-4624-923f-69a36d5744f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "03a055e9-e668-44cb-b027-88359c67231f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 16,
                "y": 12
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "44a14540-5144-4d4b-8166-68e6ea633fe7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 23,
                "y": 12
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "838dbcc9-dab0-4eb9-8796-e70065a6721f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 30,
                "y": 12
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "96a4b197-a64b-4c95-a8a0-0fe70f795f73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 30,
                "y": 22
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "03f6f2a0-be4c-4402-8eae-2e059de9acaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 23,
                "y": 22
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "95da9dfc-3e60-48ee-ada7-b515a6afe954",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 16,
                "y": 22
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "4755363e-d9d8-4c23-8c87-047b0b06bf88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 38,
                "y": 42
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "245b8f75-89c7-455c-ad0b-3e5d98a1f1d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 86,
                "y": 32
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "b2a6eab5-d274-44bb-86c5-c7874a2e4a7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 98,
                "y": 32
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "30a42ef8-85a9-4134-9efb-12191bc7e665",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 43,
                "y": 42
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "887e5528-f362-4ae4-9ef0-facc823def1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 22
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "408b3e2c-6104-4189-bd27-182c44de754a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 22
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "bec28f0a-2106-4748-b5dd-227cbef43cc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 114,
                "y": 12
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "e0e8ce77-4e3f-4f85-b3f1-6216b133c8a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 107,
                "y": 12
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "e2235a1d-7302-4f11-9a98-48ad0c68e61b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 100,
                "y": 12
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "0b97264c-ceb2-42bf-9d31-e263635239f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 93,
                "y": 12
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "4ee0df2b-d4f9-4041-808e-39794846c0b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 86,
                "y": 12
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "e29c6138-6237-4dfe-936f-f4e1862c8420",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 79,
                "y": 12
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "85afc784-32d6-41eb-8040-b197a1dfdbdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 72,
                "y": 12
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "8a0ba7a9-3831-4414-94ab-afe82dadda08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 65,
                "y": 12
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "87ad6df3-cccf-4da9-9e67-c3aabe1a6a97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 58,
                "y": 12
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "5a6022ef-91ba-49e6-9853-c39692c2aece",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 51,
                "y": 12
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "c4dbf005-2045-4ee6-95ce-c7d2ea815d8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 44,
                "y": 12
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "234b1da9-5d04-44e6-ad42-4e0678470033",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 37,
                "y": 12
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "6c704da9-e710-4221-8323-f4549c0d242d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 33,
                "y": 42
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "45adca81-ca0d-4f01-9efc-aa64af03fb32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 8,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 113,
                "y": 42
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "ea13291b-a1fc-437b-90d2-4a8e29efe2b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 23,
                "y": 42
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "c2b58b8d-b160-43eb-ba84-34d572ab31a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 6,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 42
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 6,
    "styleName": "",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}