{
    "id": "10b2ef29-9c6f-4dd0-b4be-8fb6e9ee82ec",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_pixeled",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Pixeled",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "e6721dfe-d1a2-4afe-b56f-32941b7e43bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 15,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 64,
                "y": 70
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "52bc7905-8de6-42ee-ad38-ad584aeafb60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 102,
                "y": 70
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "76b6e9cb-2ab1-4b51-8639-f6e4dc9a5914",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 53,
                "y": 70
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "defab8df-0866-4565-a006-43fe031a77cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 51,
                "y": 36
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "7d3a4324-2175-4e6d-9d29-988789376e14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "48013647-81fa-44c8-88dd-447b39a46175",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 13,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 12,
                "y": 70
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "44e3e175-2a1f-448f-9209-724430da227e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 13,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 17,
                "y": 70
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "51cefabe-768c-415b-ac0e-514202df5c0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 8,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 123,
                "y": 70
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "4c3ff4dc-b856-4bee-8e20-0477f0fe4072",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 32,
                "y": 70
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "23175940-3a8f-4972-bc56-7e984205c749",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 12,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 37,
                "y": 70
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "619d29e6-fbd0-4de2-b47a-cc3b3537f9e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 11,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 42,
                "y": 70
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "db59c609-0a81-47c7-adb3-0670021980ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 59,
                "y": 70
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "71b80d55-229d-417e-baf1-f5e68c5f1823",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 13,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 78,
                "y": 70
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "7f80255d-b69d-4078-bbfe-4d511c204b49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 73,
                "y": 70
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "7cbd4070-2084-4581-85be-7472f50fe7ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 82,
                "y": 70
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "cbf25d00-b38e-41fb-a4b1-a7fc14cc47bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 85,
                "y": 53
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "ece00a2f-0c7d-4f4f-a2ee-13f026c229ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 93,
                "y": 36
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "a146f638-676b-4187-b647-72f17140a9c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 86,
                "y": 70
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "fbf48969-cd93-4a6b-9d74-14a62989597b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 107,
                "y": 36
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "ec37f046-f4d0-494c-9119-7535c46e2c82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 79,
                "y": 53
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "b3d32a88-5646-4b43-af49-81d96b9961fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 53
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "69cf835e-b018-4662-9827-ed55fc5a55fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 65,
                "y": 53
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "613865d2-8175-486f-932f-7b508e8814be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 58,
                "y": 53
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "62712d35-256d-4ad1-b960-10b6d96d1c95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 51,
                "y": 53
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "aa77924c-81fc-4e05-b4eb-5e41da59ecbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 72,
                "y": 53
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "4a10e5dc-fc41-4ca7-9ad8-d587013efa45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 37,
                "y": 53
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "632b52a9-3773-4c19-a278-10943b6f90e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 11,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 119,
                "y": 70
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "a76f015c-e25e-46f2-9b3e-6d7ff8c757d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 94,
                "y": 70
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "6491b723-63fb-4222-9e51-2edf90d6d779",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 27,
                "y": 70
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "9265c4e6-893f-4ee8-8ab7-41389c1f2016",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 68,
                "y": 70
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "fa2680f1-b83d-4a1a-bc65-732199eed3b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 22,
                "y": 70
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "8b2b3cd9-7f36-4ae4-beb9-fa9e9518f0a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "2fe4b090-87f0-48d2-9b93-0d3ab2b2ffa6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 13,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 7,
                "y": 70
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "01f81472-829e-46bd-94a9-44673d5f84b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 30,
                "y": 36
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "a0b7fe21-a20a-49ee-9c1e-e57f229c7b0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 100,
                "y": 36
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "ad3eb792-e4ea-4e93-be01-1e0e8753fb14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 16,
                "y": 36
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "14b0f97b-2994-4f44-932f-0dfc6610fa01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 82,
                "y": 19
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "1c030683-1550-4495-8c1b-8fd55cf6f698",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 2,
                "y": 19
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "a716078f-506e-4c78-927c-2f1993fc741c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "6b18c7a8-5ebd-4d96-a5b9-5c0be52accfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 26,
                "y": 19
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "85de79fb-9c27-4e49-86fa-ff33d424fa7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 33,
                "y": 19
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "edc44fa7-93f0-4838-8ae8-5018af88cc12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 106,
                "y": 70
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "cbce4d73-7a3d-40ff-b7f9-e5a47be558ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 47,
                "y": 19
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "0236e0c6-eb95-427c-9a8d-d7f145dac513",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 54,
                "y": 19
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "60da03b1-a861-4f7d-a9c1-9f3f6806b954",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 42,
                "y": 2
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "a07d41d2-cf03-4009-a2b4-2f75746d3d62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 19
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "443cb936-1fbd-4145-8ffd-137ceeacd8f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 9,
                "y": 36
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "6ce8d3a7-2ff8-4155-a975-bda8b144d91c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 23,
                "y": 36
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "1b6cb8e2-12fd-400e-b630-feba6f45d7ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 75,
                "y": 19
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "14cae906-6d20-49de-bbf8-628503006c0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "55b0c157-9b69-4446-826c-9707f44b172b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 61,
                "y": 19
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "f8282987-41a9-4f75-8771-7ad0a0e3c4cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 89,
                "y": 19
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "1948dab2-6aff-49ab-a593-4eae98251ae8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 96,
                "y": 19
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "5fde9086-7940-41ff-ac73-ee69c866c441",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 103,
                "y": 19
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "4285c9d8-a66f-4a80-8d83-9058e503c82a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "9f584591-b3d2-494b-b8fe-808959cf1428",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "c814c0d0-9017-434b-a720-be3c6b6575e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "6dc49801-32e0-4773-88b4-b10263726124",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 19
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "4adea92c-da64-4333-9e86-03d1f51aafab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 110,
                "y": 19
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "b9c73e8b-d0e6-424e-8a5f-0f7866bb95e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 117,
                "y": 19
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "e83eff2a-2f8a-43ba-be7d-d19f53de2ba9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 91,
                "y": 53
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "21ebb89a-a86f-4ff3-8349-824809db0fda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 97,
                "y": 53
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "f8c9fe44-d915-444d-a85c-620d45e53413",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 47,
                "y": 70
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "45ca830b-b573-45d9-a221-742b6c10d677",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 103,
                "y": 53
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "d2a8503f-e49b-4bff-84ec-e13c68e42c34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 8,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 114,
                "y": 70
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "99e90b31-1568-4f69-99b0-34b207c4e31b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 44,
                "y": 53
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "399ca088-c014-417d-935d-7589186f36c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 9,
                "y": 53
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "96035374-c50c-4a41-ba70-f02f25fd16b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 36
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "8b6052b5-2028-4f43-ad42-8796263c9f08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 68,
                "y": 19
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "1a29ad80-5abe-4521-9d75-e633168f4db4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 40,
                "y": 19
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "d8dc8bbc-5962-4993-8c3f-f29b9c50836d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "1ae91c68-8b52-4024-a030-7fb22980b265",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 16,
                "y": 53
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "b9e4b887-4c1c-432a-bd3d-b05c409e7d99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 23,
                "y": 53
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "f221aadf-e8f0-436c-baa0-93ca858ac069",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 98,
                "y": 70
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "e2328b3d-c35b-4441-8c6c-ff30f19a0c3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 109,
                "y": 53
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "4c741f3c-2066-475a-bf1c-d6e1014fe868",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 30,
                "y": 53
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "49959cf0-4e83-498c-b406-1cc4a52b8521",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 90,
                "y": 70
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "c481f618-6c50-40ec-ba67-a257fc18be37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "8d058286-93da-4fa4-99b6-42bd4ac62263",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 114,
                "y": 36
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "d4df1ec6-51cc-4a82-9880-1c8a3effa98b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 86,
                "y": 36
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "5bed2439-b546-4073-ad50-0d38f259cc36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 79,
                "y": 36
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "3a96d11c-35e8-4d32-9dc2-11b6b0d6dd90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 72,
                "y": 36
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "64a5a784-68da-4d2a-afe8-b474d517f830",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "ce5d8b5c-16ab-41f1-bf7e-a4fa0e33454f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 65,
                "y": 36
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "ebdfc7e8-1333-47e3-b873-29885e139fef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "e4773278-0d49-457a-9b1e-16cea21930ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 58,
                "y": 36
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "72674715-6398-417f-96bb-3d2a540e548f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "9cc6d4f4-9cd8-4f50-9314-60b59f951e46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "cd737283-2239-47c6-a1b2-055fb2c5b150",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "c1b477a3-ea3b-4dce-b70c-0ed611febb04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 44,
                "y": 36
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "e846ddeb-1add-4ba3-9d5e-acbf4c712c16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 37,
                "y": 36
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "d685f521-f2dd-402f-8d69-bdbe9a9055de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 13,
                "offset": 1,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 70
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "c463cb3f-41c9-4322-990a-af0b5b806b91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 110,
                "y": 70
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "9262f675-fae2-46ed-93da-409d7b299aed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 13,
                "offset": 1,
                "shift": 3,
                "w": 3,
                "x": 122,
                "y": 53
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "d35b4fe6-8bc9-4f02-bfef-12987ae08649",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 115,
                "y": 53
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 4,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}